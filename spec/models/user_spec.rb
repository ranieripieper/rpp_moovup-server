require 'rails_helper'

RSpec.describe User, type: :model do

  before(:each) do
    @staff_user = create(:staff_user)
  end

  def each_user
    [@staff_user].each do |user|
      yield user
    end
  end

  it 'must have a valid origin association' do
    each_user do |user|
      expect(user).to have_one(:origin)
    end
  end

  it 'must have database indexes' do
    expect(subject).to have_db_index(:email)
  end

  it 'has a valid email' do
    user = build(:staff_user, email: 'invalid_email')
    expect(user.valid?).to be false
    expect(user.errors[:email]).to_not eq([])
  end

  it 'has a valid username' do
    user = build(:staff_user, username: 'username_staff')
    expect(user).to be_valid

    expect(user.errors[:username]).to eq([])
    expect(user.username).to eq('username_staff')
  end

  it 'dont allow invalid profile type' do
    user = build(:staff_user, profile_type: 'my_custom_profile_type')

    expect(user).not_to be_valid
    expect(user.errors[:profile_type]).to_not eq([])

    user.profile_type = User::VALID_PROFILES_TYPES[:staff]
    expect(user).to be_valid
    expect(user.errors[:profile_type]).to eq([])
  end

  it 'has a valid normalized username' do
    user = create(:staff_user, username: 'User Name Staff')
    expect(user.username).to eq('user_name_staff')

    user = create(:staff_user, username: 'User Name Staff')
    expect(user.username).to eq('user_name_staff2')

    user = create(:staff_user, username: 'User Name Staff')
    expect(user.username).to eq('user_name_staff3')

    user = create(:staff_user, username: 'User Name Staff')
    expect(user.username).to eq('user_name_staff4')

    user = create(:staff_user, first_name: 'My Awesome', last_name: 'User')
    expect(user.username).to eq('my_awesome_user')

    user = create(:staff_user, first_name: 'My Awesome', last_name: 'User')
    expect(user.username).to eq('my_awesome_user2')
  end

  it 'has a encrypted password' do
    expect(@staff_user.password_digest).to_not be_nil
  end

  it 'has a valid gender' do
    expect(@staff_user.errors[:gender]).to eq([])

    user = build(:staff_user, gender: 'Masculino')
    expect(user).not_to be_valid
    expect(user.errors[:gender]).to_not be eq([])
  end

  it 'must validates password updates' do
    expect(@staff_user.password_digest).to_not be_nil

    @staff_user.password = 'i'

    expect(@staff_user).to_not be_valid

    expect(@staff_user.errors[:password]).to_not eq([])
    expect(@staff_user.errors[:password_confirmation]).to_not eq([])

    expect(@staff_user.password_digest_changed?).to be true

    @staff_user.password = 'valid_password_123'

    last_password_digest = @staff_user.password_digest

    expect(@staff_user).to_not be_valid
    expect(@staff_user.errors[:password]).to eq([])
    expect(@staff_user.password_digest_changed?).to be true

    @staff_user.password_confirmation = 'valid_password_123'

    expect(@staff_user.password_digest).to eq(last_password_digest)

    expect(@staff_user).to be_valid
    expect(@staff_user.errors[:password_confirmation]).to eq([])

  end

  it 'successfully update user password' do
    expect(@staff_user.password_digest).to_not be_nil

    @staff_user.update_password('new_password')

    expect(@staff_user.password).to eq(@staff_user.password_confirmation)
    expect(@staff_user.password_digest).to_not be_nil
    expect(@staff_user.reset_password_token).to be_nil
    expect(@staff_user.authorizations.size).to be 0
    expect(@staff_user.password_reseted_at).to be <= Time.zone.now

    expect(@staff_user.authenticate('new_password')).to be_a(User)
    expect(@staff_user.authenticate('new_password')).not_to be_nil
  end

  it 'sucessfully create reset password token for user' do
    @staff_user.reset_password!

    user_id = @staff_user.id

    expect(@staff_user.reset_password_token).to_not be_nil
    expect(@staff_user.reset_password_token).to match %r(\A#{user_id})
    expect(@staff_user.reset_password_sent_at).to_not be_nil
    expect(@staff_user.reset_password_sent_at).to be < Time.zone.now
  end

  it 'has a fullname' do
    user = build(:staff_user, first_name: 'Full', last_name: 'Name')
    expect(user.fullname).to eq('Full Name')
    expect(user.name).to eq('Full Name')
  end

  it 'has a convinience method for check profile type' do
    expect(@staff_user.is_staff?).to be true
  end

  it 'must authenticate using class method' do
    user = create(:staff_user, password: 'password_123', password_confirmation: 'password_123', email: 'staff@moovup.com.br')

    expect(User.authenticate('staff@moovup.com.br', 'password_123')).to be_a(User)
    expect(User.authenticate('staff@moovup.com.br', 'wrong_password')).to be false
  end

  it 'must have class scope for profile types' do
    User::VALID_PROFILES_TYPES.each do |scope_name, _|
      expect(User.respond_to?(scope_name)).to be true
    end
  end

end
