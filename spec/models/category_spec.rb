require 'rails_helper'

RSpec.describe Category, type: :model do
  it 'must be valid' do
    category = create(:category)

    expect(category).to be_valid
  end

  it 'must be invalid' do
    category = build(:category, title: nil)

    expect(category).not_to be_valid
    expect(-> { create(:category, title: nil) }).to raise_error
  end

  it 'must increment position successfully' do
    category     = create(:category)
    new_category = create(:category)

    expect(category.position).to be == (new_category.position - 1)
    expect(new_category.position).to be == category.position.next
    expect(category.position.next).to be == new_category.position
  end

  it 'allow same position' do
    category     = create(:category, position: 2)
    new_category = create(:category, position: 2)

    expect(category.position).to be == new_category.position
  end

end
