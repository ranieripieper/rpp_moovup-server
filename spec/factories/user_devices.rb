FactoryGirl.define do
  factory :user_device do
    user nil
    identifier "MyString"
    token "MyString"
    platform "MyString"
    deleted_at "2015-11-17 19:27:23"
    parse_object_id "MyString"
    parse_installation_id "MyString"
  end

end
