FactoryGirl.define do

  factory :user do

    trait :basic_data do
      first_name 'Moovup'
      last_name { profile_type.to_s.titleize }
      sequence(:email) {|n| "moovup_#{profile_type.downcase}_#{n}@moovup.com.br" }
      password { "moovup_#{profile_type.downcase}_230" }
      password_confirmation { password  }
      gender { User::VALID_GENDERS.values.sample }
      activated_at { Time.zone.now }
    end

    trait :user_with_origin do
      basic_data

      after(:build) do |user|
        origin = build(:origin)
        # better than user.origin = origin (avoid error Mistmatch)
        origin.originable_id   = user.id
        origin.originable_type = user.class.name
      end

      after(:create) do |user|
        create(:origin, originable_id: user.id, originable_type: user.class )
      end
    end

    User::VALID_PROFILES_TYPES.each do |key, value|
      factory key, class: User, aliases: [ "#{key}_user" ] do |f|
        profile_type value
        user_with_origin
      end
    end

    factory :admin, class: User, aliases: [ "admin_user" ] do |f|
      profile_type :staff
      user_with_origin

      after(:create) do |user|
        user.add_role :admin
      end
    end

  end

end
