FactoryGirl.define do
  factory :category do
    title { Faker::Commerce.department(1)  }
    sequence(:position)
  end
end
