FactoryGirl.define do
  factory :authorization do |f|
    association :user, factory: :staff_user
    provider 'faker'
  end
end
