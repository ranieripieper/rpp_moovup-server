module Moovup
  module Cache
    module_function

    def config
      @@config ||= YAML.load(ERB.new(File.read(Rails.root.join('config', 'caches.yml'))).result).fetch(Rails.env, {})
    end

    def fetch(key, options = {}, &block)
      cache_key = key_for(key, options.delete(:replace_data))

      options[:expires_in] = expires_for(key)

      if options.key?(:not_expected_value)
        cached_value = Rails.cache.read(cache_key)

        return cached_value if cached_value

        response = block.call

        if response != options[:not_expected_value]
          Rails.cache.write(cache_key, response, options.slice(:expires_in))
        end

        return response
      end

      Rails.cache.fetch(cache_key, options) do
        block.call
      end
    end

    def read(key)
      Rails.cache.read(key)
    end

    def format_cache_key(key, replace_data = [])
      if key.match(/(\%(s|i|d))/i) &&
        key = key % replace_data
      end

      key
    end

    def delete(keys)
      return false if keys.blank?
      if(keys.is_a?(Array))
        return keys.map { |key| delete(key) }
      end

      Rails.cache.delete(keys) rescue nil
    end

    def delete_key(key, replace_data = [])
      key = key_for(key, replace_data)

      delete(key)
    end

    def clear_all
      Rails.cache.clear
    end


    def key_for(key, replace_data = [])
      cache_key = key
      key_data  = data_for(key)

      if key_data[:key]
        cache_key = format_cache_key(key_data[:key], replace_data)
      end

      cache_key

    end

    def data_for(key)
      key_data  = Moovup.utils.get_value_by_dot_string(key, self.config)

      if key_data.present? && key_data.any?
        return key_data
      end

      {}
    end

    def expires_for(key)
      key_data = data_for(key)
      key_data[:expires]
    end


    def mount_conditional_key(base, values)
      if(values.is_a?(Hash))
        values.delete_if {|k,v| v.blank? } # não salva chaves sem valores
        values = values.to_a.map {|k| k.join("_") }
      end
      rest = values.compact.join("_")
      key  = rest.present? ? [base, rest].join("_").parameterize : base
      key
    end

  end

  module Utils
    def self.get_value_by_dot_string(string, hash)
      hash  = hash.deep_symbolize_keys
      value = nil
      keys  = string.split('.')
      while new_key = keys.shift do
        value = (value.nil? ? hash[new_key.to_sym] : value[new_key.to_sym])
      end
      value
    end
  end

  def self.cache
    Cache
  end
  def self.utils
    Utils
  end
end
