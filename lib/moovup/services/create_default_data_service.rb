# encoding: UTF-8

require 'csv'
require 'json'

module Moovup
  module Services
    class CreateDefaultDataService < BaseService

      def initialize(options={})
        super(options)
        @verbose = options.fetch(:verbose, true)
        @actions = [options.delete(:actions) || :all_actions].flatten
      end

      def execute
        with_faker_locale do
          @actions.each do |action|
            self.send(action)
          end
        end
      end

      private
      def with_faker_locale(locale = 'pt-BR', &block)
        last_locale = Faker::Config.locale
        Faker::Config.locale = locale

        yield block if block_given?

        Faker::Config.locale = last_locale

        block
      end

      def all_actions
        find_or_create_users
        find_or_create_categories
        find_or_create_carrers
        find_or_create_comercial_activities
        find_or_create_questions
        find_or_create_states
        find_or_create_cities
      end

      def find_or_create_questions
        questions_files = Dir[File.join(Rails.root.join('docs', 'seeds', 'questions', '*.json'))]

        questions_files.each do |question_file|
          questions = JSON.load(File.read(question_file))
          category_questions = questions.keep_if {|data| data['question'].present? }
          total_questions = category_questions.size

          puts "\nAdicionando Questões para #{File.basename(question_file, '.json').titleize}\n"

          category_questions.each_with_index do |question_data, index|
            print "\r>> %s/%s" % [index.next, total_questions]

            question_body = question_data['question'].try(:squish)
            answers = Array.wrap(question_data['answers'] || [])
            category_name = question_data['category'].squish

            father_category_name = (Category.find_by(title: category_name).try(:root).try(:title) || category_name).squish

            if question_body.present?
              question_body = question_body + '?' unless question_body.ends_with?('?')
            end

            question_body = question_body.squish

            question_category = Moovup::Config.enabled?(:only_allow_root_categories_in_qa) ?
                                  father_category_name :
                                  category_name

            data = {
              category_name: question_category,
              question_body: question_body,
              answers: answers_with_images(answers, father_category_name)
            }

            next unless create_question?(question_category, question_body, ENV['FORCE'].present?)

            create_question(data)
          end
        end

      end

      def create_question?(category_name, body_text, force = false)
        scope = Moovup::Config.enabled?(:only_allow_root_categories_in_qa) ? Category.roots : Category
        category = scope.where(title: category_name).first_or_create

        return false if body_text.blank? || category.blank?
        return false if category.questions.where(body_text: body_text).exists? && !force

        return true
      end

      def answers_with_images(answers, category)
        category = category.to_s.parameterize
        answers.map do |answer|
          images = answer['images'] || []

          answer['images'] = images.map do |image|
            filename = Rails.root.join('docs', 'seeds', 'questions', 'images', category, image)
            begin
              File.open(filename)
            rescue Exception => e
              puts '%s não encontrado!' % filename
              nil
            end
          end

          answer
        end
      end

      def sample_system_user(last_user = nil)
        unless @system_users
          user_emails = []
          seed_user_images.each_with_index do |user_image, index|
            user_emails << Moovup::Config.system_user_email % index.next
          end

          @system_users = User.where(email: user_emails)
        end

        begin
          sample_user = @system_users.sample
        end while (last_user.present? ? sample_user == last_user : false)

        sample_user
      end

      def create_question(question_data, asker_user = nil)
        asker_user = sample_system_user

        scope = Moovup::Config.enabled?(:only_allow_root_categories_in_qa) ? Category.roots : Category
        category = scope.where(title: question_data[:category_name]).first_or_create

        if category

          options = {
            body_text: (question_data[:body_text] || question_data[:question_body]).squish,
            category_id: category.id
          }

          service = Moovup::V1::Questions::CreateService.new(asker_user, question: options)

          if service.execute
            if answers = question_data[:answers]
              answers = answers.map(&:deep_symbolize_keys)

              answers.each do |answer_data|
                answer_options = {
                  image: answer_data[:image] || (answer_data[:images].try(:first))
                }.merge(answer_data.merge(question_id: service.question.id))

                answer_user = sample_system_user(asker_user)
                answer_service = Moovup::V1::Answers::CreateService.new(answer_user, answer: answer_options)
                answer_service.execute
              end
            end

            sleep(0.3) if ENV['SLEEP_ON'].present?
          else
            puts "Erros ao criar question: #{service.errors}"
          end
        else
          puts "Categoria #{question_data[:category_name]} não encontrada!!!!"
        end
      end


      def find_or_create_states
        puts "\nAdicionando estados\n"
        states = CSV.parse(File.open(Rails.root.join('docs', 'seeds', 'states.csv'), 'r'))

        states.each_with_index do |state_data, index|
          State.where(name: state_data.first.squish, acronym: state_data.last.squish).first_or_create
          print "\r>> %s/%s" % [index.next, states.size]
        end
      end

      def find_or_create_cities
        cities          = CSV.parse(File.open(Rails.root.join('docs', 'seeds', 'cities.csv'), 'r'))
        cities_by_state = {}

        cities.each do |city_data|
          state = city_data[2].squish.upcase
          cities_by_state[state] ||= []
          cities_by_state[state] << city_data.first.squish
        end

        cities_by_state.each do |state_acronym, cities|
          state = State.where(acronym: state_acronym).first

          if state
            puts "\nAdicionando cidades para o estado: #{state.name}\n"
            cities.each_with_index do |city, index|
              state.cities.where(name: city).first_or_create
              print "\r>> %s/%s" % [index.next, cities.size]
            end
          else
            puts "Estado não encontrado: %s" % state_acronym
          end
        end
      end
      private

      def find_or_create_users
        find_or_create_admin_users
        find_or_create_asker_and_replier_users
      end

      def find_or_create_admin_users
        admin_email = Moovup::Config.admin_user_email

        profile_image = File.open(Rails.root.join('docs', 'seeds', 'users', 'admin.jpg'))

        create_user(admin_email, :staff, first_name: 'Staff', last_name: 'Moovup', gender: User::VALID_GENDERS[:male], profile_image: profile_image)
      end

      def seed_user_images
        users_avatar_folder = Rails.root.join('docs', 'seeds', 'users')
        Dir[File.join(users_avatar_folder, '[^admin]*.jpg')].sort
      end

      def find_or_create_asker_and_replier_users
        seed_user_images.each_with_index do |user_image, index|

          user_email = Moovup::Config.system_user_email % index.next

          gender = user_image.match(/\-female/) ? :female : :male
          name_matches = File.basename(user_image).scan(/\d+\-(.+)\+(\w+)-?/).flatten
          first_name = name_matches.shift
          last_name = name_matches.join(' ')
          profile_image = File.open(user_image)

          user_data = {
            first_name: first_name,
            last_name: last_name,
            gender: User::VALID_GENDERS[gender],
            profile_image: profile_image
          }

          create_user(user_email, :staff, user_data)
        end
      end

      def find_or_create_categories
        find_or_create_collection('categories.json', 'Category', 'title', 'measurement_type', 'hobby', 'closed_goal', 'locked')
      end

      def find_or_create_carrers
        find_or_create_collection('carrers.json', 'Carrer', 'title')
      end

      def find_or_create_comercial_activities
        find_or_create_collection('comercial_activities.json', 'CommercialActivity', 'title')
      end

      def find_or_create_collection(seed_filename, class_name, *attributes)
        collection = old_collection = load_seed_file(seed_filename)
        klass = class_name.constantize
        father_associations = nil

        collection.each_with_index do |data, index|
          childrens = data.delete('childrens')
          record = klass.find_or_create_by(data.slice(*attributes))

          if record.persisted?
            if childrens.present? && childrens.any?
              childrens.each_with_index do |children_data, index|
                children = record.children.find_or_create_by(children_data.slice(*attributes))
              end
            end
            print "\r%s/%s" % [index.next, collection.size]
          end
        end

        puts "\nTotal de #{collection.size} #{class_name} adicionadas"
      end

      def create_user(email, profile_type, default_data = {})
        user_password = "#{profile_type.downcase}_password_123"

        params = {
          async: false,
          user: {
            first_name: Faker::Name.first_name,
            last_name: Faker::Name.last_name,
            email: email,
            profile_type: profile_type,
            password: user_password,
            password_confirmation: user_password,
            gender: User::VALID_GENDERS.values.sample
          }.merge(default_data)
        }

        # TODO: Migrate to Moovup::V1::Users::CreateService
        user = User.create(params[:user])

        if user.persisted? && user.valid?
          user.activate_account!

          puts "\nRegistrando usuário com perfil do tipo = '%s' e email '%s'\n" % [profile_type, email]
        else
          puts "\nErro ao cadastrar usuário: #{user.errors.messages}"
        end
      end

      def load_seed_file(filename)
        JSON.load(Rails.root.join('docs', 'seeds', filename))
      end

      def convert_questions_seed_to_json
        csvs_to_check = Dir.glob(Rails.root.join('docs', 'seeds', 'questions', '*.csv'))

        csv_data = csvs_to_check.map do |csv_file|
          { File.basename(csv_file) => CSV.parse(File.open(csv_file)) }
        end

        csv_data.each do |data|
          data.each do |category, c_data|
            filename = File.join(Rails.root, 'docs', 'seeds', 'questions', category.sub('csv', 'json'))

            new_data = c_data.map do |question_data|
              answers = question_data.from(2).compact.map {|answer|
                { body_text: answer.squish, images: get_images_for_answer_from_seed(answer) }
              }

              a_category = question_data.shift
              question = question_data.shift

              {
                question: question,
                answers: answers,
                category: a_category
              }
            end

            File.open(filename, 'wb') {|f| f.write(JSON.pretty_generate(new_data)) }
          end
        end
      end

      def get_images_for_answer_from_seed(answer_body, images_data = nil)
        images_data ||= load_seed_file("questions/images/images_data.json")

        image_data = images_data.select {|image| image['answer'].parameterize == answer_body.parameterize }

        Array.wrap(image_data.map {|i| i['image_name'] })
      end

    end
  end
end
