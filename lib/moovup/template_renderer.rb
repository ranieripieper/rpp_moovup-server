module Moovup
  class TemplateRenderer

    attr_reader :resultm, :data

    TEMPLATES = {
      question_static_social_page: File.join('static_social_pages', 'question.html.erb')
    }

    def initialize(data)
      @result = ''
      @data = data
    end

    def render_template(template_identifier)
      template = File.read(template_file_path(template_identifier))
      render(template, return_binding)
    end

    protected
    # open to new implementations, such using OpenStruct.new(@data).instance_eval { binding }
    def return_binding
      _binding = Object.new.instance_eval { binding }
      _binding.local_variable_set(:data, @data)

      _binding
    end

    def template_file_path(template_identifier)
      Rails.root.join('app', 'views', TEMPLATES[template_identifier.to_sym] || '')
    end

    def render(template, _binding)
      ERB.new(template).result(_binding)
    end

    public
    def self.render_template(template_identifier, data)
      TemplateRenderer.new(data).render_template(template_identifier)
    end
  end
end
