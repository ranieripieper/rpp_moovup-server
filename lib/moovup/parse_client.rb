module Moovup
  module ParseClient

    module_function

    def client
      @parse_client ||= ::Parse.create(client_data)
    end

    def client_data
      {
        application_id: Moovup::Config.parse_app_id,
        api_key:        Moovup::Config.parse_api_key,
        master_key:     Moovup::Config.parse_master_key,
        quiet:          Moovup::Config.parse_quiet || false
      }
    end

  end
end
