namespace :moovup do
  task :statsetup do

    require 'rails/code_statistics'

    ::STATS_DIRECTORIES << ["Serializers", "app/serializers"]
    ::STATS_DIRECTORIES << ["Services",    "app/services"]
    ::STATS_DIRECTORIES << ["Uploaders",   "app/uploaders"]
    ::STATS_DIRECTORIES << ["Validators",  "app/validators"]
    ::STATS_DIRECTORIES << ["Workers",     "app/workers"]
    ::STATS_DIRECTORIES << ["Grape",       "app/grape"]
    ::STATS_DIRECTORIES << ["Lib",         "lib/"]

    # For test folders not defined in CodeStatistics::TEST_TYPES (ie: spec/)
    ::STATS_DIRECTORIES << ["Specs", "spec/"]

    CodeStatistics::TEST_TYPES << "Services specs"
  end
end

task :stats => "moovup:statsetup"
