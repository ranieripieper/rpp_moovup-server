# tasks/db/clean.rb

namespace :db do

  desc "Truncate all existing data"
  task :truncate => :environment do
  	options = {}
  	tables  = ENV['TABLES'].present? ? ENV['TABLES'].split(",").map(&:squish) : nil
  	options[:only] = tables if tables

  	DatabaseCleaner.clean_with :truncation, options
  end

end
