namespace :api do
  def puts_api_endpoint(endpoint)
    method = endpoint.route_method.ljust(10)
    path = endpoint.route_path.gsub(":version", endpoint.route_version)

    if ENV['DESCRIPTION'].present?
      description = endpoint.route_description
      if description
        puts ">> #{description}"
      else
        puts ">> [No Description for this endpoint]"
      end
    end

    puts "     #{method} #{path}"
  end

  desc "API Routes"
  task :routes => :environment do
    routes = API::Base.routes

    if ENV['GROUPED']
      routes.group_by { |endpoint| endpoint.route_method }.each do |method, endpoints|
        endpoints.each do |endpoint|
          puts_api_endpoint(endpoint)
        end
      end
    else
      routes.each do |endpoint|
        puts_api_endpoint(endpoint)
      end
    end

  end
end
