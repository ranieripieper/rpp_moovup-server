namespace :cron_jobs do
	namespace :async do
		desc "Send push notifications async"
		task :send_questions_to_answer_push_notifications => :environment do
			Moovup::V1::QuestionsToAnswerWorker.perform_async
		end

		desc "Send invite friend push notifications async"
		task :send_invite_push_notifications => :environment do
			Moovup::V1::InviteFriendPushNotificationDeliveryWorker.perform_async
		end
	end

	namespace :sync do
		desc "Send push notifications sync"
		task :send_questions_to_answer_push_notifications => :environment do
			Moovup::V1::QuestionsToAnswerWorker.new.perform
		end
	end
end
