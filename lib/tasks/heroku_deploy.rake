require_relative '../rake_heroku_deployer'

namespace :deploy do

	DEPLOY_ENVIRONMENTS = [:staging, :production]

	DEPLOY_ENVIRONMENTS.each do |environment|
		namespace environment do
			task :migrations do
				deployer = RakeHerokuDeployer.new(environment)
				deployer.run_migrations
			end

			task :rollback do
				deployer = RakeHerokuDeployer.new(environment)
				deployer.rollback
			end
		end

		task environment do
			deployer = RakeHerokuDeployer.new(environment)
			deployer.deploy
		end
	end

	task :all do
		DEPLOY_ENVIRONMENTS.each {|env| Rake::Task["deploy:#{env}"].invoke }
	end
end
