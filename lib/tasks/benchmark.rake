require 'benchmark'

namespace :benchmark do

	# Perfoming conditions in WHERE
	QUERY = <<-QUERY
	  SELECT questions.* FROM questions
	    INNER JOIN categories c ON c.id = questions.category_id
	  WHERE questions.deleted_at IS NULL AND questions.user_id != %{user_id} AND (
	    c.id IN (SELECT hobby_id FROM users WHERE id = %{user_id}) OR
	    c.id IN (SELECT DISTINCT(category_id) FROM goals WHERE goals.user_id = %{user_id})
	  )
		ORDER BY questions.answers_count DESC,
					   questions.upvotes_count DESC,
					   questions.created_at DESC
	QUERY

	# Perfoming conditions in WHERE with UNION
	QUERY_1 = <<-QUERY_1
	  SELECT questions.* FROM questions
	    INNER JOIN categories c ON c.id = questions.category_id
	  WHERE questions.deleted_at IS NULL AND questions.user_id != %{user_id} AND c.id IN (
	    (
	    	(SELECT hobby_id FROM users WHERE id = %{user_id}) UNION
				(SELECT DISTINCT(category_id) FROM goals WHERE goals.user_id = %{user_id})
			)
	  )
		ORDER BY questions.answers_count DESC,
	  				 questions.upvotes_count DESC,
	  				 questions.created_at DESC
	QUERY_1

	# Performing conditions in INNER JOIN
	QUERY_2 = <<-QUERY_2
	  SELECT questions.* FROM questions
	    INNER JOIN categories c ON (
	      questions.category_id = c.id AND
	      ( c.id IN (SELECT hobby_id FROM users WHERE id = %{user_id}) OR
	        c.id IN (SELECT DISTINCT(category_id) FROM goals WHERE goals.user_id = %{user_id})
	      )
	    )
	  WHERE questions.deleted_at IS NULL AND questions.user_id != %{user_id}
	  ORDER BY questions.answers_count DESC,
	  				 questions.upvotes_count DESC,
						 questions.created_at DESC
	QUERY_2

	# Performing conditions in INNER JOIN with UNION
	QUERY_3 = <<-QUERY_3
	  SELECT questions.* FROM questions
	    INNER JOIN categories c ON (
	      questions.category_id = c.id AND c.id IN (
	        SELECT hobby_id FROM users WHERE id = %{user_id} UNION
	        SELECT DISTINCT(category_id) FROM goals WHERE goals.user_id = %{user_id}
	      ))
	  WHERE questions.deleted_at IS NULL AND questions.user_id != %{user_id}
    ORDER BY questions.answers_count DESC,
    				 questions.upvotes_count DESC,
  					 questions.created_at DESC
	QUERY_3


	def test_query(query, user_id, remove_distinct = false)
		query = query % {user_id: user_id}

		query.gsub!(/DISTINCT\((\w+)\)/, '//1') if remove_distinct

		ActiveRecord::Base.connection.execute(query).to_a
	end

  task :feed_query => :environment do

  	num_loops = (ENV['LOOPS'].presence || 500).to_i

  	def remove_distinct?
  		ENV['REMOVE_DISTINCT'].present?
  	end

  	Benchmark.bm(15) do |x|
	    x.report('QUERY') {
	      num_loops.times {
	      	test_query(QUERY, 1, remove_distinct?)
	      }
	    }

	    x.report('QUERY_1') {
	      num_loops.times {
	      	test_query(QUERY_1, 1, remove_distinct?)
	      }
	    }

	    x.report('QUERY_2') {
	      num_loops.times {
	      	test_query(QUERY_2, 1, remove_distinct?)
	      }
	    }

	    x.report('QUERY_3') {
	      num_loops.times {
	      	test_query(QUERY_3, 1, remove_distinct?)
	      }
	    }
	  end
  end
end
