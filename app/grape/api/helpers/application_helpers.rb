# encoding: UTF-8

module API
  module Helpers
    module ApplicationHelpers

      extend ActiveSupport::Concern

      VALID_LOCALES = [
        :en,
        :"pt-BR"
      ]

      DEFAULT_LOCALE = "pt-BR".to_sym

      ALLOWED_PAGINATION_PER_PAGE = (1..10).map { |value| value * 10 }

      included do
        def self.with_cacheable_endpoints(namespace_name, &block)
          namespace namespace_name, &block

          cached_namespace_name = _cached_namespace(namespace_name)

          namespace cached_namespace_name do
            namespace namespace_name, &block
          end
        end

        def self._cached_namespace(namespace_name = :nil)
          :cached
        end

        def self.paginated_endpoint(paginate_options={}, &block)

          paginate_options = {
            per_page: 30,
            max_per_page: 60
          }.merge(paginate_options)

          paginate paginate_options

          params do
            optional :page, type: Integer
            optional :per_page, type: Integer, values: ALLOWED_PAGINATION_PER_PAGE
          end

          yield block
        end

        helpers do
          def serializer(serializer)
            serializer_by_key_name(serializer).constantize
          end

          def presenter(presenter)
            presenter_by_key_name(presenter).constantize
          end

          def serializer_by_key_name(serializer_name)
            version = env['rack.routing_args'][:version] rescue 'v1'

            serializer_name = get_serializer_name_for_request(serializer_name)

            "Moovup/#{version}/#{serializer_name}_serializer".camelize
          end

          def get_serializer_name_for_request(serializer_name)
            if params[:_s].present?
              serializer_type = params[:_s].to_s.downcase == 'simple' ? 'simple' : ''

              if serializer_type == 'simple'
                serializer_name = [serializer_type, serializer_name.to_s.sub(/\Asimple_/, '')].join('_')
              end
            end

            serializer_name
          end

          def presenter_by_key_name(presenter_name)
            version = env['rack.routing_args'][:version] rescue 'v1'

            "Moovup/#{version}/#{presenter_name}_presenter".camelize
          end

          def serialized_object(object, options)
            serializer = serializer(options.delete(:serializer))
            options.merge!(scope: current_user)

            serializer.new(object, options)
          end

          def serialized_array(collection, options = {})
            serializer = serializer(options.delete(:serializer))
            options    = options.merge(each_serializer: serializer, scope: current_user)

            ActiveModel::ArraySerializer.new(collection, options)
          end

          def paginated_serialized_array(collection, options = {})
            collection = paginate_array(collection) if options[:paginate]

            if options[:skip_pagination_meta].blank?
              options.merge!(meta: pagination_meta)
              options[:meta].merge!(options.delete(:meta_extra) || {})
            end

            options = options.merge(options)

            serialized_array(collection, options)
          end

          def in_sandbox_environment?
            %w(development staging).member?(Rails.env.to_s)
          end

          def set_locale
            I18n.locale = current_locale
          end

          def authenticate_user
            unless current_user
              response = token_authentication_error_response
              error!(response, response[:status_code])
            end
          end

          def current_user
            return @current_user if @current_user

            service = token_authentication_service
            service.execute

            @current_user = service.try(:user)
          end

          def authentication_token
            params[:auth_token] || headers['X-Token']
          end

          def authentication_provider
            params[:auth_provider] || params[:provider] || headers['X-Provider']
          end

          def locale_from_request
            params[:locale] || headers['X-Locale']
          end

          def current_locale
            locale       = locale_from_request || DEFAULT_LOCALE
            valid_locale = VALID_LOCALES.member?(locale.to_sym)

            valid_locale ? locale : I18n.default_locale
          end

          def current_ip
            env['HTTP_X_FORWARDED_FOR'] || env['REMOTE_ADDR']
          end

          def origin_object
            @origin ||= {
              provider: authentication_provider,
              ip: current_ip,
              user_agent: env['HTTP_USER_AGENT'],
              locale: current_locale
            }
          end

          def set_origin
            params.merge!(origin: origin_object)
          end

          def authentication_service
            @authentication_service ||= initialize_service('AuthenticationCreateService',
              params.delete(:email),
              params.delete(:password),
              params
            )
          end

          def token_authentication_service
            @token_authentication_service ||= initialize_service('TokenAuthenticationCreateService',
              authentication_token,
              authentication_provider
            )
          end

          def token_authentication_error_response
            {
              error: true,
              status_code: token_authentication_service.response_status,
              errors: token_authentication_service.errors
            }
          end

          def pagination_meta
            { pagination: params[:pagination_meta] }
          end

          def paginate(collection)
            paginate_response = super
            set_pagination_meta_params(paginate_response)
            paginate_response
          end

          def paginate_array(array)
            paginate_response = Kaminari.paginate_array(array)
            paginate(paginate_response)
          end

          def set_pagination_meta_params(data)
            pagination_data = {
              total_count: data.total_count,
              total_pages: data.num_pages,
              current_page: data.current_page,
              next_page: data.next_page,
              prev_page: data.prev_page,
              per_page: params[:per_page].to_i
            }

            params[:pagination_meta] = pagination_data
          end

          def cache
            Moovup::Cache
          end

          def options_for_cache(options)
            return { replace_data: options.values } if options.is_a?(Hash)
            return { replace_data: options   } if options.is_a?(Array)
            return { replace_data: [options] } unless options.is_a?(Array)

            options
          end

          def cache_enabled?(options = {})
            (
              options.is_a?(Hash) && options.delete(:force_cache) == true ||
              request.fullpath.match(%r{/cached/}) ||
              params[:_cache].present? ||
              Moovup::Config.enabled?(:cache_enabled)
            )
          end

          def respond_with_cacheable(key, options = {}, &block)
            unless cache_enabled?(options)
              return block.call
            end

            set_cacheable_header
            from_cache(key, options, &block)
          end

          def set_cacheable_header
            header 'X-Cached', 'cached'
          end

          def from_cache(key, options = {}, &block)
            options = options_for_cache(options)
            cache.fetch(key, options, &block)
          end

          def pagination_cache_replace_data(resource_id = nil)
            [resource_id || params[:id], params[:page], params[:per_page]]
          end

          def response_for_paginated_endpoint(cache_key, cache_resource_id = nil, &block)
            cache_resource_id ||= current_user.try(:id)
            cache_replace_data = pagination_cache_replace_data(cache_resource_id)

            respond_with_cacheable(cache_key, cache_replace_data, &block)
          end

          def initialize_service(service_name, *options)
            version = env['rack.routing_args'][:version] rescue 'v1'
            service_class = "::Moovup::#{version.camelize}::#{service_name.camelize}".constantize

            service_class.send(:new, *options)
          end

          def execute_service(service_name, *options)
            service = initialize_service(service_name, *options)
            service.execute

            service
          end
        end
      end
    end
  end
end
