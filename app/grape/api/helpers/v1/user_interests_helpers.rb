# encoding: UTF-8

module API
  module Helpers
    module V1
      module UserInterestsHelpers

        extend Grape::API::Helpers

        def response_for_batch_service(service, method = :persisted?)
          interests = service.interests.compact

          response_append = interests.map do |interest|
            next if interest.blank?
            flag = interest.send(method)
            { interest.category_id => flag }
          end

          response_for_service(service, categories_response: response_append)
        end

      end
    end
  end
end
