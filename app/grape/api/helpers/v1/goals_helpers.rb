# encoding: UTF-8

module API
  module Helpers
    module V1
      module GoalsHelpers

        extend Grape::API::Helpers

        def serialized_goal(goal, options = {})
          options = { serializer: :goal }.merge(options)
          serialized_object(goal, options)
        end

        def paginated_serialized_goals_for(user, options = {})
          options = {
            serializer: :current_user_goal,
            root: :goals
          }.merge(options)

          scope = options[:only_public] ? user.public_goals : user.goals

          paginated_goals = paginate(scope.includes(:category, :activities))

          paginated_serialized_array(paginated_goals, options).as_json
        end

      end
    end
  end
end
