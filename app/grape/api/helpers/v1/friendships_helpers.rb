# encoding: UTF-8

module API
  module Helpers
    module V1
      module FriendshipsHelpers

        extend Grape::API::Helpers

        def serialized_friendship(friendship, options = {})
          options = { serializer: :friendship }.merge(options)
          serialized_object(friendship, options)
        end

        def friendship_service_update_response(status, user, options = {})
          options.merge!(status: Friendship::STATUS[status])

          friendship = user.pending_requested_friendships.find_by(user_id: options[:user_id])

          service = execute_service('Friendships::UpdateService', friendship, user, options)
          response_for_update_service(service, :friendship)
        end

        def response_for_batch_delete_service(service)
          response_append = service.friendships.map do |friendship|
            { friendship.friend_id => friendship.canceled? }
          end

          response_for_service(service, friends_response: response_append)
        end
      end
    end
  end
end
