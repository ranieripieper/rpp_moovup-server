# encoding: UTF-8

module API
  module Helpers
    module V1
      module ActivitiesHelpers

        extend Grape::API::Helpers

        def serialized_activity(activity, options = {})
          options = { serializer: :activity }.merge(options)
          serialized_object(activity, options)
        end

        def paginated_activities_for(user, options = {})
          options = {
            serializer: :activity,
            root: :activities
          }.merge(options)

          activities_method = options[:only_public] ? :public_activities : :activities
          paginated_activities = paginate(user.send(activities_method))
                                  .with_user_likes(current_user.id)
                                  .includes(:user, :goal)

          paginated_serialized_array(paginated_activities, options).as_json
        end

      end
    end
  end
end
