# encoding: UTF-8

module API
  module Helpers
    module V1
      module AnswersHelpers

        extend Grape::API::Helpers

        def serialized_answer(answer, options = {})
          options = { serializer: :answer }.merge(options)
          serialized_object(answer, options)
        end

        def merge_params_to_create_answer(answer_params)
          answer_params.merge!(question_id: params[:question_id])
        end

        def merge_params_to_create_comment(answer_params)
          answer_params.merge!(
            question_id: nil,
            parent_id: params[:id]
          )
        end

      end
    end
  end
end
