# encoding: UTF-8

module API
  module Helpers
    module V1
      module CarrersHelpers

        extend Grape::API::Helpers

        def serialized_carrer(carrer, options = {})
          options = { serializer: :carrer }.merge(options)
          serialized_object(carrer, options)
        end

      end
    end
  end
end
