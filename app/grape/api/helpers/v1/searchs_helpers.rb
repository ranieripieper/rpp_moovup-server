# encoding: UTF-8

module API
  module Helpers
    module V1
      module SearchsHelpers

        extend Grape::API::Helpers

        def profile_types_for_search(params)
          profile_types = []

          valid_profiles_types = User::VALID_PROFILES_TYPES.values

          if params[:profile_types].present?
            profile_types = params[:profile_types]
                            .split(',')
                            .map(&:squish)
                            .map(&:parameterize)
          end

          profile_types.keep_if {|p_t| valid_profiles_types.member?(p_t) }
        end

        def cache_data_for_user_search(profile_types, params)
          replace_data = [
            params[:q].parameterize,
            (profile_types.compact.presence || ['all']).join('_'),
            params[:page],
            params[:per_page]
          ].compact
        end

      end
    end
  end
end
