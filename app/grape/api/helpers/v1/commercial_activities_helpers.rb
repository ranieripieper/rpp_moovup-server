# encoding: UTF-8

module API
  module Helpers
    module V1
      module CommercialActivitiesHelpers

        extend Grape::API::Helpers

        def serialized_commercial_activity(commercial_activity, options = {})
          options = { serializer: :commercial_activity }.merge(options)
          serialized_object(commercial_activity, options)
        end

        def paginated_commercial_activities(options = {})
          options = {
            serializer: :commercial_activity,
            root: :commercial_activities
          }.merge(options)

          paginated_activities = paginate(CommercialActivity.all)
          paginated_serialized_array(paginated_activities, options).as_json
        end

      end
    end
  end
end
