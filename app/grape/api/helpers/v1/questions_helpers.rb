# encoding: UTF-8

module API
  module Helpers
    module V1
      module QuestionsHelpers

        extend Grape::API::Helpers

        def serialized_question(question, options = {})
          options = { serializer: :question, root: :question }.merge(options)
          serialized_object(question, options)
        end

        def paginated_questions(questions, options = {})
          options = {
            serializer: :question,
            root: :questions
          }.merge(options)

          paginated_questions = paginate(questions)
          paginated_serialized_array(paginated_questions, options).as_json
        end

        def paginated_questions_feed_for_user(user, options = {})
          questions = user
                      .questions_feed_for_user
                      .with_user_favorited(user.id)
                      .includes(:user, :category)

          # unless questions.any?
          #   questions = user.questions_to_reply_feed.includes(:user, :category)
          # end

          paginated_questions(questions, options)
        end

        def paginated_questions_to_reply_feed_for_user(user, options = {})
          max_default = options.delete(:max_default)
          questions = user
                      .questions_to_reply_feed(max_default)
                      .with_user_favorited(user.id)
                      .includes(:user, :category)

          paginated_questions(questions, options)
        end

        def render_static_social_page_for(question)
          data = social_data_for_question(question)

          return '' if data.blank?

          Moovup::TemplateRenderer.render_template(:question_static_social_page, data).html_safe
        end

        def social_data_for_question(question)
          return {} unless question

          {
            page_title: question.body_text,
            title: question.body_text,
            site: Moovup::Config.social_share_site_url,
            site_name: Moovup::Config.social_share_site_name,
            description: Moovup::Config.social_share_default_description,
            image: question.social_share_image_url,
            url: "#{Moovup::Config.social_share_site_url}/pergunta/#{question.to_param_social}",
            # plataform specific
            facebook: {},
            twitter: {}
          }
        end

      end
    end
  end
end
