# encoding: UTF-8

module API
  module Helpers
    module V1
      module SharedServiceActionsHelpers

        extend Grape::API::Helpers

        def new_activity_service_response(user, options = {})
          service = execute_service('Activities::CreateService', user, options)
          response_for_create_service(service, :activity)
        end
      end
    end
  end
end
