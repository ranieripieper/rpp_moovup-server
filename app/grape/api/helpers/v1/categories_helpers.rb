# encoding: UTF-8

module API
  module Helpers
    module V1
      module CategoriesHelpers

        extend Grape::API::Helpers

        def serialized_category(category, options = {})
          options = { serializer: :category }.merge(options)
          serialized_object(category, options)
        end

        def paginated_categories(categories, options = {})
          paginated = paginate(categories)

          ignore_children = options[:each_serializer_meta].try(:fetch, :ignore_children, false)
          paginated.includes(children: [:children, :parent]) unless ignore_children

          paginated
        end

        def paginated_serialized_categories(categories, options = {})
          paginate_categories = paginated_categories(categories, options)

          options = {
            serializer: :category,
            root: :categories
          }.merge(options)

          paginated_serialized_array(paginate_categories, options)
        end
      end
    end
  end
end
