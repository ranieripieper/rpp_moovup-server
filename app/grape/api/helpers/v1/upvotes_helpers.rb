# encoding: UTF-8

module API
  module Helpers
    module V1
      module UpvotesHelpers

        extend Grape::API::Helpers

        def response_for_upvote_service(service)
          resource_type = service.send(:resource_type).to_s.underscore

          response = { upvotes_count: service.upvotes_count }

          service_response = response_for_create_service(service, resource_type).as_json

          response.merge(service_response)
        end

      end
    end
  end
end
