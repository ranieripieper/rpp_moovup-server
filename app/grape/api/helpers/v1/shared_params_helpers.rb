# encoding: UTF-8

module API
  module Helpers
    module V1
      module SharedParamsHelpers

        extend Grape::API::Helpers

        params :new_activity do
          requires :activity, type: Hash do
             requires :date
             requires :total
             optional :body_text
             optional :image
          end
        end

      end
    end
  end
end
