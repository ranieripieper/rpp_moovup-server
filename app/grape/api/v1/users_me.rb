# encoding: UTF-8

module API
  module V1
    class UsersMe < API::V1::Base

      helpers API::Helpers::V1::UsersHelpers
      helpers API::Helpers::V1::SharedParamsHelpers
      helpers API::Helpers::V1::SharedServiceActionsHelpers

      before do
        authenticate_user
      end

      namespace :users do
        namespace :me do

          desc 'Suspend user account'
          delete :account do
            service = execute_service('Users::DeleteService', current_user, current_user)
            simple_response_for_service(service)
          end

          desc 'Update user data'
          put do
            service = execute_service('Users::UpdateService', current_user, current_user, params)
            response_for_update_service(service, :user, serializer: :current_user, root: :user_data)
          end

          desc 'Flag notification as read'
          put '/notifications/:id' do
            notification = current_user.notifications.unread.find_by(id: params[:id])

            if notification.try(:mark_as_read)
              status 204
            else
              status 403
            end
          end

          desc 'Updates user profile picture'
          put :picture do
            service = execute_service('Users::ProfileImageUpdateService', current_user, current_user, params)

            response_for_update_service(service, :user)
          end

          desc 'Create a new device in Parse'
          params do
            requires :device, type: Hash do
              requires :token, type: String
              requires :platform, type: String, values: ::UserDevice::VALID_PLATFORMS.values
            end
          end
          post :devices do
            service = execute_service('Parse::DeviceCreateService', current_user, params)

            simple_response_for_service(service)
          end

          desc 'Update current user preferences'
          put :preferences do
            service = execute_service('Users::PreferencesUpdateService', current_user, current_user, params)
            response_for_update_service(service, :user, serializer: :current_user)
          end
        end
      end
    end
  end
end
