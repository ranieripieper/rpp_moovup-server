# encoding: UTF-8

module API
  module V1
    class Activities < API::V1::Base

      helpers API::Helpers::V1::ActivitiesHelpers
      helpers API::Helpers::V1::SharedParamsHelpers
      helpers API::Helpers::V1::SharedServiceActionsHelpers
      helpers API::Helpers::V1::UpvotesHelpers

      before do
        authenticate_user
      end

      namespace :activities do
        params do
          use :new_activity
          group :activity, type: Hash do
            requires :goal_id
          end
        end

        desc 'Create a new activity'
        post do
          new_activity_service_response(current_user, params)
        end

        route_param :id do
          desc 'Update a Activity'
          put do
            activity = current_user.activities.find_by(id: params[:id])
            service = execute_service('Activities::UpdateService', activity, current_user, params)
            response_for_update_service(service, :activity)
          end

          desc 'Delete a Activity'
          delete do
            activity = current_user.activities.find_by(id: params[:id])
            service = execute_service('Activities::DeleteService', activity, current_user, params)
            response_for_delete_service(service, :activity)
          end

          namespace :upvote do
            post do
              params[:upvote] = { resource_id: params[:id] }

              service = execute_service('Activities::UpvoteCreateService', current_user, params)
              response_for_upvote_service(service)
            end

            delete do
              upvote = current_user.activities_upvotes.find_by(upvotable_id: params[:id])

              service = execute_service('Upvotes::DeleteService', upvote, current_user, params)
              response_for_upvote_service(service)
            end
          end
        end
      end
    end
  end
end
