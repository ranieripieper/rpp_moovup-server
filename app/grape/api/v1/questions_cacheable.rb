# encoding: UTF-8

module API
  module V1
    class QuestionsCacheable < API::V1::Base

      helpers API::Helpers::V1::QuestionsHelpers
      helpers API::Helpers::V1::SharedParamsHelpers
      helpers API::Helpers::V1::SharedServiceActionsHelpers

      before do
        authenticate_user
      end

      with_cacheable_endpoints :questions do
        route_param :id do
          desc 'Get a question'
          get do
            question = Question.includes(answers: [:user]).find_by(id: params[:id])
            if question
              respond_with_cacheable('question.show', params[:id]) do
                serialized_question(question).as_json
              end
            else
              not_found_error_response(:questions)
            end
          end
        end
      end
    end
  end
end
