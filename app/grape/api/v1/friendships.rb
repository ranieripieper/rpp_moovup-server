# encoding: UTF-8

module API
  module V1
    class Friendships < API::V1::Base

      helpers API::Helpers::V1::FriendshipsHelpers

      before do
        authenticate_user
      end

      namespace :friendships do
        params do
          requires :friend_id
        end

        desc 'send friend request'
        post do
          service = execute_service('Friendships::CreateService', current_user, params)
          response_for_create_service(service, :friendship)
        end

        desc 'delete friends in batch'
        params do
          requires :friend_ids
        end
        delete do
          service = execute_service('Friendships::BatchDeclineService', current_user, params)
          response_for_batch_delete_service(service)
        end
      end


      namespace :friends do
        route_param :user_id do
          desc "Decline friendship request"
          delete :decline do
            friendship_service_update_response(:declined, current_user, params)
          end

          desc "Accept friendship request"
          post :accept do
            friendship_service_update_response(:accepted, current_user, params)
          end
        end

      end
    end
  end
end
