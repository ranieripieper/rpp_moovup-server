# encoding: UTF-8

module API
  module V1
    class QuestionsActions < API::V1::Base

      helpers API::Helpers::V1::UpvotesHelpers
      helpers API::Helpers::V1::QuestionsHelpers
      helpers API::Helpers::V1::AnswersHelpers

      before do
        authenticate_user
      end

      namespace :questions do
        route_param :id do

          namespace :upvote do
            post do
              params[:upvote] = { resource_id: params[:id] }

              service = execute_service('Questions::UpvoteCreateService', current_user, params)
              response_for_upvote_service(service)
            end

            delete do
              upvote = current_user.questions_upvotes.find_by(upvotable_id: params[:id])

              service = execute_service('Upvotes::DeleteService', upvote, current_user, params)
              response_for_service(service, upvotes_count: service.upvotes_count)
            end
          end

          desc 'Add question to user blacklist'
          post :ignore do
            params.merge!(question_id: params[:id])

            service = execute_service('IgnoredQuestions::CreateService', current_user, params)
            response_for_create_service(service, :question, serializer: :simple_question)
          end

          namespace :answers do
            route_param :answer_id do
              namespace :upvote do
                post do
                  question = Question.find_by(id: params[:id])

                  if question
                    answer = question.answers.find_by(id: params[:answer_id])

                    params.merge!(resource: answer)

                    service = execute_service('Answers::UpvoteCreateService', current_user, params)
                    response_for_upvote_service(service)
                  else
                    not_found_error_response(:questions)
                  end
                end

                delete do
                  upvote = current_user.answers_upvotes.find_by(upvotable_id: params[:answer_id])
                  service = execute_service('Upvotes::DeleteService', upvote, current_user, params)

                  response_for_service(service, upvotes_count: service.upvotes_count)
                end
              end
            end
          end
        end
      end
    end
  end
end
