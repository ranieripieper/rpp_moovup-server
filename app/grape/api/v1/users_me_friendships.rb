# encoding: UTF-8

module API
  module V1
    class UsersMeFriendships < API::V1::Base

      helpers API::Helpers::V1::FriendshipsHelpers
      helpers API::Helpers::V1::UsersHelpers

      before do
        authenticate_user
      end

      with_cacheable_endpoints :users do
        namespace :me do

          [:friends, :pending_requested_friends, :requested_friends].each do |namespace_name|
            namespace namespace_name do
              paginated_endpoint do
                desc "get all #{namespace_name} from user"
                get do
                  response_for_paginated_endpoint "current_user.#{namespace_name}" do

                    paginated_friends = paginate(current_user.send(namespace_name))

                    if namespace_name == :friends
                      paginated_friends = paginated_friends
                                          .includes(current_public_goal: [:category, :current_activity])
                                          .order_by_last_activity_for_user(current_user.id)
                    end

                    options = {
                      serializer: namespace_name == :friends ? :friend : :simple_user,
                      root: :friends
                    }

                    paginated_serialized_array(paginated_friends, options).as_json
                  end
                end
              end

              route_param :id do
                desc "Get a #{namespace_name}"
                get do
                  user = current_user.send(namespace_name).find_by(id: params[:id])

                  if user
                    respond_with_cacheable("current_user.#{namespace}_show", [current_user.id, params[:id]]) do
                    serialized_user(user, serializer: :friend).as_json
                    end
                  else
                    not_found_error_response(:friends)
                  end
                end
              end
            end
          end
        end
      end
    end
  end
end
