# encoding: UTF-8

module API
  module V1
    class Base < API::Base

      version 'v1'

      helpers API::Helpers::V1::ApplicationHelpers
      include API::Helpers::V1::ErrorsHelpers

      mount API::V1::Activities
      mount API::V1::Answers
      mount API::V1::AnswersCacheable

      mount API::V1::CarrersCacheable
      mount API::V1::CategoriesCacheable
      mount API::V1::Cities
      mount API::V1::CommercialActivitiesCacheable

      mount API::V1::FavoritedQuestions
      mount API::V1::Friendships

      mount API::V1::Goals

      mount API::V1::Questions
      mount API::V1::QuestionsActions
      mount API::V1::QuestionsCacheable
      mount API::V1::QuestionsPublicCacheable

      mount API::V1::Reports
      mount API::V1::Searchs
      mount API::V1::States

      mount API::V1::UsersAuth
      mount API::V1::UsersInterests
      mount API::V1::UsersMe
      mount API::V1::UsersMeCacheable
      mount API::V1::UsersMeFriendships
      mount API::V1::Users
      mount API::V1::UsersCacheable

      add_swagger_documentation(
        base_path: "/api",
        hide_format: true,
        hide_documentation_path: true,
        api_version: 'v1'
      )

    end
  end
end
