# encoding: UTF-8

module API
  module V1
    class Users < API::V1::Base

      helpers API::Helpers::V1::ActivitiesHelpers
      helpers API::Helpers::V1::UsersHelpers

      namespace :users do
        desc 'Create a new user'
        params do
          requires :user, type: Hash do
            requires :email
            requires :profile_type, values: User::VALID_PROFILES_TYPES.values
            requires :password
            requires :password_confirmation

            optional :hobby_id
            optional :commercial_activity_id

            mutually_exclusive :hobby_id, :commercial_activity_id

            given :hobby_id do
              requires :first_name, type: String
              requires :carrer_id, type: Integer
              optional :last_name, type: String
              optional :gender, values: User::VALID_GENDERS.values
            end

            given :commercial_activity_id do
              requires :company_name, type: String
              optional :foundation_date
            end

          end

          requires :provider, values: Authorization::PROVIDERS.keys.map(&:to_s)
        end

        post do
          service = execute_service('Users::CreateService', nil, params)

          if service.success?
            response = user_success_response_for_service(service)
          else
            status service.response_status
            response = error_response_for_service(service)
          end

          response
        end

        desc 'Check if user with given mail exists'
        params do
          requires :email, type: String, regexp: User::EMAIL_REGEXP
        end

        get :check_email do
          u = User.find_by(email: params[:email].squish)
          response_status = u.present? ? 200 : 404
          status response_status

          {
            status_code: response_status,
            user_exists: response_status == 200
          }

        end

        desc 'Activate an user account'
        params do
          requires :token, type: String, allow_blank: false
        end
        post :activate_account do
          user = User.not_activated.find_by(activation_token: params[:token])

          service = execute_service('Users::ActivateAccountService', user, user, params)
          response_extra = {
            user_name: service.user_name,
            yet_activated: service.yet_activated?(params[:token])
          }

          response_for_service(service, response_extra, true)
        end

        desc 'Resend account confirmation mail'
        params do
          requires :email, type: String, allow_blank: false
        end
        post :resend_activation_mail do
          user = User.not_activated.find_by(email: params[:email])

          service = execute_service('Users::ActivateAccountMailerDeliveryService', user, params)
          simple_response_for_service(service)
        end
      end

    end
  end
end
