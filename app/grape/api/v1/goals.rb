# encoding: UTF-8

module API
  module V1
    class Goals < API::V1::Base

      helpers API::Helpers::V1::UpvotesHelpers
      helpers API::Helpers::V1::GoalsHelpers
      helpers API::Helpers::V1::SharedParamsHelpers
      helpers API::Helpers::V1::SharedServiceActionsHelpers

      before do
        authenticate_user
      end

      namespace :goals do
        params do
          requires :goal, type: Hash do
            requires :category_id

            optional :periodicity_type, values: Goal::PERIODICITY_TYPE.values.map(&:to_s)
            optional :total
            optional :public, default: true
            optional :target_date

            given :target_date do
              requires :target
              requires :current
            end
          end
        end

        desc 'Create a new goal'
        post do
          service = execute_service('Goals::CreateService', current_user, params)
          response_for_create_service(service, :goal)
        end

        route_param :id do

          desc 'Update a Goal'
          put do
            goal = current_user.goals.find_by(id: params[:id])
            service = execute_service('Goals::UpdateService', goal, current_user, params)
            response_for_update_service(service, :goal)
          end

          desc 'Delete a Goal'
          delete do
            goal = current_user.goals.find_by(id: params[:id])
            service = execute_service('Goals::DeleteService', goal, current_user, params)
            response_for_delete_service(service, :goal)
          end

          params do
            use :new_activity
          end

          desc 'Create a new activity'
          post :activities do
            params[:activity].merge!(goal_id: params[:id])
            new_activity_service_response(current_user, params)
          end

          namespace :upvote do
            post do
              params[:upvote] = { resource_id: params[:id] }

              service = execute_service('Goals::UpvoteCreateService', current_user, params)
              response_for_upvote_service(service)
            end

            delete do
              upvote = current_user.goals_upvotes.find_by(upvotable_id: params[:id])

              service = execute_service('Upvotes::DeleteService', upvote, current_user, params)
              response_for_upvote_service(service)
            end
          end


        end
      end
    end
  end
end
