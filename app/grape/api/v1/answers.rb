# encoding: UTF-8

module API
  module V1
    class Answers < API::V1::Base

      helpers API::Helpers::V1::AnswersHelpers
      helpers API::Helpers::V1::QuestionsHelpers

      before do
        authenticate_user
      end

      namespace :questions do

        route_param :question_id do
          namespace :answers do

            params do
              requires :answer, type: Hash do
                requires :body_text
              end
            end

            desc 'Create a new answer'
            post do
              merge_params_to_create_answer(params[:answer])
              params[:answer].merge!(question_id: params[:question_id])

              service = execute_service('Answers::CreateService', current_user, params)
              response_for_create_service(service, :answer)
            end

            route_param :id do
              desc 'Update a answer'
              put do
                params.merge!(question_id: params[:question_id])
                answer = current_user.answers.find_by(id: params[:id])

                service = execute_service('Answers::UpdateService', answer, current_user, params)
                response_for_update_service(service, :answer)
              end

              desc 'Delete a answer'
              delete do
                answer = current_user.answers.find_by(id: params[:id])

                service = execute_service('Answers::DeleteService', answer, current_user, params)
                response_for_delete_service(service, :answer)
              end

              post :comments do
                merge_params_to_create_comment(params[:answer])

                service = execute_service('Answers::CreateService', current_user, params)
                response_for_create_service(service, :answer)
              end
            end
          end
        end
      end
    end
  end
end
