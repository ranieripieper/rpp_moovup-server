# encoding: UTF-8

module API
  module V1
    class UsersCacheable < API::V1::Base

      helpers API::Helpers::V1::ActivitiesHelpers
      helpers API::Helpers::V1::GoalsHelpers
      helpers API::Helpers::V1::UsersHelpers

      before do
        authenticate_user
      end

      with_cacheable_endpoints :users do
        route_param :id do

          desc 'get user profile data'
          get do
            user = User.includes(public_goals: [:category, :activities]).find_by(id: params[:id])

            if user
              respond_with_cacheable("users.show", [current_user.id, params[:id]]) do

                options = {
                  serializer: :public_user_profile,
                  meta: {
                    include_user_goals: current_user.friend_of?(user)
                  }
                }

                serialized_user(user, options).as_json
              end
            else
              not_found_error_response(:users)
            end
          end

          namespace :activities do
            paginated_endpoint do
              desc 'Get paginated activities for user'
              get do
                user = current_user.friends.find_by(id: params[:id])

                if user
                  response_for_paginated_endpoint 'users.activiries' do
                    paginated_activities_for(user, serializer: :current_user_activity, only_public: true)
                  end
                else
                  not_found_error_response(:friends)
                end
              end
            end

            route_param :activity_id do
              desc 'Get a activity for user'
              get do
                user = current_user.friends.find_by(id: params[:id])

                if user
                  activity = user.public_activities.find_by(id: params[:activity_id])

                  return not_found_error_response(:activities) unless activity

                  respond_with_cacheable('users.show_activity', params[:activity_id]) do
                    serialized_activity(activity).as_json
                  end
                else
                  not_found_error_response(:friends)
                end
              end
            end
          end

          namespace :goals do
            paginated_endpoint do
              desc 'Get paginated goals for user'
              get do
                user = current_user.friends.find_by(id: params[:id])

                if user
                  response_for_paginated_endpoint 'user.goals' do
                    paginated_serialized_goals_for(user, only_public: true, serializer: :user_goal)
                  end
                else
                  not_found_error_response(:friends)
                end

              end
            end

            route_param :goal_id do
              desc 'Get a goal of user'
              get do
                user = current_user.friends.find_by(id: params[:id])

                if user
                  goal = user.public_goals.find_by(id: params[:goal_id])

                  return not_found_error_response(:goals) unless goal

                  respond_with_cacheable('current_user.goal', params[:goal_id]) do
                    serialized_goal(goal, options).as_json
                  end
                else
                  not_found_error_response(:friends)
                end
              end

              namespace :reports do
                desc 'Goal report'
                get do
                  user = current_user.friends.find_by(id: params[:id])

                  if user
                    goal = user.public_goals.find_by(id: params[:goal_id])
                    return not_found_error_response(:goals) unless goal

                    serialized_object(goal, serializer: :goal_report)
                  else
                    not_found_error_response(:friends)
                  end
                end
              end

              namespace :activities do
                paginated_endpoint do
                  desc 'Get paginated activities for goal'
                  get do
                    user = current_user.friends.find_by(id: params[:id])

                    return not_found_error_response(:friends) unless user

                    goal = user.goals.includes(:user, :category).find_by(id: params[:goal_id])

                    return not_found_error_response(:goals) unless goal

                    paginated_activities_for(goal, serializer: :current_user_activity, root: :activities)
                  end
                end
              end
            end
          end
        end
      end
    end
  end
end
