# encoding: UTF-8

module API
  module V1
    class CarrersCacheable < API::V1::Base

      helpers API::Helpers::V1::CarrersHelpers

      with_cacheable_endpoints :carrers do
        paginated_endpoint do
          desc 'Get paginated carrers'
          get do
            response_for_paginated_endpoint 'carrers.all', 'all' do
              paginated_carrers = paginate(Carrer.all)
              paginated_serialized_array(paginated_carrers, serializer: :simple_carrer, root: :carrers).as_json
            end
          end
        end

        route_param :id do
          get do
            respond_with_cacheable('carrers.show', params[:id]) do
              carrer = Carrer.find_by(id: params[:id])
              if carrer
                options = {
                  serializer: :simple_carrer
                }
                serialized_carrer(carrer, options).as_json
              else
                not_found_error_response(:carrers)
              end
            end
          end
        end
      end
    end
  end
end
