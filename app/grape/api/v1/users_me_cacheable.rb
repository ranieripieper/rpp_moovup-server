# encoding: UTF-8

module API
  module V1
    class UsersMeCacheable < API::V1::Base

      helpers API::Helpers::V1::ActivitiesHelpers
      helpers API::Helpers::V1::GoalsHelpers
      helpers API::Helpers::V1::QuestionsHelpers
      helpers API::Helpers::V1::UsersHelpers


      before do
        authenticate_user
      end

      with_cacheable_endpoints :users do
        namespace :me do
          namespace :activities do
            paginated_endpoint do
              desc 'Get paginated activities'
              get do
                response_for_paginated_endpoint 'current_user.activities' do
                  paginated_activities_for(current_user, serializer: :current_user_activity)
                end
              end
            end

            route_param :id do
              desc 'Get a activity'
              get do
                respond_with_cacheable('current_user.activity', params[:id]) do
                  activity = current_user.activities.find_by(id: params[:id])
                  if activity
                    serialized_activity(activity).as_json
                  else
                    not_found_error_response(:activities)
                  end
                end
              end
            end
          end

          namespace :interests do
            desc 'Get current user interests'
            get do
              respond_with_cacheable 'current_user.interests', current_user.id do
                interests = current_user.interests.includes(:category)
                serialized_user_interests(interests, root: :interests).as_json
              end
            end
          end

          desc 'Get current user preferences'
          get :preferences do
            respond_with_cacheable('current_user.preferences', current_user.id) do
              current_user.create_default_preferences
              current_user.as_json(only: [:id, :preferences], root: nil)
            end
          end

          desc 'Get current logged in user data'
          get do
            respond_with_cacheable('current_user.show', current_user.id) do
              serialized_current_user(current_user).as_json
            end
          end

          paginated_endpoint do
            desc 'Get paginated favorited questions for current user'

            params do
              optional :category_id, type: Integer
            end

            get :favorite_questions do

              cache_data = [
                current_user.id,
                params[:category_id] || 'all',
                params[:page],
                params[:per_page]
              ]

              respond_with_cacheable 'current_user.favorite_questions', cache_data do
                questions = current_user.favorite_questions

                if params[:category_id].present?
                  questions = questions.joins(:category).where('categories.id' => params[:category_id])
                end

                paginated_questions = paginate(questions).includes(:category, :user)

                options = {
                  serializer: :favorited_question_feed,
                  root: :questions,
                  each_serializer_meta: {
                    force_favorited: true
                  }
                }

                paginated_serialized_array(paginated_questions, options).as_json
              end
            end
          end

          paginated_endpoint do
            desc 'Get paginated notifications for current user'
            get :notifications do
              response_for_paginated_endpoint 'current_user.notifications',current_user.id do
                notifications = paginated_notifications_for_user(current_user)
                paginated_serialized_notifications(notifications, serializer: :current_user_notification).as_json
              end
            end
          end

          paginated_endpoint do
            desc 'Get paginated questions created by current user'
            get :questions do
              response_for_paginated_endpoint 'current_user.questions' do
                paginated_questions(current_user.questions)
              end
            end
          end

          namespace :goals do
            paginated_endpoint do
              desc 'Get paginated goals for current_user'
              get do
                response_for_paginated_endpoint 'current_user.goals' do
                  paginated_serialized_goals_for(current_user)
                end
              end
            end

            route_param :id do
              desc 'Get a goal of current_user'
              get do
                respond_with_cacheable('current_user.goal', params[:id]) do
                  goal = current_user.goals.find_by(id: params[:id])
                  if goal
                    serialized_goal(goal, meta: { include_activities: false }).as_json
                  else
                    not_found_error_response(:goals)
                  end
                end
              end

              namespace :activities do
                paginated_endpoint do
                  desc 'Get paginated activities for goal'
                  get do
                    response_for_paginated_endpoint 'goals.activities', params[:id] do
                      goal = current_user.goals
                             .includes(:user, :category)
                             .find_by(id: params[:id])

                      if goal
                        paginated_activities_for(goal, serializer: :current_user_activity, root: :activities)
                      else
                        not_found_error_response(:goals)
                      end
                    end
                  end
                end
              end

              namespace :reports do
                desc 'Goal report'
                get do
                  goal = current_user.goals.find_by(id: params[:id])

                  if goal
                    serialized_object(goal, serializer: :goal_report)
                  else
                    not_found_error_response(:goals)
                  end
                end
              end
            end
          end

          namespace :feed do
            paginated_endpoint do
              desc 'Get questions feed for current user'
              get :questions do
                response_for_paginated_endpoint 'current_user.questions_feed' do
                  paginated_questions_feed_for_user(current_user, serializer: :user_question_feed)
                end
              end
            end

            paginated_endpoint do
              desc 'Get questions feed for current user'
              get :questions_to_reply do
                response_for_paginated_endpoint 'current_user.questions_to_reply_feed' do
                  options = {
                    serializer: :user_question_feed
                  }

                  paginated_questions_to_reply_feed_for_user(current_user, options)
                end
              end
            end
          end
        end
      end
    end
  end
end
