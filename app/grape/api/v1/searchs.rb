# encoding: UTF-8

module API
  module V1
    class Searchs < API::V1::Base

      helpers API::Helpers::V1::SearchsHelpers

      before do
        authenticate_user
      end

      with_cacheable_endpoints :search do

        namespace :questions do
          desc 'search for questions matching the criteria'
          params do
            requires :q, type: String, regexp: /[^\s]{1,}/, allow_blank: false
          end

          paginated_endpoint do
            get do
              cache_replace = [current_user.id, params[:q].parameterize]

              response_for_paginated_endpoint 'searchs.questions', cache_replace do
                questions_searched = Question.not_reported.search_by_user(params[:q], current_user.id)

                questions = paginate(questions_searched)

                options = {
                  serializer: :user_question_feed,
                  root: :questions
                }

                paginated_serialized_array(questions, options).as_json
              end
            end
          end
        end

        # Users
        namespace :users do
          paginated_endpoint do
            desc 'search for users with given name'
            params do
              requires :q, allow_blank: false
            end

            get do
              profile_types = profile_types_for_search(params)

              replace_data = cache_data_for_user_search(profile_types, params)
              cache_key = profile_types.all? ? 'users_by_name_and_profile_type' : 'users_by_name'

              respond_with_cacheable("searchs.#{cache_key}", replace_data) do
                users = User.all # current scope

                if profile_types.any?
                  users = User.where(profile_type: profile_types)
                end

                users = paginate(users.search_with_friendship_status(params[:q], current_user.id))

                paginated_serialized_array(users, serializer: :public_user, root: :users).as_json
              end
            end
          end
        end
      end
    end
  end
end
