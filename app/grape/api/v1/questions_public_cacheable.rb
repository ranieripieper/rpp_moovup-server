# encoding: UTF-8

module API
  module V1
    class QuestionsPublicCacheable < API::V1::Base

      helpers API::Helpers::V1::QuestionsHelpers

      with_cacheable_endpoints :public do
        namespace :questions do
          route_param :question_id do
            desc 'Get a question'
            get do
              question = Question.includes(recent_answers: [:user]).find_by(id: params[:question_id])

              if question
                respond_with_cacheable('question.public_show', params[:question_id]) do
                  serialized_question(question, serializer: :public_question).as_json
                end
              else
                not_found_error_response(:questions)
              end
            end

            desc 'Returns a static HTML version of question data for social media sharing (facebook, google, pinterest)'
            get :static_social_page do
              content_type "text/html; charset=utf-8"
              env['api.format'] = nil # dont touch

              opts = {
                id: params[:question_id],
                force_cache: true
              }

              respond_with_cacheable 'question.static_social_page', opts do
                question = Question.includes(:recent_answers).find_by(id: params[:question_id])

                render_static_social_page_for(question)
              end
            end


            namespace :answers do

              paginated_endpoint do
                desc 'Get paginated answers'
                get do
                  question = Question.find_by(id: params[:question_id])

                  response_for_paginated_endpoint 'question.public_answers', true do
                    if question
                      paginated_answers = paginate(question.answers).includes(:user)
                      paginated_serialized_array(paginated_answers, serializer: :public_question_answer, root: :answers).as_json
                    else
                      not_found_error_response(:questions)
                    end
                  end
                end
              end

              route_param :answer_id do
                namespace :comments do

                  paginated_endpoint do
                    desc 'Get paginated comments for answer'
                    get do
                      response_for_paginated_endpoint 'answers.public_comments', true do
                        question = Question.find_by(id: params[:question_id])

                        if question
                          answer = question.answers.find_by(id: params[:answer_id])

                          return not_found_error_response(:answers) unless answer.present?

                          paginated_answers = paginate(answer.children).includes(:user)

                          options = {
                            serializer: :public_question_answer,
                            root: :comments
                          }

                          paginated_serialized_array(paginated_answers, options).as_json
                        else
                          not_found_error_response(:questions)
                        end
                      end
                    end
                  end

                end
              end
            end
          end
        end
      end
    end
  end
end
