# encoding: UTF-8

module API
  module V1
    class Reports < API::V1::Base

      # helpers API::Helpers::V1::ReportHelpers

      before do
        authenticate_user
      end

      namespace :report do
        params do
          requires :report, type: Hash do
            requires :reportable_id
            requires :reportable_type
            requires :report_type
          end
        end

        desc 'Report question or answer'
        post do
          service = execute_service('Reports::CreateService', current_user, params)
          response_for_create_service(service, :report)
        end
      end

      [:question, :answer].each do |reportable_type|
        namespace reportable_type.to_s.pluralize do
          route_param :id do
            namespace :report do

              desc 'Report Question'
              params do
                requires :report_type
              end
              post do
                params[:report] = {
                  reportable_type: reportable_type.to_s.classify,
                  reportable_id: params[:id],
                  report_type: params[:report_type]
                }

                service = execute_service('Reports::CreateService', current_user, params)
                response_for_create_service(service, :report)
              end
            end
          end
        end
      end
    end
  end
end
