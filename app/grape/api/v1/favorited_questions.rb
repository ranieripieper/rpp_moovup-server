# encoding: UTF-8

module API
  module V1
    class FavoritedQuestions < API::V1::Base

      before do
        authenticate_user
      end

      namespace :questions do

        route_param :question_id do
          namespace :favorite do

            desc 'Favorite a question'
            post do
              service = execute_service('FavoritedQuestions::CreateService', current_user, params)
              response_for_create_service(service, :favorited_question)
            end

            desc 'Remove question as favorited'
            delete do
              question = current_user.favorited_questions.find_by(question_id: params[:question_id])

              service = execute_service('FavoritedQuestions::DeleteService', question, current_user, params)
              response_for_delete_service(service, :favorited_question)
            end
          end
        end
      end
    end
  end
end
