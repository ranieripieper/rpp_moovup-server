# encoding: UTF-8

module API
  module V1
    class Questions < API::V1::Base

      helpers API::Helpers::V1::QuestionsHelpers

      before do
        authenticate_user
      end

      namespace :questions do
        params do
          requires :question, type: Hash do
            requires :body_text
            requires :category_id
          end
        end

        desc 'Create a new question'
        post do
          service = execute_service('Questions::CreateService', current_user, params)
          response_for_create_service(service, :question)
        end

        route_param :id do
          desc 'Update a Question'
          put do
            question = current_user.questions.find_by(id: params[:id])
            service = execute_service('Questions::UpdateService', question, current_user, params)
            response_for_update_service(service, :question)
          end

          desc 'Delete a Question'
          delete do
            question = current_user.questions.find_by(id: params[:id])
            service = execute_service('Questions::DeleteService', question, current_user, params)
            response_for_delete_service(service, :question)
          end
        end
      end
    end
  end
end
