# encoding: UTF-8

module API
  module V1
    class CommercialActivitiesCacheable < API::V1::Base

      helpers API::Helpers::V1::CommercialActivitiesHelpers

      with_cacheable_endpoints :commercial_activities do
        paginated_endpoint do
          desc 'Get paginated commercial activities'
          get do
            response_for_paginated_endpoint 'commercial_activities.all', 'all' do
              paginated_commercial_activities
            end
          end
        end
      end
    end
  end
end
