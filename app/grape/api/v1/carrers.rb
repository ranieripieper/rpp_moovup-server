# encoding: UTF-8

module API
  module V1
    class Carrers < API::V1::Base

      helpers API::Helpers::V1::CarrersHelpers

      before do
        authenticate_user
      end

      namespace :carrers do
        desc 'Create a new carrer'
        params do
          requires :carrer, type: Hash do
            requires :title
          end
        end

        post do
          service = execute_service('Carrers::CreateService', nil, params)
          response_for_create_service(service, :carrer)
        end
        route_param :id do
          desc 'Update a carrer'
          put do
            carrer = Carrer.find_by(id: params[:id])
            service = execute_service('Carrers::UpdateService', carrer, nil, params)
            response_for_update_service(service, :carrer)
          end

          desc 'Delete a carrer'
          delete do
            carrer = Carrer.find_by(id: params[:id])
            service = execute_service('Carrers::DeleteService', carrer, nil, params)
            response_for_delete_service(service, :carrer)
          end
        end
      end

    end
  end
end
