# encoding: UTF-8

module API
  module V1
    class AnswersCacheable < API::V1::Base

      helpers API::Helpers::V1::AnswersHelpers

      namespace :questions do
        route_param :question_id do

          with_cacheable_endpoints :answers do

            paginated_endpoint do
              desc 'Get paginated answers'
              get do
                question = Question.find_by(id: params[:question_id])

                response_for_paginated_endpoint 'question.answers' do
                  if question
                    paginated_answers = paginate(question.answers).includes(:user)
                    paginated_serialized_array(paginated_answers, serializer: :answer, root: :answers).as_json
                  else
                    not_found_error_response(:questions)
                  end
                end
              end
            end

            route_param :id do
              desc 'Get a answer beloging to a question'
              get do
                question = Question.find_by(id: params[:question_id])

                if question
                  answer = question.answers.find_by(id: params[:id])
                  if answer
                    options = {
                      serializer: :answer
                    }
                    serialized_answer(answer, options).as_json
                  else
                    not_found_error_response(:answers)
                  end
                else
                  not_found_error_response(:questions)
                end
              end

              paginated_endpoint do
                desc 'Get paginated comments for answer'
                get :comments do
                  response_for_paginated_endpoint 'current_user.answers.children' do
                    question = Question.find_by(id: params[:question_id])

                    if question
                      answer = question.answers.find_by(id: params[:id])

                      return not_found_error_response(:answers) unless answer.present?

                      paginated_answers = paginate(answer.children.not_reported).includes(:user)

                      options = {
                        serializer: :answer,
                        root: :comments
                      }

                      paginated_serialized_array(paginated_answers, options).as_json
                    else
                      not_found_error_response(:questions)
                    end

                  end
                end
              end
            end
          end
        end
      end
    end
  end
end
