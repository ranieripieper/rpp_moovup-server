# encoding: UTF-8

module API
  module V1
    class CategoriesCacheable < API::V1::Base

      helpers API::Helpers::V1::CategoriesHelpers

      with_cacheable_endpoints :categories do
        paginated_endpoint do
          desc 'Get paginated categories'
          params do
            optional :filter, type: String, default: 'all', values: ['all', 'goals', 'questions', 'interests']
          end
          get do
            filter = params[:filter].try(:downcase).try(:to_sym)
            has_filter = filter.present? && [:all].exclude?(filter)
            ignore_children = (([:questions, :interests].member?(filter)) &&
                                Moovup::Config.enabled?(:only_allow_root_categories_in_qa))

            response_for_paginated_endpoint "categories.#{filter}", '' do
              categories = (has_filter ? Category.all : Category.roots).includes(children: [:children])

              if has_filter
                categories = categories.send("valid_for_#{filter}")
              end

              options = {
                each_serializer_meta: {
                  ignore_children: ignore_children
                }
              }

              paginated_serialized_categories(categories, options).as_json
            end
          end

          route_param :id do
            get do
              respond_with_cacheable('categories.show', params[:id]) do
                category = Category.includes(children: [:children]).find_by(id: params[:id])
                if category
                  serialized_category(category).as_json
                else
                  not_found_error_response(:categories)
                end
              end
            end
          end

        end
      end

      with_cacheable_endpoints :hobbies do
        get do
          respond_with_cacheable('hobbies.all', 'all') do
            serialized_array(Category.hobby, serializer: :simple_category).as_json
          end
        end
      end
    end
  end
end
