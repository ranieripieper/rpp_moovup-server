# encoding: UTF-8

module API
  module V1
    class UsersInterests < API::V1::Base

      helpers API::Helpers::V1::UserInterestsHelpers

      before do
        authenticate_user
      end

      namespace :users do
        namespace :me do
          namespace :interests do
            params do
              requires :interest, type: Hash do
                requires :category_id, type: Integer
                optional :max_answers_count, type: Integer
              end
            end

            desc 'Associate an category as interest to user'
            post do
              service = execute_service('UserInterests::CreateService', current_user, params)
              response_for_create_service(service, :user_interest)
            end

            params do
              requires :interests, type: Array do
              end
            end
            desc 'Batch associates categories as interest to user'
            post :batch do
              service = execute_service('UserInterests::BatchCreateService', current_user, params)
              response_for_batch_service(service)
            end

            params do
              requires :categories_ids
            end
            desc 'Batch delete associates categories as interest to user'
            delete :batch do
              service = execute_service('UserInterests::BatchDeleteService', current_user, params)
              response_for_batch_service(service, :destroyed?)
            end

            params do
              requires :category_id, allow_blank: false
            end
            desc 'Delete an associated category interests of user'
            delete do
              interest = current_user.interests.find_by(category_id: params[:category_id])

              service = execute_service('UserInterests::DeleteService', interest, current_user, params)
              response_for_create_service(service, :user_interest)
            end
          end
        end
      end
    end
  end
end
