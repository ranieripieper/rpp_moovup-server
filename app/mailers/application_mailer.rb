class ApplicationMailer < ActionMailer::Base
  default from: Moovup::Config.contact_email

  layout 'mailer'
end
