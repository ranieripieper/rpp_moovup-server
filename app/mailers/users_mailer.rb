class UsersMailer < ApplicationMailer
  def welcome(user)
    @user = user

    mail to: user.email
  end

  def password_recovery(user)
    @user = user

    mail to: user.email
  end

  def password_updated(user)
    @user = user

    mail to: user.email
  end

  def activate_account(user)
    @user = user

    mail to: user.email
  end
end
