# encoding: UTF-8

module Moovup
  class BaseCrudService < BaseService

    attr_reader :record, :user

    def self.record_type(record_type, options = {})
      define_method :record_type do
        record_type
      end

      record_alias = options.delete(:alias_name) || record_type.to_s.underscore

      alias_method record_alias.to_sym, :record
    end

    def initialize(record, user, options = {})
      @record = record
      @user   = user

      super(options)
    end

    def execute
      not_implemented_exception(__method__)
    end

    def record_type
      not_implemented_exception(__method__)
    end

    private

    def record_params
      not_implemented_exception(__method__)
    end

    def record_error_key
      record_type.to_s.pluralize.underscore
    end

    def valid_record?
      valid_object?(@record, record_type)
    end
  end
end
