# encoding: UTF-8

module Moovup
  class BaseUpdateService < BaseCrudService

    def execute
      if can_execute_action?

        set_history_user

        @updated_record = update_record
        @updated_record ||= @record

        if success_updated?
          track_record_versions
          success_response
        else
          errors = update_errors
          bad_request_error(errors) if errors.present?
        end
      end

      success?
    end

    def changed_attributes
      return [] if fail?
      @changed_attributes ||= changes(@old_record, @record, changed_attributes_array)
    end

    private

    def track_record_versions
      @old_record = @record.dup
      @record     = @updated_record
    end

    def set_history_user
      if @record.respond_to?(:history_user) && valid_user?
        @record.history_user = @user
      end
    end

    def changed_attributes_array
      record_params.keys
    end

    def success_updated?
      @updated_record.valid?
    end

    def update_errors
      @updated_record.errors
    end

    def update_record
      @record.class.send(:update, @record.id, record_update_params(record_params))
    end

    def attributes_to_clear
      return @_attrs_to_clear if @_attrs_to_clear

      attrs = array_values_from_string(@options[:_clear_attrs] || '')

      whitelist = whitelist_attributes_to_clear.map(&:to_sym)

      @attributes_to_clear = attrs.select {|_attr| whitelist.member?(_attr.to_sym) }.map(&:to_sym)
    end

    def record_update_params(record_params)
      (attributes_to_clear || []).each {|param| record_params[param.to_sym] = nil }

      record_params
    end

    def whitelist_attributes_to_clear
      []
    end

    def removing_attribute?(attribute)
      attributes_to_clear.member?(attribute.to_sym) && record_params[attribute.to_sym].nil?
    end

    def can_execute_action?

      unless valid_record?
        return not_found_error!("#{record_error_key}.not_found")
      end

      unless valid_user?
        return not_found_error!('users.not_found')
      end

      unless can_update?
        return forbidden_error!("#{record_error_key}.user_cant_update")
      end

      return true
    end

    def can_update?
      return false unless valid_user?
      return false unless valid_record?

      user_can_update?
    end

    def user_can_update?
      @record.user_can_update?(@user)
    end

    def after_success
    end
  end
end
