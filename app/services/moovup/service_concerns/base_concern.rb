module Moovup
  module ServiceConcerns
    module BaseConcern

      def array_values_from_params(options, key, root=nil)
        options = options.symbolize_keys

        if root.present?
          options = (options[root.to_sym] || {}).symbolize_keys
        end

        return [] unless options.key?(key.to_sym)

        values = options[key.to_sym]

        return values if values.is_a?(Array)

        array_values_from_string(values)
      end

      def array_values_from_string(string)
        string.to_s.split(/\,/).map(&:squish)
      end

    end
  end
end
