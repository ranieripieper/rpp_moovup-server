# encoding: UTF-8

module Moovup
  module V1
    module Users
      class ProfileImageUpdateService < BaseUpdateService

        record_type ::User

        private
        def user_can_update?
          @record.id == @user.id
        end

        def record_params
          filter_hash(@options[:user], [:profile_image])
        end
      end
    end
  end
end
