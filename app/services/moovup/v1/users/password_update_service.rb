# encoding: UTF-8

module Moovup
  module V1
    module Users
      class PasswordUpdateService < BaseService

        attr_reader :user

        delegate :name, to: :user, prefix: :user, allow_nil: true

        def initialize(user, options={})
          super(options)
          @user = user
        end

        def execute
          if can_execute_action?
            if @user.update_password(new_password_value)
              success_response
            else
              unprocessable_entity_error(@user.errors)
            end
          end
        end

        private

        def new_password_value
          @options[:password]
        end

        def can_execute_action?
          unless valid_token?
            return not_found_error!('users.invalid_password_update_token')
          end

          unless valid_user?
            return not_found_error!('users.not_found')
          end

          unless confirmation_match?
            return unprocessable_entity_error!('users.password_combination_dont_match')
          end

          return true
        end

        def valid_token?
          return false if @options[:token].blank?

          @token ||= User.find_by(reset_password_token: @options[:token]).try(:reset_password_token)

          return @token.present? && @user.try(:reset_password_token) == @token
        end

        def confirmation_match?
          return false if @options[:password].blank? || @options[:password_confirmation].blank?
          return @options[:password] == @options[:password_confirmation]
        end

        def after_success
          notify_users
        end

        def notify_users
          create_system_notification_async(@user, :password_updated)
          delivery_async_email(UsersMailer, :password_updated, @user)
        end
      end
    end
  end
end
