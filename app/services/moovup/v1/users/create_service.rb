# encoding: UTF-8

module Moovup
  module V1
    module Users
      class CreateService < BaseCreateService

        include V1::ServiceConcerns::UserParams

        record_type ::User

        attr_reader :authorization

        delegate :id, :username,
                 :profile_image_url, :profile_type,
                 :oauth_provider, :oauth_provider_uid,
                 to: :user, prefix: :user

        delegate :token, :provider, :expires_at, to: :authorization, prefix: :user_auth, allow_nil: true

        def new_user?
          @record.persisted?
        end

        private
        def can_create_record?
          validations = company? ? [:commercial_activity] : [:hobby, :carrer]

          validations.each do |association|
            unless send(association).present?
              return not_found_error!("#{association.to_s.pluralize}.not_found")
            end
          end

          return true
        end

        def build_record
          deleted_user = nil

          # TODO: Only restore user account is not secure
          # needed second step to authenticate user ownership of email account
          if Moovup::Config.enabled?(:restore_deleted_account_on_signup)
            deleted_user = User.with_deleted.find_by(email: @options[:user][:email])
          end

          if deleted_user
            user = restore_deleted_user(deleted_user)
          else
            user = User.new(user_params)
          end

          user
        end

        def restore_deleted_user(user)
          user.restore(recursive: true)
          user.update_password(@options[:user][:password], true)

          user
        end

        def async?
          return Moovup::Config.enabled?(:update_user_after_signup_async) || @options[:async].eql?(true)
        end

        def create_authorization
          if @record.account_activated?
            return @authorization ||= @record.authorizations.create(provider: @options[:provider] || DEFAULT_PROVIDER)
          end

          @authorization = nil
        end

        def execute_async_actions
          # try to find `device` key in @options[:device] && @options[:user][:device]
          update_service_options = @options.slice(:origin, :device).merge(@options.fetch(:user, {}).slice(:device))
          update_service_options[:notify_user] = new_user?

          if async?
            Moovup::V1::UserSignupUpdateWorker.perform_async(@record.id, update_service_options)
          else
            service = Moovup::V1::Users::SignupUpdateService.new(@record, update_service_options)
            service.execute
          end
        end

        [:carrer, :commercial_activity].each do |method_name|
          define_method method_name do
            object_id = @options[:user] && @options[:user][:"#{method_name}_id"]
            return nil unless object_id

            klass = method_name.to_s.camelize.constantize
            instance_variable_set("@#{method_name}", klass.find_by(id: object_id))
          end
        end

        def hobby
          object_id = @options[:user] && @options[:user][:hobby_id]
          return nil if object_id.blank?

          @hobby ||= Category.find_by(id: object_id)
        end

        def valid_user?
          return true
        end

        def after_success
          after_success_actions
        end

        def after_success_actions
          create_interests_by_hobby
          create_default_interests
          create_authorization
          execute_async_actions
        end

        def create_default_interests
          interests = default_interests

          interests.map {|i| add_interest(i) }
        end

        def default_interests
          interests = (@options[:default_interests] || Moovup::Config.default_interests_per_user || '')

          return interests if interests.is_a?(Array) # [1,2,3]
          return interests.keys if interests.is_a?(Hash) # {"id" => "name"}

          return interests.to_s.split(",").map(&:squish)
        end

        def create_interests_by_hobby
          add_interest(@record.hobby_id)
        end

        def add_interest(category_id)
          begin
            @record.interests.create(category_id: category_id)
          rescue Exception => e
            Rollbar.error(e)
          end
        end
      end
    end
  end
end
