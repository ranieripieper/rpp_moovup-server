module Moovup
  module V1
    module Users
      class UpdateService < BaseUpdateService

        include V1::ServiceConcerns::UserParams

        NON_UPDATABLE_ATTRIBUTES = [:email, :profile_type]

        record_type ::User

        def record_params
          params = user_params

          NON_UPDATABLE_ATTRIBUTES.each {|attr| params.delete(attr) }

          params
        end

        private

        def user_can_update?
          @record == @user
        end

        def company?
          @record.try(:company?)
        end
      end
    end
  end
end
