# encoding: UTF-8

module Moovup
  module V1
    class Users::PreferencesUpdateService < BaseUpdateService

      record_type ::User

      def changed_attributes
        [:preferences]
      end

      private
      def record_params
        preferences_hash = filter_hash(@options, [:preferences])
        clear_preferences(preferences_hash[:preferences] || preferences_hash)
      end

      def update_record
        record_params.each do |key, value|
          user.update_preference_key(key, value, false)
        end

        user.save

        user
      end

      def clear_preferences(preferences_hash)
        hash = preferences_hash.keep_if do |key, _|
          user.allowed_preference_key?(key)
        end.to_h.symbolize_keys
      end

      def user_can_update?
        @record.id == @user.id
      end

      def after_success
        update_user_parse_channels
      end

      def update_user_parse_channels
        ::Moovup::V1::UpdateUserParseChannelsWorker.perform_async(@user.id)
      end
    end
  end
end
