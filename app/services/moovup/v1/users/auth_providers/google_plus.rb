# encoding: UTF-8

module Moovup
  module V1
    module Users
      module AuthProviders
        class GooglePlus < Base

          PROVIDER_ATTRIBUTES = [:email]

          def memoized_fetch_user_data(scope = 'me')
            @users_data ||= {}
            @users_data[scope.to_sym] ||= fetch_user_data(scope)
          end

          def provider_name
            :google_plus
          end

          def provider_uid_key
            :id
          end

          def provider_request_url(access_token, uuid = 'me')
            "https://www.googleapis.com/plus/v1/people/#{uuid}?access_token=#{access_token}"
          end

          def provider_remote_avatar_url(user_id, size='300')
            image_data = (@auth_provider_data || {})['image']
            image_url  = image_data && image_data['url'] || ''

            image_url.sub(/sz=(\d+)/i, "sz=#{size}")
          end

          private
          def normalized_user_data_from_provider(provider_data)
            email = provider_data['email'] ||
                    provider_data['emails'] && provider_data['emails'].first.try(:[],'value')
            name_data = provider_data['name']
            first_name, last_name = name_data['givenName'], name_data['familyName']

            user_data = provider_data
                        .slice(*PROVIDER_ATTRIBUTES.map(&:to_s))
                        .merge(email: email, first_name: first_name, last_name: last_name)


            user_data[:gender] ||= ::User::VALID_GENDERS[:male] # default to avoid errors

            base_user_data.merge(user_data)
          end
        end
      end
    end
  end
end
