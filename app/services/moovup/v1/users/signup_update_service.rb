# encoding: UTF-8

module Moovup
  module V1
    module Users
      class SignupUpdateService < BaseService

        attr_reader :user

        def initialize(user, options)
          @user = user
          super(options)
        end

        def execute
          execute_actions
          success_response
        end

        private
        def notify_user?
          return @options[:notify_user].present?
        end

        def execute_actions
          create_user_device
          notify_user if notify_user?
        end

        def device_params
          @options.symbolize_keys.slice(:device) || {}
        end

        def create_user_device
          if device_params.present?
            service = Moovup::V1::Parse::DeviceCreateService.new(@user, device_params)
            service.execute
          end
        end

        def notify_user
          send_signup_email
          create_welcome_notifications
          send_welcome_push_notification
        end

        def send_welcome_push_notification
          send_push_notification_async(@user, :welcome)
        end

        def send_signup_email
          delivery_async_email(UsersMailer, :welcome, @user)
        end

        def create_welcome_notifications
          create_system_notification_async(@user, :welcome)
        end
      end
    end
  end
end
