# encoding: UTF-8

module Moovup
  module V1
    module Users
      class DeleteService < BaseDeleteService

        record_type ::User

        private
        def destroy_record
          @user_questions = @user.questions
          @record.delete
        end

        def after_success
          clear_authorizations
          clear_devices
          clear_favorited_questions_by_other_users
          clear_originated_notifications
          clear_created_questions_and_answers
        end

        def clear_authorizations
          @record.authorizations.destroy_all
        end

        def clear_devices
          @record.devices.find_each do |device|
            ::Moovup::V1::ParseDeviceDeleteWorker.perform_async(@record.id, device.id)
          end
        end

        def clear_originated_notifications
          delete_all_updating_deleted_at(@user.originated_notifications) # working better than .delete_all
        end

        def clear_created_questions_and_answers
          clear_created_questions
          clear_created_answers
        end

        def clear_created_questions
          clear_favorited_questions_by_other_users
          delete_all_updating_deleted_at(@user_questions)
        end

        def clear_created_answers
          delete_all_updating_deleted_at(@user.answers)
        end

        def clear_favorited_questions_by_other_users
          FavoritedQuestion.where(question: @user_questions.map(&:id)).delete_all
        end

        protected

        def delete_all_updating_deleted_at(collection)
          collection.update_all(deleted_at: Time.zone.now)
        end

      end
    end
  end
end
