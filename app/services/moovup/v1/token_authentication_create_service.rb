# encoding: UTF-8

module Moovup
  module V1
    class TokenAuthenticationCreateService < AuthenticationCreateService

      def initialize(token, provider, options={})
        super(nil, nil, options)
        @token    = token
        @provider = provider.to_s.downcase
      end

      def execute
        if valid_provider?

          @auth_token = Authorization.find_by(token: @token, provider: @provider)

          if @auth_token
            if @auth_token.valid_access?
              success_response
            else
              @auth_token.delete
            end
          else
            not_authorized_error('invalid_auth_token')
          end
        else
          invalid_provider_response
        end

        success?
      end

      private
      def after_success
        @auth_token.update_token_expires_at
        @user = @auth_token.try(:user)
      end
    end
  end
end
