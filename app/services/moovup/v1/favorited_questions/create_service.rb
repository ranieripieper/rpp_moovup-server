# encoding: UTF-8

module Moovup
  module V1
    module FavoritedQuestions
      class CreateService < BaseCreateService

        include V1::ServiceConcerns::FavoritedQuestionParams

        record_type ::FavoritedQuestion

        private
        def can_create_record?
          unless valid_question?
           return not_found_error!('questions.not_found')
          end

          return true
        end

        def build_record
          @user.favorited_questions.build(favorited_question_params)
        end

        def save_record
          begin
            @record.save
          rescue ActiveRecord::RecordNotUnique, PG::UniqueViolation => e
            @record.errors.add(:question_id, :yet_favorited)
            false
          end
        end
      end
    end
  end
end
