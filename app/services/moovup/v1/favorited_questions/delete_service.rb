# encoding: UTF-8

module Moovup
  module V1
    module FavoritedQuestions
      class DeleteService < BaseDeleteService

        record_type ::FavoritedQuestion

      end
    end
  end
end
