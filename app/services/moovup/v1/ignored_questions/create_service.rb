module Moovup
  module V1
    module IgnoredQuestions
      class CreateService < BaseCreateService

        record_type ::IgnoredQuestion

        WHITELIST_ATTRIBUTES = [
          :question_id,
          :reason
        ]

        def question
          question_id = attributes_hash.fetch(:question_id, nil)

          return nil if question_id.blank?

          @question ||= ::Question.find_by(id: question_id)
        end

        private
        def can_create_record?
          unless valid_question?
            return not_found_error!('questions.not_found')
          end

          return true
        end

        def save_record
          begin
            @record.save
          rescue ActiveRecord::RecordNotUnique, PG::UniqueViolation => e
            @record.errors.add(:question_id, :yet_ignored)
            false
          end
        end

        def build_record
          @user.ignored_questions.build(ignored_question_params)
        end

        def ignored_question_params
          @ignored_question_params ||= filter_hash(attributes_hash, WHITELIST_ATTRIBUTES)
        end

        def valid_question?
          return question.present?
        end

        def attributes_hash
          @options[:question] || @options
        end

      end
    end
  end
end

