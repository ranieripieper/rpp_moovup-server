# encoding: UTF-8

module Moovup
  module V1
    module Reports
      class CreateService < BaseCreateService

        include V1::ServiceConcerns::ReportParams

        record_type ::Report

        private
        def can_create_record?
          unless can_create_report?
            return unprocessable_entity_error!('reports.duplicate_report')
          end

          unless valid_reportable_type?
           return unprocessable_entity_error!('reports.invalid_reportable_type')
          end

          if reportable_yet_reported?
            return unprocessable_entity_error!('reports.resource_yet_reported')
          end

          return true
        end

        def build_record
          reportable.reports.build(report_params.merge(user_id: @user.id))
        end

        def can_create_report?
          # report = @user.reports.where(reportable_id: report_params[:reportable_id]).last
          # return true if report.blank?
          true
        end

        def after_success
          update_reported_at_in_reportable
          update_question_cover_image
        end

        def update_question_cover_image
          if reportable.is_a?(Answer) && reportable.has_image?
            ::Moovup::V1::QuestionUpdateCoverImageWorker.perform_async(reportable.question_id)
          end
        end

        def update_reported_at_in_reportable
          return false unless reportable.respond_to?(:attributes) && reportable.attributes.member?('reported_at')

          reportable.update_attribute(:reported_at, Time.zone.now)

          if reportable.is_a?(Answer)
            if reportable.root?
              Question.decrement_counter(:answers_count, reportable.question_id)
            else
              Answer.decrement_counter(:replies_count, reportable.parent_id)
            end
          end

        end
      end
    end
  end
end
