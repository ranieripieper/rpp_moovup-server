# encoding: UTF-8

module Moovup
  module V1
    module Questions
      class DeleteService < BaseDeleteService

        record_type ::Question

      end
    end
  end
end
