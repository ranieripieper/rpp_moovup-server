module Moovup
  module V1
    module Questions
      class UpvoteCreateService < Upvotes::CreateService

        alias :question :resource

        def resource_type
          Question.to_s
        end

        def notification_type
          :new_like_for_question
        end

      end
    end
  end
end
