# encoding: UTF-8

module Moovup
  module V1
    module Questions
      class CreateService < BaseCreateService

        include V1::ServiceConcerns::QuestionParams

        record_type ::Question

        private
        def can_create_record?
          unless can_create_question?
            return unprocessable_entity_error!('questions.duplicate_name')
          end

          unless valid_category?
           return unprocessable_entity_error!('questions.invalid_category')
          end

          return true
        end

        def build_record
          @user.questions.build(question_params)
        end

        def can_create_question?
          question = @user.questions.where(question_params.slice(:body_text, :category_id)).last
          return true if question.blank?

          return (question.created_at + 5.seconds) < Time.now
        end
      end
    end
  end
end
