# encoding: UTF-8

module Moovup
  module V1
    module Questions
      class UpdateService < BaseUpdateService

        include V1::ServiceConcerns::QuestionParams

        record_type ::Question

        private
        def record_params
          question_params
        end

        def user_can_update?
          unless valid_category?
            return unprocessable_entity_error!("questions.invalid_category")
          end

          super
        end

        def valid_category?
          return true if @options[:question][:category_id].blank?
          super
        end

      end
    end
  end
end
