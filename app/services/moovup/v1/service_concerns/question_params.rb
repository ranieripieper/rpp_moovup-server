module Moovup
  module V1
    module ServiceConcerns
      module QuestionParams

        extend ActiveSupport::Concern

        WHITELIST_ATTRIBUTES = [
          :body_text,
          :category_id
        ]

        included do
          def question_params
            @question_params ||= filter_hash(@options[:question], WHITELIST_ATTRIBUTES)
          end
        end

        def valid_category?
          valid_object?(category, Category)
        end

        def category_id
          @options[:question] && @options[:question][:category_id]
        end

        def category
          @category ||= Category.find_by(id: category_id)
        end

      end
    end
  end
end
