module Moovup
  module V1
    module ServiceConcerns
      module GoalParams

        extend ActiveSupport::Concern

        WHITELIST_ATTRIBUTES = [
          :public,
          :category_id
        ]

        OPEN_WHITELIST_ATTRIBUTES = [
          :periodicity_type,
          :total
        ]

        CLOSED_WTHITELIST_ATTRIBUTES = [
          :target,
          :current,
          :target_date
        ]

        def goal_params
          goal_attrs = open_goal? ? OPEN_WHITELIST_ATTRIBUTES : CLOSED_WTHITELIST_ATTRIBUTES
          whitelist_attrs = WHITELIST_ATTRIBUTES + goal_attrs

          @goal_params ||= filter_hash(attributes_hash, whitelist_attrs)
        end

        def attributes_hash
          @options[:goal]
        end

        def closed_goal?
          any_closed_goal_data_received?
        end

        def all_closed_goal_data_received?
          CLOSED_WTHITELIST_ATTRIBUTES.all? {|value| attributes_hash[value.to_sym].present? }
        end

        def any_closed_goal_data_received?
          CLOSED_WTHITELIST_ATTRIBUTES.any? {|value| attributes_hash[value.to_sym].present? }
        end

        def open_goal?
          !closed_goal?
        end

        def valid_category?
          category_id.present? && category.present?
        end

        def category_id
          @options[:goal] && @options[:goal][:category_id]
        end

        def category
          @category ||= Category.find_by(id: category_id)
        end

        def valid_closed_goal_category?
          return false unless category.present?

          @category.closed?
        end

      end
    end
  end
end
