module Moovup
  module V1
    module ServiceConcerns
      module FriendshipParams

        extend ActiveSupport::Concern

        WHITELIST_ATTRIBUTES = [
          :friend_id
        ]

        included do
          def friendship_params
            @friendship_params ||= filter_hash(attributes_hash, WHITELIST_ATTRIBUTES)
          end

          def attributes_hash
            @options[:friendship] || @options
          end
        end

      end
    end
  end
end
