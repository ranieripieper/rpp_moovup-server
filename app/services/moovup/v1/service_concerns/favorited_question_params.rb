module Moovup
  module V1
    module ServiceConcerns
      module FavoritedQuestionParams

        extend ActiveSupport::Concern

        WHITELIST_ATTRIBUTES = [
        ]

        included do
          def favorited_question_params
            @favorited_question_params ||= filter_hash(attributes_hash, WHITELIST_ATTRIBUTES).merge(question_id: question_id)
          end
        end

        def attributes_hash
          @options.fetch(:question, {})
        end

        def valid_question?
          valid_object?(question, ::Question)
        end

        def question_id
          @options[:question_id] || attributes_hash.fetch(:question_id, nil)
        end

        def question
          @question ||= ::Question.find_by(id: question_id)
        end
      end
    end
  end
end
