module Moovup
  module V1
    module ServiceConcerns
      module UserParams

        extend ActiveSupport::Concern

        COMPANY_WHITELIST_ATTRIBUTES = [
          :company_name,
          :foundation_date,
          :commercial_activity_id
        ]

        USER_WHITELIST_ATTRIBUTES = [
          :carrer_id,
          :hobby_id
        ]

        WHITELIST_ATTRIBUTES = [
          :first_name,
          :last_name,
          :gender,
          :birthday_date,
          :email,
          :profile_type,
          :password,
          :password_confirmation,
          :profile_image
        ]

        DEFAULT_PROVIDER = :site

        included do

          def user_params
            whitelist_attributes = WHITELIST_ATTRIBUTES.dup.concat(
              company? ? COMPANY_WHITELIST_ATTRIBUTES : USER_WHITELIST_ATTRIBUTES
            )

            @user_params ||= filter_hash(@options[:user], whitelist_attributes)
            @user_params[:password_confirmation] ||= true

            # avoid admin/staff creation
            if @user_params[:profile_type] && @user_params[:profile_type].to_sym == :staff
              @user_params[:profile_type] = User::VALID_PROFILES_TYPES[:common_user]
            end

            @user_params
          end

          def company?
            @options[:user] && @options[:user][:profile_type].to_s == User::VALID_PROFILES_TYPES[:company]
          end
        end
      end
    end
  end
end
