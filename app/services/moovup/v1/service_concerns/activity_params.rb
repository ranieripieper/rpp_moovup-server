module Moovup
  module V1
    module ServiceConcerns
      module ActivityParams


        extend ActiveSupport::Concern

        WHITELIST_ATTRIBUTES = [
         :body_text,
         :total,
         :goal_id,
         :date,
         :image
        ]

        included do
          def activity_params
            @activity_params ||= filter_hash(@options[:activity], WHITELIST_ATTRIBUTES)
          end
        end

        def valid_goal?
          valid_object?(goal, ::Goal)
        end

        def goal_id
          @options[:activity] && @options[:activity][:goal_id]
        end

        def goal
          @goal ||= @user.goals.find_by(id: goal_id)
        end

      end
    end
  end
end
