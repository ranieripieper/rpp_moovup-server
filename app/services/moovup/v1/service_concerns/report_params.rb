module Moovup
  module V1
    module ServiceConcerns
      module ReportParams

        extend ActiveSupport::Concern

        WHITELIST_ATTRIBUTES = [
          :report_type
        ]

        VALID_REPORTABLE_TYPE = [
          'answer',
          'question'
        ]

        included do
          def report_params
            @report_params ||= filter_hash(@options[:report], WHITELIST_ATTRIBUTES)
          end
        end

        def valid_reportable_type?
          VALID_REPORTABLE_TYPE.member?(reportable_type.downcase)
        end

        def reportable
          return nil unless valid_reportable_type?

          reportable_type.camelize.constantize.find_by(id: reportable_id)
        end

        def reportable_type
          @options[:report] && @options[:report][:reportable_type]
        end

        def reportable_id
          @options[:report] && @options[:report][:reportable_id]
        end

        def reportable_yet_reported?
          reportable.try(:reported_at).present?
        end

      end
    end
  end
end
