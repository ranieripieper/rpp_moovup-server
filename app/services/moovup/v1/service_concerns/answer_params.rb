module Moovup
  module V1
    module ServiceConcerns
      module AnswerParams

        extend ActiveSupport::Concern

        WHITELIST_ATTRIBUTES = [
          :body_text,
          :question_id,
          :image
        ]

        included do
          def answer_params
            @answer_params ||= filter_hash(@options[:answer], WHITELIST_ATTRIBUTES)
          end
        end

        def creating_comment_for_answer?
          parent_id.present?
        end

        def valid_question?
          valid_object?(question, ::Question)
        end

        def valid_parent_answer?
          valid_object?(parent_answer, ::Answer)
        end

        def question_id
          @options[:question_id] || @options[:answer].fetch(:question_id, nil)
        end

        def question
          @question ||= ::Question.find_by(id: question_id)
        end

        def parent_id
          @options[:parent_id] || @options[:answer].fetch(:parent_id, nil)
        end

        def parent_answer
          return nil if parent_id.blank? || question.blank?
          @answer ||= question.answers.find_by(id: parent_id)
        end
      end
    end
  end
end
