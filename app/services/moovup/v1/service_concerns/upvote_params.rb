module Moovup
  module V1
    module ServiceConcerns
      module UpvoteParams

        extend ActiveSupport::Concern

        VALID_RESOURCES = [
          ::Question,
          ::Answer,
          ::Goal,
          ::Activity
        ]

        included do
          def valid_resource?
            VALID_RESOURCES.any? {|klass| valid_object?(resource, klass) }
          end

          def attributes_hash
            @options[:upvote] || {}
          end

          def resource
            return @options[:resource] if @options.key?(:resource)

            return false if resource_id.blank?
            @resource ||= find_resource
          end

          def resource_type
            @options[:resource_type] || attributes_hash.fetch(:resource_type, nil)
          end

          def resource_id
            @options[:resource_id] || attributes_hash.fetch(:resource_id, nil)
          end

          def find_resource
            return nil unless [resource_type, resource_id].all?(&:present?)

            resource_type.constantize.find_by(id: resource_id)
          end
        end
      end
    end
  end
end
