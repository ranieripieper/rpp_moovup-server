module Moovup
  module V1
    module ServiceConcerns
      module UserInterestParams

        extend ActiveSupport::Concern

        WHITELIST_ATTRIBUTES = [
          :category_id,
          :max_answers_count
        ]

        included do
          def interest_params
            @interest_params ||= filter_hash(@options[:interest], WHITELIST_ATTRIBUTES)
          end
        end

        def valid_category?
          valid_object?(category, ::Category)
        end

        def category
          @category ||= ::Category.find_by(id: interest_params[:category_id])
        end

      end
    end
  end
end
