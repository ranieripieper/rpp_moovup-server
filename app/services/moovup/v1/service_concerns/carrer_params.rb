module Moovup
  module V1
    module ServiceConcerns
      module CarrerParams

        extend ActiveSupport::Concern

        WHITELIST_ATTRIBUTES = [
          :title
        ]

        included do
          def carrer_params
            @carrer_params ||= filter_hash(@options[:carrer], WHITELIST_ATTRIBUTES)
          end
        end

      end
    end
  end
end
