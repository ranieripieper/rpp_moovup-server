# encoding: UTF-8

module Moovup
  module V1
    module Upvotes
      class CreateService < BaseCreateService

        include V1::ServiceConcerns::UpvoteParams

        record_type ::Upvote

        RESOURCES_WHO_INCREMENTS_OWNER_COUNTER = [
          'Answer'
        ]

        def upvotes_count
          resource.try(:upvotes_count) || 0
        end

        def upvotable_resource
          @record && @record.upvotable
        end

        private
        def can_create_record?
          unless valid_resource?
            return not_found_error!('upvotes.invalid_resource')
          end

          return true
        end

        def build_record
          resource.upvotes.build(user_id: @user.id)
        end

        def after_success
          resource.reload
          notify_users
          increment_upvotable_owner_upvotes_count_column
        end

        def notify_users
          n_user, n_type, n_notificable = resource.user, notification_type, resource

          create_system_notification_async(n_user, n_type, n_notificable, sender_user_id: @user.id)
          send_push_notification_async(n_user, n_type, n_notificable, origin_user: @user, push_metadata: push_notification_metadata)
        end

        def push_notification_metadata
          return {} unless resource.respond_to?(:push_notification_metadata)

          resource.push_notification_metadata
        end

        def increment_upvotable_owner_upvotes_count_column
          return false unless RESOURCES_WHO_INCREMENTS_OWNER_COUNTER.member?(record.upvotable_type)

          owner = resource.user
          owner.try(:increment!, :received_upvotes_count)
        end

      end
    end
  end
end
