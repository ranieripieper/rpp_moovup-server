# encoding: UTF-8

module Moovup
  module V1
    module Upvotes
      class DeleteService < BaseDeleteService

        record_type ::Upvote

        RESOURCES_WHO_DECREMENTS_OWNER_COUNTER = [
          'Answer'
        ]

        def upvotes_count
          @record.try(:upvotes_count) || 0
        end

        def resource_type
          @record && @record.upvotable.class.to_s
        end

        def upvotable_resource
          @record && @record.upvotable
        end

        def record_not_found_error
          'users.not_upvoted_resource'
        end

        def upvotes_count
          return nil if @record.blank? || @record.upvotable_id.blank?
          @record.upvotable.reload.upvotes_count
        end

        def after_success
          decrement_upvotable_owner_upvotes_count_column
          update_question_cover_image
        end

        def decrement_upvotable_owner_upvotes_count_column
          return false unless RESOURCES_WHO_DECREMENTS_OWNER_COUNTER.member?(record.upvotable_type)

          owner = record.user
          owner.try(:decrement!, :received_upvotes_count)
        end

        def update_question_cover_image
          return unless upvotable_resource.is_a?(Answer)

          if upvotable_resource.has_image?
            ::Moovup::V1::QuestionUpdateCoverImageWorker.perform_async(upvotable_resource.question_id)
          end
        end


        alias :question :upvotable_resource
        alias :answer   :upvotable_resource
        alias :comment  :upvotable_resource
        alias :goal     :upvotable_resource
        alias :activity :upvotable_resource

      end
    end
  end
end
