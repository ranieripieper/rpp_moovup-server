module Moovup
  module V1
    module PushNotifications
      class QuestionsToAnswerDeliveryService < BaseActionService

        action_name :questions_to_answer_push_notification_delivery

        NOTIFICATION_TYPE = :new_questions_to_answer

        def execute_action
          @_sent_pushes ||= 0

          scoped_users.find_each do |user|
            if send_push_notification_for_user?(user)
              notify_user(user)
              @_sent_pushes += 1
            end
          end
        end

        def send_push_notification_for_user?(user)
          return false unless user

          return false unless can_delivery_push_for_user?(user)

          user.questions_to_reply_feed.exists?
        end

        def can_delivery_push_for_user?(user)
          !(user
          .push_notification_histories
          .where("notification_type = ? AND (now() < (delivery_at + interval '1 hours'))", NOTIFICATION_TYPE)
          .exists?)
        end

        def notify_user(user)
          create_system_notification_async(user, NOTIFICATION_TYPE)
          send_push_notification_async(user, NOTIFICATION_TYPE) if user.devices.exists?
        end

        def success_runned_action?
          @_sent_pushes && @_sent_pushes > 0
        end

        def valid_record?
          true
        end

        def valid_user?
          true
        end

        def user_can_execute_action?
          true
        end

        def record_error_key
          :users
        end

        def scoped_users
          # only user with interests
          User.joins(:interests).group('users.id')
        end
      end
    end
  end
end
