module Moovup
  module V1
    module PushNotifications
      class InviteFriendDeliveryService < BaseActionService

        action_name :invite_friend_push_notification_delivery

        NOTIFICATION_TYPE = :invite_friends

        def execute_action
          @_sent_pushes ||= 0

          # only user with interests
          scoped_users.each do |user|
            if send_push_notification_for_user?(user)
              notify_user(user)
              @_sent_pushes += 1
            end
          end
        end

        def scoped_users
          User.joins(:devices).find_each
        end

        def send_push_notification_for_user?(user)
          return false unless user

          return false unless can_delivery_push_for_user?(user)

          return true
        end

        def can_delivery_push_for_user?(user)
          return false unless user.preference_on?(:notify_official_statement)

          !(user
          .push_notification_histories
          .where("notification_type = ? AND (now() < (delivery_at + interval '24 hours'))", NOTIFICATION_TYPE)
          .exists?)
        end

        def notify_user(user)
          send_push_notification_async(user, NOTIFICATION_TYPE)
          create_system_notification_async(user, NOTIFICATION_TYPE)
        end

        def success_runned_action?
          @_sent_pushes && @_sent_pushes > 0
        end

        def valid_record?
          true
        end

        def valid_user?
          true
        end

        def user_can_execute_action?
          true
        end

        def record_error_key
          :users
        end
      end
    end
  end
end
