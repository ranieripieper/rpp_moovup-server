# encoding: UTF-8

module Moovup
  module V1
    module Activities
      class CreateService < BaseCreateService

        include V1::ServiceConcerns::ActivityParams

        record_type ::Activity

        private

        def can_create_record?
          unless valid_goal?
            return not_found_error!('activities.invalid_goal')
          end

          begin
            unless can_create_activity?
              return unprocessable_entity_error!('activities.user_cant_create')
            end
          rescue => e
            return internal_server_error!('activities.cant_create')
          end

          return true
        end

        def build_record
          goal.activities.new(activity_params)
        end

        def can_create_activity?
          return false if goal.blank?

          data_to_check = activity_params.slice(:date, :body_text)

          activity =  goal.activities.where(data_to_check).last

          return true if activity.blank?
          return (activity.created_at + 10.seconds) < Time.now
        end
      end
    end
  end
end
