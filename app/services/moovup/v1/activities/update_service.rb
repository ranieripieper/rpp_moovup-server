# encoding: UTF-8

module Moovup
  module V1
    module Activities
      class UpdateService < BaseUpdateService

        include V1::ServiceConcerns::ActivityParams

        record_type ::Activity

        private
        def record_params
          # cant change goal_id after create
          activity_params.delete(:goal_id)
          activity_params
        end

      end
    end
  end
end
