module Moovup
  module V1
    module Activities
      class UpvoteCreateService < Upvotes::CreateService

        alias :activity :resource

        def resource_type
          ::Activity.to_s
        end

        def notification_type
          :new_like_for_activity
        end

        def can_create_record?
          return false unless super

          # user can like your own activity
          return true if @user.id == resource.user_id

          unless resource.user.is_friend_of?(@user)
            return forbidden_error!('activities.only_friends_can_upvote')
          end

          return true
        end
      end
    end
  end
end
