# encoding: UTF-8

module Moovup
  module V1
    module Parse
      class BaseService < ::Moovup::V1::BaseService

        attr_reader :user

        def initialize(user, options = {})
          @user = user
          super(options)
        end

        private
        def register_device_in_parse(device)
          if Moovup::Config.enabled?(:save_user_device_async)
            Moovup::V1::ParseDeviceSaveWorker.perform_async(device.id)
          else
            Moovup::V1::ParseDeviceSaveWorker.new.perform(device.id)
          end
        end

        def channels_for_device(device)
          user = device.user
          user.channels_for_notification(device)
        end

        def parse_client
          @parse_client ||= Moovup::ParseClient.client
        end
      end
    end
  end
end
