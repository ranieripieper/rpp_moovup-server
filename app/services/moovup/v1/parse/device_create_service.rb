module Moovup
  module V1
    module Parse
      class DeviceCreateService < Parse::BaseService

        attr_reader :device

        WHITELIST_PARAMETERS = [
          :token,
          :platform
        ]

        def execute
          if can_create_device?
            @device = build_device
            new_device = @device.new_record?

            if save_device(device)
              create_origin_async(device)
              new_device ? success_created_response : success_response
            else
              unprocessable_entity_error(@device.errors)
            end
          else
            forbidden_error('users.cant_create_device')
          end
        end

        private

        def save_device(device)
          return false unless device

          begin
            device.save
          rescue Exception => e
            return false
          end
        end

        def can_create_device?
          valid_user? && user_can_create_device?
        end

        def after_success
          register_device_in_parse(@device)
        end

        def build_device
          device = user.devices.find_or_initialize_by(device_params)
        end

        def device_params
          (@options[:device] || {}).slice(*WHITELIST_PARAMETERS)
        end

        def user_can_create_device?
          return false unless valid_device?

          !@user.try(:devices).try(:exists?, device_params)
          #!::UserDevice.where(device_params).exists?
        end

        def valid_device?
          return device_params.any?
        end
      end
    end
  end
end
