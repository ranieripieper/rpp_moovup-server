# encoding: UTF-8

module Moovup
  module V1
    module Parse
      class DeviceDeleteService < Parse::BaseService

        attr_reader :device

        def initialize(user, device, options = {})
          @device = device
          super(user, options)
        end

        def execute
          if can_execute_action?
            @device.destroy rescue forbidden_error!('devices.error_destroying')

            begin
              installation = parse_client.installation(device.parse_object_id)
              # installation.parse_delete
            rescue => e
              Rollbar.error(e)
              forbidden_error('devices.error_deleting_in_parse')
            end
          else
            return forbidden_error!('devices.user_cant_delete')
          end
        end

        def can_execute_action?
          return false unless @device

          @device.user_can_delete?(@user)
        end

      end
    end
  end
end
