# encoding: UTF-8

module Moovup
  module V1
    module Goals
      class DeleteService < BaseDeleteService

        record_type ::Goal

        def after_success
          delete_all_associated_notifications
        end

        def delete_all_associated_notifications
          @record.associated_notifications.update_all(deleted_at: Time.zone.now)
        end

      end
    end
  end
end
