# encoding: UTF-8

module Moovup
  module V1
    module Goals
      class UpdateService < BaseUpdateService

        include V1::ServiceConcerns::GoalParams

        record_type ::Goal

        private
        def record_params
          goal_params
        end

        def user_can_update?
          unless valid_category?
            return unprocessable_entity_error!("goals.invalid_category")
          end

          if current_goal_is_closed?
            if changing_goal_category?
              return forbidden_error!('goals.can_change_category_for_closed_goal')
            end
          end

          super
        end

        def changing_goal_category?
          category && @record.category_id != category.id
        end

        def valid_category?
          return true if @options[:goal][:category_id].blank?
          super
        end

        def closed_goal?
          category.try(:closed_goal?)
        end

        def current_goal_is_closed?
          @record.closed_goal?
        end

      end
    end
  end
end
