# encoding: UTF-8

module Moovup
  module V1
    module Goals
      class CreateService < BaseCreateService

        include V1::ServiceConcerns::GoalParams

        record_type ::Goal

        private

        def can_create_record?
          unless valid_category?
            return not_found_error!('categories.not_found')
          end

          if closed_goal?
            unless all_closed_goal_data_received?
              return forbidden_error!('goals.invalid_data_for_closed_goal')
            end

            unless valid_closed_goal_category?
              return forbidden_error!('goals.category_not_closed')
            end
          end

          return true
        end

        def build_record
          @user.goals.build(goal_params)
        end
      end
    end
  end
end

