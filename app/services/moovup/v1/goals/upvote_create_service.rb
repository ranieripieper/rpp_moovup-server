module Moovup
  module V1
    module Goals
      class UpvoteCreateService < Upvotes::CreateService

        alias :goal :resource

        def resource_type
          ::Goal.to_s
        end

        def notification_type
          :new_like_for_goal
        end

        def can_create_record?
          return false unless super

          unless resource.user.is_friend_of?(@user)
            return forbidden_error!('goals.only_friends_can_upvote')
          end

          return true
        end

      end
    end
  end
end
