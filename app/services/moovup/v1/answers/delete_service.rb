# encoding: UTF-8

module Moovup
  module V1
    module Answers
      class DeleteService < BaseDeleteService

        record_type ::Answer

        def after_success
          update_question_cover_image
        end

        def update_question_cover_image
          if @record.has_image?
            ::Moovup::V1::QuestionUpdateCoverImageWorker.perform_async(@record.question_id)
          end
        end

      end
    end
  end
end
