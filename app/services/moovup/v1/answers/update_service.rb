# encoding: UTF-8

module Moovup
  module V1
    module Answers
      class UpdateService < BaseUpdateService

        include V1::ServiceConcerns::AnswerParams

        record_type ::Answer

        private
        def whitelist_attributes_to_clear
          [:image]
        end

        def record_params
          answer_params
        end

        def after_success
          if removing_attribute?(:image)
            @record.remove_image!
            @record.save
          end
          update_question_cover_image
        end

        def update_question_cover_image
          if (removing_attribute?(:image) || (changed_attributes.member?('image') && @record.has_image?))
           ::Moovup::V1::QuestionUpdateCoverImageWorker.perform_async(@record.question_id)
          end
        end

      end
    end
  end
end
