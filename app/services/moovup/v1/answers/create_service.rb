# encoding: UTF-8

module Moovup
  module V1
    module Answers
      class CreateService < BaseCreateService

      include V1::ServiceConcerns::AnswerParams

        record_type ::Answer

        private
        def can_create_record?
          unless can_create_answer?
            return unprocessable_entity_error!('answers.duplicate_title')
          end

          unless valid_question?
           return not_found_error!('answers.invalid_question')
          end

          if creating_comment_for_answer?
            unless valid_parent_answer?
              return not_found_error!('answers.invalid_answer')
            end
          end

          return true
        end

        def build_record
          answer_data = creating_comment_for_answer? ? answer_comment_params : answer_params
          @user.answers.build(answer_data)
        end

        def answer_comment_params
          answer_params.merge(parent_id: parent_id)
        end

        def can_create_answer?
          answer = @user.answers.where(body_text: answer_params[:body_text],
                                       question_id: @options[:question_id]).last

          return true if answer.blank?
          return (answer.created_at + 10.seconds) < Time.now
        end

        def after_success
          create_ignored_question
          update_question_cover_image
          notify_users
        end

        protected
        def update_question_cover_image
          if @record.has_image?
            ::Moovup::V1::QuestionUpdateCoverImageWorker.perform_async(@record.question_id)
          end
        end

        def notify_users
          if creating_comment_for_answer?
            n_user, n_type, n_notificable = parent_answer.user, :new_comment_for_answer, answer
          else
            n_user, n_type, n_notificable = question.user, :new_answer_for_question, question
          end

          create_system_notification_async(n_user, n_type, n_notificable, sender_user_id: @user.id)
          send_push_notification_async(n_user, n_type, n_notificable, origin_user: @user)
        end

        def create_ignored_question
          method_to_call = Moovup::Config.enabled?(:create_ignored_question_async) ? :async : :sync
          self.send("create_ignored_question_#{method_to_call}")
        end

        def create_ignored_question_async
          ::Moovup::V1::IgnoredQuestionCreateWorker.perform_async(@user.id, ignored_question_data)
        end

        def create_ignored_question_sync
          ::Moovup::V1::IgnoredQuestionCreateWorker.new.perform(@user.id, ignored_question_data)
        end

        def ignored_question_data
          @iq_data ||= { question_id: question.id, reason: ::IgnoredQuestion::REASONS[:replied] }
        end

      end
    end
  end
end
