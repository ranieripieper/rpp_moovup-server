module Moovup
  module V1
    module Answers
      class UpvoteCreateService < Upvotes::CreateService

        alias :answer :resource

        def resource_type
          Answer.to_s
        end

        def notification_type
          :new_like_for_answer
        end

        def after_success
          super
          update_question_cover_image
        end

        def update_question_cover_image
          if upvotable_resource.has_image?
            ::Moovup::V1::QuestionUpdateCoverImageWorker.perform_async(upvotable_resource.question_id)
          end
        end

      end
    end
  end
end
