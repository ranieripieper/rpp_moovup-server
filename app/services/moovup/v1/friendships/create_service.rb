# encoding: UTF-8

module Moovup
  module V1
    module Friendships
      class CreateService < BaseCreateService

        include V1::ServiceConcerns::FriendshipParams

        record_type ::Friendship

        private
        def can_create_record?
          unless can_create_friendship?
            return unprocessable_entity_error!('friendships.duplicated_request')
          end

          unless valid_friend_user?
            return unprocessable_entity_error!('friendships.invalid_friend_user')
          end

          return true
        end

        def build_record
          @user.friendships.build(friendship_params)
        end

        def valid_friend_user?
          @valid_friend_user ||= ::User.exists?(id: friendship_params[:friend_id])
        end

        def can_create_friendship?
          return true
        end

        def after_success
          notify_users
        end

        def notify_users
          n_user, n_type, n_notificable = @record.friend, :new_friendship_request, @record

          create_system_notification_async(n_user, n_type, n_notificable, sender_user_id: @user.id)
          send_push_notification_async(n_user, n_type, n_notificable, origin_user: @user)
        end

      end
    end
  end
end
