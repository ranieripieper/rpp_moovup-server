# encoding: UTF-8

module Moovup
  module V1
    module Friendships
      class UpdateService < BaseUpdateService

        include V1::ServiceConcerns::FriendshipParams

        record_type ::Friendship

        private
        def record_params
          friendship_params.merge!(status: @options[:status]).keep_if { |_,v| v.present? }
        end

        def after_success
          if create_friendship?
            @updated_record.exchange_friend_register(@user)
            notify_users
          elsif cancel_friendship?
            @updated_record.decline_friendship
          end

          update_records_status_changed_at
        end

        def notify_users
          n_user, n_type, n_notificable = @record.user, :friendship_request_accepted, @record.friend

          create_system_notification_async(n_user, n_type, n_notificable, sender_user_id: @user.id)
          send_push_notification_async(n_user, n_type, n_notificable, origin_user: @user)
        end

        def update_records_status_changed_at
          if changed_attributes.member?("status")
            @record.update_attribute(:status_changed_at, Time.zone.now)
            @record.other_side_record.update_attribute(:status_changed_at, Time.zone.now) if @record.other_side_record
          end
        end

        def create_friendship?
          #TODO validar se parâmetros são válidos, após o after_succes tanto o record como o updated_record vem como accepted? true
          status_match?(:accepted?, :accepted?)
        end

        def decline_friendship?
          status_match?(:declined?, :declined?)
        end

        def cancel_friendship?
          status_match?(:declined?, :declined?)
        end

        def status_match?(last_status, new_status)
          return @record.send(last_status) && @updated_record.send(new_status)
        end

      end
    end
  end
end
