module Moovup
  module V1
    module Friendships
      class BatchDeclineService < BaseActionService

        action_name :batch_decline

        attr_reader :user, :friendships

        def initialize(user, options = {})
          @user        = user
          @friendships = []

          super(options)
        end

        def execute_action
          update_friendships
        end

        private
        def user_can_execute_action?

          if friends_ids.blank?
            return forbidden_error!("#{record_error_key}.invalid_friend_ids")
          end

          if user_friendships.blank?
            return forbidden_error!("#{record_error_key}.invalid_friendships")
          end

          return true
        end

        def update_friendships
          return nil if user_friendships.blank?

          @updated_friendships = []

          user_friendships.each do |friendship|
            @updated_friendships << migrate_friendship_to_canceled(friendship)
          end
        end

        def user_friendships
          return @friendships if @friendships.present?

          @friendships = find_user_friendships(friends_ids)
        end

        def success_runned_action?
          declined_friendships.any?
        end

        def declined_friendships
          user_friendships.select(&:canceled?)
        end

        def friends_ids
          array_values_from_params(@options, :friend_ids)
        end

        def find_user_friendships(friends_ids)
          return [] if friends_ids.blank?

          @user.accepted_friendships.where(friend_id: friends_ids)
        end

        def migrate_friendship_to_canceled(friendship)
          return nil if friendship.blank?

          another_friendship = friendship.other_side_record

          friendship.migrate_to_canceled
          another_friendship.migrate_to_canceled

          friendship
        end

        def valid_record?
          return true
        end

        def record_error_key
          :friendships
        end
      end
    end
  end
end
