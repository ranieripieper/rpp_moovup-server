# encoding: UTF-8

module Moovup
  module V1
    class AuthenticationCreateService < BaseService

      attr_reader :email, :password, :user, :auth_token

      delegate :id, :username,
               :profile_image_url, :profile_type,
               :oauth_provider, :oauth_provider_uid,
               to: :user, prefix: :user

      delegate :provider, :expires_at, :token, to: :auth_token, prefix: :user_auth

      def initialize(email, password, options={})
        super(options, 401)
        @email = email
        @password = password
      end

      def execute
        if can_execute_action?

          @user = User.authenticate(@email, @password)

          if valid_user?
            unless account_activated?
              return forbidden_error!('user_account_not_activated')
            end

            unless check_user_login_block(@user, false)
              success_created_response
            end
          else
            unless check_user_login_block(user_from_email)
              not_authorized_error('invalid_user_credentials')
            end
          end
        end

        success?
      end

      def login_blocked?
        valid_object?(@blocked_user, ::User)
      end

      def login_block_until
        login_blocked? && @blocked_user.blocked_until
      end

      def account_activated?
        @user.try(:account_activated?)
      end

      def new_user?
        @user.first_login_on_provider?(auth_provider)
      end

      private
      def check_user_login_block(user, update_counter = true)
        return false unless user

        user.update_login_attempts_counter if update_counter

        if user.blocked?
          @blocked_user = user
          blocked_until = user.blocked_until.to_formatted_s(:short)
          not_authorized_error('user_login_blocked', blocked_until: blocked_until)
        end

        return user.blocked?
      end

      def user_from_email
        @user_from_email ||= User.find_by(email: @email)
      end

      def can_execute_action?
        unless valid_provider?
          return invalid_provider_response && false
        end

        return true
      end

      def after_success
        execute_after_success_actions
      end

      def execute_after_success_actions
        update_user_login_block
        clear_provider_authorizations
        register_new_devices_for_push
        create_authorization_for_provider
        update_user_notifications_channels
      end

      def register_new_devices_for_push
        device_data = @options.slice(:device) || (@options[:user] && @options[:user]).slice(:device)
        if Moovup::Config.enabled?(:save_user_device_async)
          ::Moovup::V1::ParseDeviceCreateWorker.perform_async(@user.id, device_data, true)
        else
          ::Moovup::V1::ParseDeviceCreateWorker.new.perform(@user.id, device_data, true)
        end
      end

      def update_user_login_block
        @user.unblock!
      end

      def valid_providers
        Authorization::PROVIDERS.keys.map(&:to_sym)
      end

      def valid_provider?
        return valid_providers.member?(auth_provider.to_sym)
      end

      def auth_provider
        @provider ||= @options[:provider]

        @provider.to_s.downcase
      end

      def create_authorization_for_provider
        if @auth_token = @user.authorizations.create(provider: auth_provider)
          @user.update_login_count_from_provider!(auth_provider)
        end

        @auth_token
      end

      def update_user_notifications_channels
       ::Moovup::V1::UpdateUserParseChannelsWorker.perform_async(@user.id)
      end

      def clear_provider_authorizations
        @user.authorizations.where(provider: auth_provider).delete_all
      end

      def invalid_provider_response
        bad_request_error('invalid_auth_provider', valid_providers: valid_providers.to_sentence)
      end
    end
  end
end
