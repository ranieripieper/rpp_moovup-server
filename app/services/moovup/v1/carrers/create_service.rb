# encoding: UTF-8

module Moovup
  module V1
    module Carrers
      class CreateService < BaseCreateService

        include V1::ServiceConcerns::CarrerParams

        record_type ::Carrer

        private
        def can_create_record?
          return @user.admin?
        end

        def build_record
          ::Carrer.new(carrer_params)
        end
      end
    end
  end
end
