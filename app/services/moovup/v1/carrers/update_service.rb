# encoding: UTF-8

module Moovup
  module V1
    module Carrers
      class UpdateService < BaseUpdateService

        include V1::ServiceConcerns::CarrerParams

        record_type ::Carrer

        private
        def record_params
          carrer_params
        end
      end

    end
  end
end
