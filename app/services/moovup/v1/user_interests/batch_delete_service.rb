module Moovup
  module V1
    module UserInterests
      class BatchDeleteService < BaseBatchService

        action_name :batch_delete_user_interests

        attr_reader :user, :interests

        def execute_action
          delete_interests
        end

        private
        def categories_ids
          array_values_from_params(@options, :categories_ids)
        end

        def delete_interests
          return nil if categories.blank?
          categories.each do |category|
            @interests << delete_interest_for(category)
          end
        end

        def delete_interest_for(category)
          return nil if category.blank?

          service = delete_interest_with_service(category)
          service.user_interest
        end

        def delete_interest_with_service(category)
          category_id = category.is_a?(Integer) ? category : category.id
          interest = @user.interests.find_by(category_id: category_id)

          service = DeleteService.new(interest, @user)
          service.execute

          service
        end
      end
    end
  end
end
