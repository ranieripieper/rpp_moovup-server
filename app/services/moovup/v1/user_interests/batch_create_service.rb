module Moovup
  module V1
    module UserInterests
      class BatchCreateService < BaseBatchService

        action_name :batch_create_user_interests

        def execute_action
          create_interests
        end

        private
        def create_interests
          return nil if categories.blank?

          categories.each do |category|
            @interests << create_interest_for(category)
          end
        end

        def create_interest_for(category)
          return nil if category.blank?

          service = create_interest_with_service(category)
          service.user_interest
        end

        def max_answers_count_for(category_id)
          category_data = data_for_category(category_id)
          category_data.is_a?(Hash) ? category_data[:max_answers_count].presence || nil : nil
        end

        def create_interest_with_service(category)
          category_id = category.is_a?(Integer) ? category : category.id
          max_answers_count = max_answers_count_for(category_id)
          options = {
            category_id: category_id,
            max_answers_count: max_answers_count
          }

          service = CreateService.new(@user, interest: options)
          service.execute

          service
        end
      end
    end
  end
end
