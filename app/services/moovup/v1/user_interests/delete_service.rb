# encoding: UTF-8

module Moovup
  module V1
    module UserInterests
      class DeleteService < BaseDeleteService

        record_type ::UserInterest

      end
    end
  end
end
