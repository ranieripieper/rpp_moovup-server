module Moovup
  module V1
    module UserInterests
      class BaseBatchService < BaseActionService

        attr_reader :user, :interests

        def initialize(user, options = {})
          @user        = user
          @interests = []

          super(options)
        end

        private
        def user_can_execute_action?

          if categories_ids.blank?
            return forbidden_error!("#{record_error_key}.invalid_categories_ids")
          end

          if categories.blank?
            return forbidden_error!("#{record_error_key}.invalid_categories")
          end

          return true
        end

        def categories
          return @categories if @categories.present?

          @categories = find_categories(categories_ids)
        end

        def success_runned_action?
          @interests.any?
        end

        def categories_ids
          @options[:interests].map {|v| v[:category_id] }
        end

        def find_categories(category_ids)
          return [] if category_ids.blank?

          Category.where(id: category_ids)
        end

        def data_for_category(category_id)
          @options[:interests].find {|v| v[:category_id].to_s == category_id.to_s }
        end

        def valid_record?
          return true
        end

        def record_error_key
          :user_interests
        end
      end
    end
  end
end
