# encoding: UTF-8

module Moovup
  module V1
    module UserInterests
      class CreateService < BaseCreateService

        include V1::ServiceConcerns::UserInterestParams

        record_type ::UserInterest

        private

        def can_create_record?
          unless valid_category?
            return not_found_error!('categories.not_found')
          end

          return true
        end

        def build_record
          @user.interests.build(interest_params)
        end

        def save_record
          begin
            super
          rescue ActiveRecord::RecordNotUnique, PG::UniqueViolation => e
            # search interest to update and defines as "new record"
            @record = @user.interests.find_by(category_id: interest_params[:category_id])

            if @record
              whitelist_attrs = interest_params.slice(:max_answers_count)
              return @record.update_attributes(whitelist_attrs)
            end

            return false
          end
        end

      end
    end
  end
end

