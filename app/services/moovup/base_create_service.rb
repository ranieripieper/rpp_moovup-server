# encoding: UTF-8

module Moovup
  class BaseCreateService < BaseCrudService

    def initialize(user, options={})
      @user = user
      super(nil, user, options)
    end

    def execute
      if can_execute_action?
        @record = build_record

        if save_record
          after_execute_success_response
          create_origin_for_record
          create_roles_for_users
        else
          errors = create_error_response(@record)
          after_error_response(errors)
        end
      end

      success?
    end

    private
    def save_record
      @record.save
    end

    def create_error_response(record)
      record.errors
    end

    def after_error_response(errors)
      unprocessable_entity_error(errors) if errors.present?
    end

    def after_execute_success_response
      success_created_response
    end

    def after_success
    end

    def create_roles_for_users
      # TODO: think about rolify
      # @user.add_role :admin, @record if @user.respond_to?(:add_role)
    end

    def create_origin_for_record
      if @record.respond_to?(:create_origin)
        create_origin_async(@record, @options)
      end
    end

    def build_record
      not_implemented_exception(__method__)
    end

    def can_execute_action?
      unless valid_user?
        return not_found_error!('users.not_found')
      end

      unless can_create?
        if @errors.blank?
          return forbidden_error!("#{record_error_key}.user_cant_create")
        else
          return false
        end
      end

      return valid?
    end

    def can_create?
      return false unless valid_user?

      return can_create_record?
    end

    def can_create_record?
      not_implemented_exception(__method__)
    end
  end
end
