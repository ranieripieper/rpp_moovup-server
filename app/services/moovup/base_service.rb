# encoding: UTF-8

module Moovup
  class BaseService

    include ServiceConcerns::BaseConcern

    attr_reader :options, :response_status, :errors

    CALLBACKS = [
      :after_initialize,
      :before_error,
      :after_error,
      :after_success,
      :before_success
    ]

    ERROR_RESPONSE_METHODS = {
      :bad_request_error          => 400,
      :not_authorized_error       => 401,
      :forbidden_error            => 403,
      :not_found_error            => 404,
      :unprocessable_entity_error => 422,
      :internal_server_error      => 500
    }

    ERRORS_I18N_NAMESPACE = 'moovup.errors'

    CALLBACKS.each do |callback|
      define_method "#{callback}" do
        nil
      end
    end

    def initialize(options = {}, initial_response_status = 400)
      @options         = set_default_options(options)
      @response_status = initial_response_status
      @errors          = []

      call_callback(:after_initialize)
    end

    def valid?
      return @errors.blank?
    end

    def success?
      @success == true && valid?
    end

    def fail?
      !success?
    end

    def response_status
      @response_status ||= 400
    end

    def changed?
      changed_attributes.any?
    end

    def callback_fired?(callback_name)
      return @_fired_callbacks && @_fired_callbacks[callback_name.to_sym].present?
    end

    alias :callback_called? :callback_fired?

    def origin_params(params={})
      origin_data = (params.is_a?(Hash) && params.present? ? params : @options)[:origin] || {}
      origin_data = origin_data.slice(:provider, :locale, :user_agent, :ip)
    end

    def option_enabled?(key)
      options_var = @options
      options_var && options_var.key?(key) && options_var[key] == true
    end

    def option_disabled?(key)
      !option_enabled?(key)
    end

    private

    def create_origin(originable, params = {})
      origin_params = origin_params(params)

      origin_params = origin_params.merge!({
        originable_type: originable.class.to_s,
        originable_id: originable.id,
        fetch_originable: true
      }).dup

      async = params.delete(:async) == true

      if async
        Moovup::V1::OriginCreateWorker.perform_async(origin_params)
      else
        Moovup::V1::OriginCreateWorker.new.perform(origin_params)
      end

    end

    def create_origin_async(originable, params = {})
      params.merge!(async: Moovup::Config.enabled?(:create_records_origin_async))

      create_origin(originable, params)
    end

    ERROR_RESPONSE_METHODS.each do |error_method, status_code|
      error_method = error_method.match(/_error$/) ? error_method : error_method.to_s + '_error'

      define_method error_method do |message_key, options={}|
        error(status_code, message_key, options)
      end

      define_method "#{error_method}!" do |message_key, options={}|
        error!(status_code, message_key, options)
      end
    end

    def error(status, message_key, options = {})
      call_callback(:before_error)

      @response_status = status
      error_message = process_error_message_for_key(message_key, options)

      error = add_error(error_message)

      call_callback(:after_error)

      error_message
    end

    def error!(status, message_key, options = {})
      error(status, message_key, options)

      # TODO:
      # maybe throw a Exception making bang(!) semantic
      # raise "Moovup::V1::Exceptions::#{status.titleize}".constantize
      return false
    end

    def process_error_message_for_key(message_key, options)
      if message_key.is_a?(ActiveModel::Errors)
        message = message_key.messages # .messages
      elsif message_key.is_a?(Array) && message_key.first.is_a?(Hash)
        message = message_key.map(&:values).flatten.to_sentence
      elsif message_key.is_a?(Array)
        message = message_key.flatten.to_sentence
      else
        if options[:no_i18n].present?
          message = message_key
        else
          message = I18n.t("#{ERRORS_I18N_NAMESPACE}.#{message_key}", options)
        end
      end

      message
    end

    def success_response(status=200)
      call_callback(:before_success)

      @response_status = status
      @success         = true

      call_callback(:after_success)
    end

    def success_created_response
      success_response(201)
    end

    def call_callback(callback_name)
      @_fired_callbacks ||= {}
      callback_name = callback_name.to_s.underscore.to_sym

      if self.respond_to?(callback_name, true)
        @_fired_callbacks[callback_name] = true
        self.send(callback_name)
      end
    end

    def valid_object?(record, expected_class)
      record.present? && record.is_a?(expected_class)
    end

    def valid_user?
      valid_object?(@user, User)
    end

    def filter_hash(hash, whitelist_keys=[])
      (hash || {}).symbolize_keys.slice(*whitelist_keys.map(&:to_sym))
    end

    def changes(old, current, attributes={})
      changes = []

      return changes if old.blank? || current.blank?

      old_attributes = old.attributes.slice(*attributes.map(&:to_s))
      new_attributes = current.attributes.slice(*attributes.map(&:to_s))

      new_attributes.each do |attribute, value|
        if old_attributes[attribute] != value
          changes << attribute
        end
      end

      changes
    end

    def add_error(error)
      @errors ||= []
      _method = error.is_a?(Array) ? :concat : :push
      @errors.send(_method, error)
    end

    def create_system_notification(user, type, notificable = nil, options = {})
      return false unless create_system_notification?

      options = options.merge(type: type.to_sym, notificable: notificable)

      service = ::Moovup::V1::Users::NotificationCreateService.new(user, options)
      service.execute
    end

    def create_system_notification_async(user, type, notificable=nil, options={})
      options.merge!(
        type: type.to_sym,
        notificable: notificable,
        notificable_type: notificable.presence && notificable.class.to_s,
        notificable_id: notificable.presence && notificable.try(:id)
      )

      if Moovup::Config.enabled?(:create_system_notification_async)
        ::Moovup::V1::NotificationCreateWorker.perform_async(user.try(:id), options)
      else
        create_system_notification(user, type, notificable, options)
      end
    end

    def delivery_sync_email(mailer, mail_method, *args)
      delivery_email(create_mail_message_for_mailer(mailer, mail_method, *args), :deliver_now)
    end

    def delivery_async_email(mailer, mail_method, *args)
      delivery_email(create_mail_message_for_mailer(mailer, mail_method, *args))
    end

    def create_mail_message_for_mailer(mailer, mail_method, *args)
      mailer.to_s.constantize.send(mail_method, *args)
    end

    def delivery_email(mail, method=:deliver_later)
      return false unless delivery_email?

      mail.send(method)
    end

    def set_default_options(options={})
      {
        delivery_email: true,
        send_push_notification: true,
        create_system_notification: true
      }.merge(options.symbolize_keys).deep_symbolize_keys
    end

    def create_system_notification?
      option_enabled?(:create_system_notification)
    end

    def delivery_email?
      option_enabled?(:delivery_email)
    end

    def send_push_notification_async(user, type, notificable=nil, options={})
      options.merge! async: Moovup::Config.enabled?(:send_push_notifications_async)

      send_push_notification(user, type, notificable, options)
    end

    def send_push_notification_sync(user, type, notificable=nil, options={})
      options.merge! async: false

      send_push_notification(user, type, notificable, options)
    end

    def user_preference_on?(user, preference_key)
      return false unless user

      user.preference_on?(preference_key)
    end

    def user_preference_off?(user, preference_key)
      !user_preference_on?(user, preference_key)
    end

    def send_push_notification(user, type, notificable = nil, options = {})
      return false unless send_push_notification?
      return false unless user_preference_on?(user, notification_preference_key(type))

      push_data = push_notification_data(user, notificable, type, options)

      async = options[:async].eql?(true)

      if async
        Moovup::V1::PushNotificationDeliveryWorker.perform_async(user.id, push_data)
      else
        Moovup::V1::PushNotificationDeliveryWorker.new.perform(user.id, push_data)
      end
    end

    def push_notification_data(user, notificable, notification_type, options = {})
      data  = Moovup::NotificationMetaParse.new(user, options[:origin_user], notificable).parse
      body  = I18n.t(notification_type, scope: 'moovup.notifications')

      alert = (body % data) rescue body
      sanitized_alert = Moovup::Helpers.strip_tags(alert)

      push_data = data_for_push(user, options[:origin_user], sanitized_alert, notificable, notification_type, options)

      push_config = {
        alert: sanitized_alert,
      }

      return push_config if options[:only_alert].present?

      push_config.merge(data: push_data)
    end

    def data_for_push(receiver_user, sender_user, message, notificable, notification_type, options = {})
      Moovup::NotificationDataFormatter.new(receiver_user, sender_user, message, notificable, notification_type, options).format
    end

    def notification_preference_key(notification_type)
       "notify_#{notification_type.to_s.downcase.underscore}".to_sym
    end

    def send_push_notification?
      option_enabled?(:send_push_notification)
    end


    protected
    def not_implemented_exception(method_name)
      raise NotImplementedError, "#{method_name} must be implemented in subclass"
    end
  end
end
