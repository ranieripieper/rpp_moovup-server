module Moovup
  module V1
    class CategorySerializer < SimpleCategorySerializer

      attributes :children

      def children
        return [] if ignore_children?

        serializer_options = { each_serializer_meta: @meta, meta: @meta, scope: @scope, each_serializer: CategorySerializer }

        return ActiveModel::ArraySerializer.new(object.children, serializer_options)
      end

      protected
      def ignore_children?
        @meta && @meta[:ignore_children].present?
      end

    end
  end
end
