module Moovup
  module V1
    class SimpleFriendshipSerializer < ActiveModel::Serializer

      root false

      attributes :id, :friend_id, :user_id, :status,
                 :status_changed_at, :created_at, :updated_at

    end
  end
end
