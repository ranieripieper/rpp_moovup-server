module Moovup
  module V1
    class SimpleCarrerSerializer < ActiveModel::Serializer

      root false

      attributes :id, :title, :created_at, :updated_at

    end
  end
end
