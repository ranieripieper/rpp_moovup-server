module Moovup
  module V1
    class SimpleQuestionSerializer < ActiveModel::Serializer

      root false

      attributes :id, :body_text, :user_id, :upvotes_count, :created_at, :updated_at,
                 :favorited_count, :category_id, :answers_count, :favorited, :cover_image_url


      def favorited
        return true if @meta && @meta[:force_favorited].present?
        return nil
      end

    end
  end
end
