module Moovup
  module V1
    class SimpleReportSerializer < ActiveModel::Serializer

      root false

      attributes :id, :user_id, :reportable_type, :report_type,
                 :reported_user_id, :created_at, :updated_at


    end
  end
end
