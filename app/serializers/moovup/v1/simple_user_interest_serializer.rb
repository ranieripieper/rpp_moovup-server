module Moovup
  module V1
    class SimpleUserInterestSerializer < ActiveModel::Serializer

      root false

      attributes :id, :user_id, :category_id, :max_answers_count, :created_at, :updated_at

    end
  end
end
