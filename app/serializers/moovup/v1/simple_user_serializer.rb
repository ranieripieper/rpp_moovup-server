module Moovup
  module V1
    class SimpleUserSerializer < ActiveModel::Serializer

      root false

      attributes :id, :profile_type, :first_name, :last_name, :username, :fullname,
                 :has_uploaded_image?, :profile_image_url, :profile_images,
                 :birthday_date, :gender, :received_upvotes_count, :created_at, :updated_at

    end
  end
end
