module Moovup
  module V1
    class CurrentUserSerializer < UserSerializer

      attributes :email, :preferences, :unread_notifications_count,
                 :commercial_activity_id, :carrer_id, :hobby_id

      has_one :commercial_activity, serializer: SimpleCommercialActivitySerializer

      has_one :carrer, serializer: SimpleCarrerSerializer

      has_one :hobby,  serializer: SimpleCategorySerializer

    end
  end
end
