module Moovup
  module V1
    class UserInterestSerializer < SimpleUserInterestSerializer

     has_one :category,
             serializer: SimpleCategorySerializer,
             embed: :ids,
             embed_in_root_key: :linked_data,
             embed_in_root: true

     has_one :user,
             serializer: SimpleUserSerializer,
             embed: :ids,
             embed_in_root_key: :linked_data,
             embed_in_root: true

    end
  end
end
