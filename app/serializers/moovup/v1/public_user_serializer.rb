module Moovup
  module V1
    # can inherit SimpleUserSerializer here, but it's better
    # to not mix things
    class PublicUserSerializer < ActiveModel::Serializer

      root false

      attributes :id, :profile_type, :first_name, :last_name, :username, :fullname,
                 :has_uploaded_image?, :profile_image_url, :profile_images,
                 :birthday_date, :gender, :created_at, :updated_at, :friendship_status

      def friendship_status
        # This will avoid a HUGE N+1 using `search_with_friendship_status` scope in User model.
        if object.respond_to?(:mine_friendship_status) && object.respond_to?(:user_friendship_status)
          friendship_prefix = 'user'
          friendship_status = object.user_friendship_status

          if friendship_status.blank?
            friendship_status = object.mine_friendship_status
            friendship_prefix = 'self'
          end

          return mount_friendship_status(friendship_status, friendship_prefix)
        end

        fetch_friendship_status
      end

      def fetch_friendship_status
        # serializer.user(object) is in charge for final decision(accept, decline, cancel)
        friendship_prefix = 'user'
        # search current_user created(sent) friendships
        friendship = scope.friendships.find_by(friend_id: object.id)

        # search serializer.user(object) created(sent) friendships
        if friendship.blank? && friendship = object.friendships.find_by(friend_id: scope.id)
          # current_user
          friendship_prefix = 'self'
        end

        mount_friendship_status(friendship.try(:status), friendship_prefix)
      end

      def mount_friendship_status(status, prefix)
        return 'no_friendship' if status.blank?
        [prefix, status].join('_')
      end

    end
  end
end
