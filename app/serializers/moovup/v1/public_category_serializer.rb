module Moovup
  module V1
    class PublicCategorySerializer < ActiveModel::Serializer

      root false

      attributes :id, :parent_id, :title, :measurement_type

    end
  end
end
