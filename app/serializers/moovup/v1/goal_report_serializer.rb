module Moovup
  module V1
    class GoalReportSerializer < SimpleGoalSerializer

      attributes :activities_reports

      def activities_reports
        Goal.activities_reports(object.periodicity_type, self.object.id)
      end

    end
  end
end
