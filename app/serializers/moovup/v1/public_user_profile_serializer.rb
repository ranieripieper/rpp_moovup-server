module Moovup
  module V1
    class PublicUserProfileSerializer < PublicUserSerializer

      attributes :received_upvotes_count

      has_one :commercial_activity, serializer: SimpleCommercialActivitySerializer

      has_one :carrer, serializer: SimpleCarrerSerializer

      has_one :hobby,  serializer: SimpleCategorySerializer

      has_many :goals, serializer: SimpleGoalSerializer

      def received_upvotes_count
        object.received_upvotes_count || 0
      end

      def filter(keys)
        blacklist = []

        unless include_goals?
          blacklist = [:goals]
        end

        keys - blacklist
      end

      def goals
        return nil unless include_goals?

        object.public_goals
      end

      protected
      def include_goals?
        @meta && @meta[:include_user_goals].present?
      end

    end
  end
end
