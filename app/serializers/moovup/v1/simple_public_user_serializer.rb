module Moovup
  module V1
    class SimplePublicUserSerializer < ActiveModel::Serializer

      root false

      attributes :id, :profile_type, :first_name, :last_name, :username, :fullname,
                 :has_uploaded_image?, :profile_image_url, :profile_images
    end
  end
end
