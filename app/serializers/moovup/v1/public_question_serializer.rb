module Moovup
  module V1
    class PublicQuestionSerializer < ActiveModel::Serializer

      root false

      attributes :id, :body_text, :upvotes_count, :created_at, :updated_at,
                 :category_id, :answers_count

      has_one :user, serializer: SimplePublicUserSerializer

      has_one :category, serializer: PublicCategorySerializer

      has_many :answers, serializer: PublicQuestionAnswerSerializer


      def answers
        object.recent_answers.take(5)
      end

    end
  end
end
