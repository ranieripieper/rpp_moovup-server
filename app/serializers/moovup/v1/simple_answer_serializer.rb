module Moovup
  module V1
    class SimpleAnswerSerializer < ActiveModel::Serializer

      root false

      attributes :id, :question_id, :user_id, :body_text, :upvotes_count,
                 :parent_id, :replies_count, :comments_count,
                 :has_uploaded_image?, :images_url, :created_at, :updated_at

    end
  end
end
