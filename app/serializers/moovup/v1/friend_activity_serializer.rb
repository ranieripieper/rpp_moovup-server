module Moovup
  module V1
    class FriendActivitySerializer < SimpleActivitySerializer

      attributes :liked

      # FIXME: N+1
      def liked
        return object.like_id.present? if object.respond_to?(:like_id)

        scope && scope.liked?(object)
      end
    end
  end
end
