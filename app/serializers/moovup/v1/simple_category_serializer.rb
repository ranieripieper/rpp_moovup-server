module Moovup
  module V1
    class SimpleCategorySerializer < ActiveModel::Serializer
      root false

      attributes :id, :parent_id, :is_children, :title, :position, :hobby, :measurement_type,
                 :valid_for, :only_valid_for, :not_valid_for, :measurement_text, :measurement_metric_texts,
                 :current_measurement_text, :target_measurement_text, :closed_goal,
                 :created_at, :updated_at


      private
      def is_children
        object.children?
      end

      def measurement_text
        categories_texts[object.title.parameterize] || I18n.t("moovup.categories_texts.#{object.title}")
      end

      def old_measurement_text
        I18n.t("goal_texts.#{i18n_key}", category_name: object.title)
      end

      def measurement_metric_texts
        I18n.t("goal_units.#{i18n_key}")
      end

      def current_measurement_text
        I18n.t("current_goal_texts.#{i18n_key}")
      end

      def target_measurement_text
        I18n.t("target_goal_texts.#{i18n_key}")
      end

      protected
      def i18n_key
        object.measurement_type || 'minutes'
      end

      def categories_texts
        @categories_texts ||= I18n.t("moovup.categories_texts").transform_keys {|key| key.to_s.parameterize }
      end

    end
  end
end
