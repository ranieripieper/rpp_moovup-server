module Moovup
  module V1
    class AnswerSerializer < SimpleAnswerSerializer

      has_one :user,
              serializer: SimpleUserSerializer,
              embed: :ids,
              embed_in_root_key: :linked_data,
              embed_in_root: true

      attributes :liked

      def liked
        scope && scope.upvoted?(object)
      end

    end
  end
end
