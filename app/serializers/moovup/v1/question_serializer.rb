module Moovup
  module V1
    class QuestionSerializer < SimpleQuestionSerializer

      has_one :category, serializer: SimpleCategorySerializer

      has_many :answers, serializer: QuestionAnswerSerializer

      attributes :favorited

      # FIXME: N+1
      def favorited
        return object.favorited_id.present? if object.respond_to?(:favorited_id)

        scope && scope.favorited?(object)
      end

    end
  end
end
