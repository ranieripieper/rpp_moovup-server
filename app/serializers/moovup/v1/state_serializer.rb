module Moovup
  module V1
    class StateSerializer < SimpleStateSerializer

      def attributes
        hash = {}
        if @meta && @meta[:cities]
          hash = { cities: cities.as_json(only: [:id, :name]) }
        end

        return hash if @meta && @meta[:only_cities]

        super.merge(hash)
      end

    end
  end
end
