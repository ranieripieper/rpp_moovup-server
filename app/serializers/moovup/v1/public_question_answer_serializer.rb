module Moovup
  module V1
    class PublicQuestionAnswerSerializer < ActiveModel::Serializer

      root false

      attributes :id, :body_text, :user_id, :upvotes_count,
                 :question_id, :parent_id, :comments_count,
                 :has_uploaded_image?, :images_url, :created_at, :updated_at

      has_one :user,
              serializer: SimplePublicUserSerializer,
              embed: :ids,
              embed_in_root_key: :linked_data,
              embed_in_root: true


    end
  end
end
