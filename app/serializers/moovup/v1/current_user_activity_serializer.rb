module Moovup
  module V1
    class CurrentUserActivitySerializer < SimpleActivitySerializer

      has_one :goal,
              serializer: SimpleGoalSerializer,
              embed: :ids,
              embed_in_root_key: :linked_data,
              embed_in_root: true

      has_one :user,
              serializer: SimpleUserSerializer,
              embed: :ids,
              embed_in_root_key: :linked_data,
              embed_in_root: true


      attributes :liked

      # FIXME: N+1
      def liked
        return object.like_id.present? if object.respond_to?(:like_id)

        scope && scope.liked?(object)
      end

    end
  end
end
