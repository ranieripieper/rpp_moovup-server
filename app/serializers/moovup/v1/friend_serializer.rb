module Moovup
  module V1
    class FriendSerializer < SimpleUserSerializer

      has_one :current_goal, serializer: FriendGoalSerializer

      def current_goal
        object.current_public_goal
      end

    end
  end
end
