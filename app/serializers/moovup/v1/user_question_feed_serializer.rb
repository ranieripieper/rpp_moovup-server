module Moovup
  module V1
    class UserQuestionFeedSerializer < SimpleQuestionSerializer

      has_one :category,
              serializer: SimpleCategorySerializer,
              embed: :ids,
              embed_in_root_key: :linked_data,
              embed_in_root: true

      has_one :user,
              serializer: SimpleUserSerializer,
              embed: :ids,
              embed_in_root_key: :linked_data,
              embed_in_root: true


      attributes :favorited

      # FIXME: N+1
      def favorited
        return object.favorited_id.present? if object.respond_to?(:favorited_id)

        scope && scope.favorited?(object)
      end

    end
  end
end
