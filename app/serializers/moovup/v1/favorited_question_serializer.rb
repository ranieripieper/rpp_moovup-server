module Moovup
  module V1
    class FavoritedQuestionSerializer < SimpleFavoritedQuestionSerializer

      has_one :question, serializer: SimpleQuestionSerializer
      has_one :user, serializer: SimpleUserSerializer

    end
  end
end
