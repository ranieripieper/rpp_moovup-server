module Moovup
  module V1
    class ActivitySerializer < SimpleActivitySerializer

      has_one :goal, serializer: SimpleGoalSerializer

      has_one :user, serializer: SimpleUserSerializer

    end
  end
end
