module Moovup
  module V1
    class SimpleGoalSerializer < ActiveModel::Serializer

      root false

      attributes :id, :user_id, :category_id, :public, :periodicity_type,
                 :target, :current, :target_date, :total, :likes_count,
                 :created_at, :updated_at

      def target
        object.target.to_f
      end

      def current
        object.current.to_f
      end

      def likes_count
        object.upvotes_count
      end

    end
  end
end
