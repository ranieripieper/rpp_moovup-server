module Moovup
  module V1
    class SimpleActivitySerializer < ActiveModel::Serializer

      root false

      attributes :id, :body_text , :total, :date,
                 :goal_id, :upvotes_count, :has_uploaded_image?, :images_url,
                 :created_at, :updated_at
    end
  end
end
