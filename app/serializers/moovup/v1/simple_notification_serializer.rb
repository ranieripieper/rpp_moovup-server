module Moovup
  module V1
    class SimpleNotificationSerializer < ActiveModel::Serializer

      attributes :id, :sender_user_id, :notification_type,
                 :read, :updated_at
    end
  end
end
