module Moovup
  module V1
    class SimpleStateSerializer < ActiveModel::Serializer

      root false

      attributes :id, :name, :cities_count

      def cities_count
        cities.size
      end

      private
      def cities
        @cities ||= object.cities.sort_by(&:name)
      end
    end
  end
end
