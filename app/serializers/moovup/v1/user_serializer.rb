module Moovup
  module V1
    class UserSerializer < SimpleUserSerializer

      has_one :commercial_activity, serializer: SimpleCommercialActivitySerializer

      has_one :carrer, serializer: SimpleCarrerSerializer

      has_one :hobby,  serializer: SimpleCategorySerializer

    end
  end
end
