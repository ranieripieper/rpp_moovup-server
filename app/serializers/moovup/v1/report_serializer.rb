module Moovup
  module V1
    class ReportSerializer < SimpleReportSerializer

      has_one :user, serializer: SimpleUserSerializer
      has_one :reported_user, serializer: SimpleUserSerializer

      attributes :reportable

      VALID_REPORTABLES = [
        'Question',
        'Answer'
      ]

      def reportable
        return nil unless VALID_REPORTABLES.member?(object.reportable_type)

        "Moovup::V1::Simple#{object.reportable_type}Serializer".constantize.new(object.reportable)
      end

    end
  end
end
