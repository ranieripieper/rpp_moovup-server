module Moovup
  module V1
    class FriendGoalSerializer < SimpleGoalSerializer

      has_one :category, serializer: SimpleCategorySerializer

      has_one :current_activity, serializer: FriendActivitySerializer

      def category
        object.try(:category)
      end

      def current_activity
        object.try(:current_activity)
      end

    end
  end
end
