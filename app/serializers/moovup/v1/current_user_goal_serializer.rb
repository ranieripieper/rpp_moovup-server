module Moovup
  module V1
    class CurrentUserGoalSerializer < SimpleGoalSerializer

      has_one :user,
              serializer: UserSerializer,
              embed: :ids,
              embed_in_root_key: :linked_data,
              embed_in_root: true

      has_one :category,
              serializer: SimpleCategorySerializer,
              embed: :ids,
              embed_in_root_key: :linked_data,
              embed_in_root: true

      # has_many :activities, serializer: SimpleActivitySerializer

    end
  end
end
