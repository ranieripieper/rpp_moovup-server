module Moovup
  module V1
    class SimpleFavoritedQuestionSerializer < ActiveModel::Serializer

      root false

      attributes :id, :question_id, :user_id,
                 :created_at, :updated_at

    end
  end
end
