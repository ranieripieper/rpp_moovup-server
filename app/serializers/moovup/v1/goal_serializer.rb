module Moovup
  module V1
    class GoalSerializer < SimpleGoalSerializer

      has_one :user, serializer: SimpleUserSerializer

      has_one :category, serializer: SimpleCategorySerializer

      has_many :activities, serializer: SimpleActivitySerializer

      def filter(keys)
        blacklist = include_activities? ? [] : [:activities]

        keys - blacklist
      end

      def activities
        return nil unless include_activities?

        object.activities
      end

      protected
      def include_activities?
        @meta && @meta[:include_activities].present?
      end

    end
  end
end
