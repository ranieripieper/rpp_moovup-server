# encoding: UTF-8

class FavoritedQuestion < ActiveRecord::Base

  include Accessable

  belongs_to :user
  belongs_to :question

  has_one :origin, as: :originable, dependent: :destroy

  validates :user, :question, presence: true

  validates :question_id, uniqueness: { scope: [:user_id] }
end
