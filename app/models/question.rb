# encoding: UTF-8
class Question < ActiveRecord::Base
  acts_as_paranoid

  include Accessable
  include PgSearch

  BLACKLIST_SEARCH_TERMS = -> { JSON.load(Rails.root.join('config', 'blacklist_search_words.json')) }

  belongs_to :user

  belongs_to :category

  has_one :origin, as: :originable

  scope :not_reported, -> { where(reported_at: nil) }

  has_many :answers, -> { not_reported.popular_recent }, dependent: :destroy
  has_many :recent_answers, -> { not_reported.popular_recent.limit(5) }, class_name: 'Answer'

  has_one :most_upvoted_answer_with_image, -> { not_reported.popular_recent.with_image }, class_name: 'Answer'

  has_many :favorited_questions

  has_many :interests, class_name: 'UserInterest'

  has_many :upvotes, as: :upvotable

  has_many :reports, as: :reportable

  has_many :ignored, class_name: 'IgnoredQuestion'

  # horrible name ;(, but this is the most semantinc for now
  has_many :users_ignoring, through: :ignored, source: :user

  acts_as_tree order: :id, counter_cache: :answers_count

  validates :body_text, :user_id, :category_id, presence: true

  validate :validate_category_must_be_root, if: -> { Moovup::Config.enabled?(:only_allow_root_categories_in_qa) }

  pg_search_scope :search_by_term, against: {
                                      :body_text => 'A'
                                    },
                                    :ignoring => :accents,
                                    using: {
                                      tsearch: {
                                        prefix: Moovup::Config.enabled?(:prefix_questions_search_terms),
                                        any_word: true
                                      },
                                      # TODO:
                                      # https://github.com/Casecommons/pg_search#using-tsvector-columns
                                      # dmetaphone: {}
                                    },
                                    :order_within_rank => "questions.created_at DESC"

  scope :with_like_id, -> {
    select('upvotes.id as like_id, questions.*')
    .joins('LEFT JOIN upvotes ON (upvotes.upvotable_id = questions.id AND upvotes.upvotable_type = \'Question\')')
  }

  scope :with_user_likes, -> (user_id) {
    select('u.id as like_id, questions.*')
    .joins("LEFT JOIN upvotes u ON (u.upvotable_id = questions.id AND u.upvotable_type = \'Question\') AND u.user_id = #{user_id}")
  }

  scope :with_user_favorited, -> (user_id) {
    select('fq.id as favorited_id, questions.*')
    .joins("LEFT JOIN favorited_questions fq ON fq.question_id = questions.id AND fq.user_id = #{user_id}")
  }

  scope :not_reported, -> { where(reported_at: nil) }

  include QuestionConcerns::SQL

  def social_share_image_url
    most_upvoted_answer_with_image.try(:social_share_image_url) || Moovup::Config.default_social_share_image_url
  end

  def to_param_social
    "#{id}-#{body_text}".parameterize
  end

  def update_cover_image
    answer_url = nil
    answer = most_popular_answer_with_image

    answer_url = (answer.has_image? ? answer.image_url : nil) if answer

    self.update_attribute(:cover_image_url, answer_url)
  end

  def most_popular_answer_with_image
    self.answers.elegible_for_question_cover_image.first
  end

  alias :answer_for_cover_image :most_popular_answer_with_image

  def self.blackist_regexp
    words = BLACKLIST_SEARCH_TERMS.call.map {|word| "(#{word})" }.join('|')

    %r{^\s*(#|$)|\b(#{words})\b}
  end

  def self.basic_search(term)
    cleaned_term = term.gsub(self.blackist_regexp, '').squish

    search_by_term(cleaned_term).not_reported.includes(:user, :category)
  end

  def self.full_search(term)
    cleaned_term = term.gsub(self.blackist_regexp, '').squish

    query = search_by_term(cleaned_term)
      .popular
      .with_answers
      .not_reported
      .with_image_answers_order.to_sql

    # I need this second query here to avoid duplicated itens
    select("questions.*")
    .from("(#{query}) as questions")
    .includes(:user, :category)
    .uniq
  end

  def self.search_by_user(term, user_id)
    search_method = Moovup::Config.search_method || :basic_search

    self.send(search_method, term).with_user_favorited(user_id).with_user_likes(user_id)
  end

  protected
  def validate_category_must_be_root
    if self.category_id && self.category.children?
      self.errors.add(:category_id, 'must be a root category')
    end
  end
end
