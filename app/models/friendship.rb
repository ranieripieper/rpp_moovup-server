class Friendship < ActiveRecord::Base

  include Accessable

  acts_as_paranoid

  STATUS = {
    pending:  'pending',
    accepted: 'accepted',
    declined: 'declined',
    blocked:  'blocked',
    canceled: 'canceled'
  }.freeze

  after_initialize :set_friendship_status

  # after_save :create_other_side_record

  STATUS.each do |key, value|
    scope key, -> { where(status: value) }

    define_method("#{key}?") do
      self.status == value
    end

    define_method "migrate_to_#{key}" do
      set_status(value)
    end
  end

  validates :friend_id, :status, :user_id, presence: true

  validates_uniqueness_of :friend_id, :scope => :user_id

  validate :same_user_frienship

  belongs_to :user

  belongs_to :friend, class_name: 'User'

  has_one :origin, as: :originable

  def other_side_record
    return nil if self.friend_id.blank?

    friend.friendships.find_by(friend_id: self.user_id)
  end

  def create_other_side_record
    return nil if self.friend_id.blank?

    friend.friendships.where(friend_id: self.user_id, status: self.status).first_or_create
  end

  def user_can_update?(user)
    super || self.friend_id == user.id
  end

  def decline_friendship
    return nil if self.friend_id.blank?
    #self.friend.friendships.find_or_create_by(friend_id: self.user_id).try(:migrate_to_declined)
    self.friend.friendships.find_by(friend_id: self.user_id).try(:migrate_to_declined)
  end

  def exchange_friend_register(user, save = true, save_method = :save)
    raise 'Invalid save method' if [:save, :save!].exclude?(save_method.to_sym)

    friend = user.friendships.new(status: STATUS[:accepted], friend_id: self.user_id)
    friend.send(save_method) if save

    return friend
  end

  def set_status(new_status, save = true)
    return false if new_status.to_s == self.status.to_s

    self.status            = new_status
    self.status_changed_at = Time.zone.now

    save ? self.save : self
  end

  private
  def same_user_frienship
    unless self.user_id != self.friend_id
      errors.add(:user_id, 'user cant not be the same to build a friendship')
    end
  end

  def set_friendship_status
    self.status ||= STATUS[:pending]
  end
end
