class Activity < ActiveRecord::Base

  acts_as_paranoid

  include Accessable
  include Imageable

  belongs_to :goal
  belongs_to :user

  has_one :origin, as: :originable

  has_many :upvotes, as: :upvotable

  has_many :associated_notifications, as: :notificable, class_name: 'Notification'

  validates :goal_id, :user_id, :date, presence: true

  before_validation :set_user_id

  has_one :user, through: :goal

  validates :goal_id, :user_id, :total, presence: true
  validates :total, numericality: { greater_than: 0 }

  validate :validate_date_must_be_after_goal_creation
  validate :validate_date_must_be_today_or_past
  validate :validate_date_is_valid_datetime

  scope :with_user_likes, -> (user_id) {
    select('u.id as like_id, activities.*')
    .joins("LEFT JOIN upvotes u ON (u.upvotable_id = activities.id AND u.upvotable_type = \'Activity\') AND u.user_id = #{user_id}")
    .recent
  }

  scope :recent, -> {
    order(created_at: :desc)
  }

  skip_callback :commit, :after, :remove_image!

  def push_notification_metadata
    {
      goal_id: self.goal_id,
      father_resource_id: self.goal_id
    }
  end

  private
  def set_user_id
    self.user_id ||= self.goal.user_id
    nil
  end

  def validate_date_must_be_after_goal_creation
    if (self.date && self.goal) &&
        (self.date.beginning_of_day < self.goal.created_at.to_date.beginning_of_day)
      errors.add(:date, I18n.t('moovup.errors.activities.cant_be_before_goal_creation_date'))
    end
  end

  def validate_date_is_valid_datetime
    if ((DateTime.parse(self.date.to_s) rescue ArgumentError) == ArgumentError)
      errors.add(:date, I18n.t('moovup.errors.activities.invalid_date'))
    end
  end

  def validate_date_must_be_today_or_past
    if (self.date && self.goal) &&
        (self.date.beginning_of_day > Date.today.beginning_of_day)
      errors.add(:date, I18n.t('moovup.errors.activities.cant_be_in_future'))
    end
  end


end
