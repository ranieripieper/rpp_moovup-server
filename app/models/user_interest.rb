class UserInterest < ActiveRecord::Base

#  acts_as_paranoid

  include Accessable

  belongs_to :user
  belongs_to :category

  has_one :origin, as: :originable, dependent: :destroy

  validate :validate_category_is_root, if: -> { Moovup::Config.enabled?(:only_allow_root_categories_in_qa) }
  validate :validate_category_is_children, unless: -> { Moovup::Config.enabled?(:only_allow_root_categories_in_qa) }

  protected
  def validate_category_is_root
    if self.category && self.category.children?
      errors.add(:category, I18n.t('moovup.errors.user_interests.category_must_be_root'))
    end
  end

  def validate_category_is_children
    if self.category && !self.category.children?
      errors.add(:category, I18n.t('moovup.errors.user_interests.category_must_be_children'))
    end
  end
end
