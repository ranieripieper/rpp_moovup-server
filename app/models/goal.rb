class Goal < ActiveRecord::Base

  acts_as_paranoid

  include Accessable

  after_initialize :set_defaults

  has_many :activities, :dependent => :destroy

  has_one :current_activity, -> { order updated_at: :desc }, class_name: 'Activity', foreign_key: :goal_id

  belongs_to :user

  belongs_to :category

  has_one :origin, as: :originable

  has_many :upvotes, as: :upvotable

  has_many :associated_notifications, as: :notificable, class_name: 'Notification'

  PERIODICITY_TYPE = {
    :day   => 'day',
    :week  => 'week',
    :month => 'month'
  }

  validates :category_id, :user_id, presence: true

  validates :total, :periodicity_type, presence: true, if: :open_goal?

  validates :periodicity_type, inclusion: PERIODICITY_TYPE.values, on: :create, unless: :closed_goal?

  validates :current, :target, numericality: { greater_than: 0 }, presence: true , if: :closed_goal?

  validate :validate_target_date_is_valid_datetime

  validate :validate_target_date_must_be_in_future

  validate :validate_category_must_be_children

  before_save :clear_attributes_by_goal_type

  ACTIVITIES_REPORTS_QUERY = (<<-QUERY)
    SELECT
      COUNT(*)  as total_count,
      SUM(total) as total_by_interval,
      date_trunc(?  , date) as date_interval
    FROM "activities"
    WHERE "activities"."goal_id" = ?
    GROUP BY date_interval
    ORDER BY date_interval ASC;
  QUERY

  def self.activities_reports(periodicity_type, goal_id)
    query = ActiveRecord::Base.send(:sanitize_sql, [ACTIVITIES_REPORTS_QUERY, periodicity_type, goal_id], '')
    ActiveRecord::Base.connection.execute(query).to_a
  end

  def migrate_to_public
    self.update_attribute(:public, true)
  end

  def migrate_to_private
    self.update_attribute(:public, false)
  end

  def set_defaults
    self.public = false if self.public.nil?
  end

  def closed_goal?
    self.category.try(:closed_goal?)
  end

  def open_goal?
    !closed_goal?
  end

  protected
  def clear_attributes_by_goal_type
    if closed_goal?
      self.periodicity_type = PERIODICITY_TYPE[:day]
      self.total = 0
    else
      self.current = self.target = self.target_date = nil
    end

    nil
  end

  def validate_target_date_must_be_in_future
    return nil if open_goal?

    if self.target_date && self.target_date < Time.zone.now
      errors.add(:target_date, 'must be in future')
    end
  end

  def validate_target_date_is_valid_datetime
    return true unless closed_goal?

    if ((DateTime.parse(self.target_date.to_s) rescue ArgumentError) == ArgumentError)
      errors.add(:target_date, 'must be a valid date')
    end
  end

  def validate_category_must_be_children
    if self.category_id && self.category.root?
      self.errors.add(:category_id, 'must be a children category')
    end
  end

end
