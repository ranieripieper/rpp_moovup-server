# encoding: UTF-8

class Category < ActiveRecord::Base

  acts_as_paranoid

  include Positionable

  MEASUREMENT_TYPES = {
    :kilometers => 1,
    :minutes    => 2,
    :unit       => 3,
    :weight     => 4,
    :time       => 5
  }

  has_many :questions

  enum measurement_type: MEASUREMENT_TYPES

  validates :title, presence: true

  # validates measurement type only for children
  validates :measurement_type, presence: true, if: -> {
    self.parent_id.present?
  }

  scope :hobby, -> {
    where(hobby: true)
  }

  scope :locked, -> {
    where(locked: true)
  }

  scope :valid_for_goals, -> {
    children.where('locked IS FALSE OR locked IS NULL')
  }

  scope :valid_for_questions, -> {
    scope = Moovup::Config.enabled?(:only_allow_root_categories_in_qa) ? roots : children

    scope.where('locked IS TRUE OR locked IS NULL')
  }

  scope :valid_for_interests, -> {
    valid_for_questions
  }

  scope :children, -> {
    where.not(parent_id: nil)
  }

  acts_as_tree order: :id
  acts_as_list scope: :parent

  def closed?
    self.closed_goal?
  end

  def valid_for_goals?
    children? && [false, nil].member?(self.locked) # self.locked.blank?
  end

  def valid_for_questions?
    return false if children? && Moovup::Config.enabled?(:only_allow_root_categories_in_qa)

    children? && [true, nil].member?(self.locked)
  end

  def children?
    parent_id.present?
  end

  alias :is_children? :children?

  def valid_for_interests?
    valid_for_questions?
  end

  def valid_for
    {
      goals: valid_for_goals?,
      questions: valid_for_questions?,
      interests: valid_for_interests?
    }
  end

  def only_valid_for
    valid_for.keep_if {|_, v| v.present? }.keys
  end

  def not_valid_for
    valid_for.keep_if {|_, v| v.blank? }.keys
  end

end
