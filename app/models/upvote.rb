class Upvote < ActiveRecord::Base
  #  acts_as_paranoid

  include Accessable

  belongs_to :upvotable, polymorphic: true, counter_cache: true

  belongs_to :user

  has_one :origin, as: :originable, dependent: :destroy

  validates :user, :upvotable, presence: true
  validates :user_id, uniqueness: { scope: [:upvotable_id, :upvotable_type] }
end
