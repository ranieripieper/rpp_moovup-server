class PushNotificationHistory < ActiveRecord::Base

  belongs_to :notificable, polymorphic: true

  belongs_to :receiver_user, foreign_key: :receiver_user_id, class_name: 'User'
  belongs_to :sender_user,   foreign_key: :sender_user_id  , class_name: 'User'

  validates :receiver_user_id, :notification_type, presence: true
  validates :notification_type, inclusion: Notification::TYPES.map(&:to_s)
end
