# encoding: UTF-8

class Authorization < ActiveRecord::Base

  PROVIDERS = {
    site:    "site",
    postman: "postman",
    faker:   "faker",
    android: "android",
    ios:     "ios"
  }

  EXPIRATION_TIME_LEFT_TO_UPDATE = 5.days

  validates :token, presence: true
  validates :provider, presence: true, inclusion: PROVIDERS.values

  validate :owner_user_must_be_activated_account

  belongs_to :user

  before_validation :normalize_provider, :generate_token

  before_create :set_expiration_date

  def expired?
    Time.zone.now >= expires_at
  end

  def valid_access?
    return false if expired?
    return true
  end

  def update_token_expires_at(force=false)
    if eligible_for_expiration_update?(force)
      self.update_attribute(:expires_at, expiration_date_from_now)
    end
  end

  def eligible_for_expiration_update?(force=false)
    force || ((Time.zone.now + EXPIRATION_TIME_LEFT_TO_UPDATE) >= expires_at)
  end

  private

  def set_expiration_date
    self.expires_at = expiration_date_from_now
    nil
  end

  def expiration_date_from_now
    Time.zone.now + Moovup::Config.session_duration.to_i.seconds
  end

  def generate_token
    return self if self.token.present?

    begin
      self.token = SecureRandom.hex
    end while self.class.exists?(token: token)
  end

  def normalize_provider
    self.provider = self.provider.to_s.downcase
  end

  def owner_user_must_be_activated_account
    if self.user && self.user.account_deactivated?
      self.errors.add(:user, I18n.t('moovup.errors.user_account_not_activated'))
    end
  end
end
