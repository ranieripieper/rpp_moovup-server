class Answer < ActiveRecord::Base
  acts_as_paranoid

  acts_as_tree order: :id, counter_cache: :replies_count

  include Accessable
  include Imageable

  belongs_to :question, counter_cache: true

  belongs_to :user

  has_one :origin, as: :originable

  has_many :upvotes, as: :upvotable

  has_many :reports, as: :reportable

  validates :body_text, presence: true
  validates :question_id, presence: true, if: -> { self.parent_id.blank? }
  validates :body_text, uniqueness: { scope: [:user_id, :parent_id] }

  scope :popular, -> {
    order(upvotes_count: :desc)
  }

  scope :recent, -> {
    order(created_at: :desc)
  }

  scope :recent_popular, -> {
    recent.popular
  }

  scope :most_commented, -> {
    order(replies_count: :desc)
  }

  scope :popular_recent, -> {
    popular.most_commented.recent
  }

  scope :with_image, -> {
    where.not(image: nil)
  }

  scope :elegible_for_question_cover_image, -> {
    not_reported.with_image.popular_recent
  }

  scope :not_reported, -> { where(reported_at: nil) }
  scope :reported, -> { where.not(reported_at: nil) }

  skip_callback :commit, :after, :remove_image!

  def has_image?
    self.image.present?
  end

  # alias
  def comments_count
    replies_count
  end

  def push_notification_metadata
    is_comment = self.parent_id.present?

    metadata = { is_comment: is_comment }

    # if is a answer comment
    if is_comment
      metadata.merge!({
        answer_id: self.parent_id,
        question_id: self.parent.question_id,
        father_resource_id: self.parent_id
      })
    # answer for question
    else
      metadata.merge!({
        question_id: self.question_id,
        father_resource_id: self.question_id
      })
    end

    metadata
  end
end
