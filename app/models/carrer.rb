class Carrer < ActiveRecord::Base

  acts_as_paranoid

  include Accessable

  validates_presence_of :title
  validates_uniqueness_of :title

  has_many :users

  def user_can_manage?(user)
    user.admin?
  end

end
