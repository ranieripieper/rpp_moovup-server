class IgnoredQuestion < ActiveRecord::Base
  belongs_to :user
  belongs_to :question

  has_one :origin, as: :originable, dependent: :destroy

  REASONS = {
    dont_want_to_reply: 1,
    replied: 2
  }

  enum reason: REASONS

  validates :user_id, :question_id, presence: true

  validates :question_id, uniqueness: { scope: [:user_id] }
end
