# encoding: UTF-8
class State < ActiveRecord::Base

  has_many :cities

  def uf
    self.acronym
  end
end
