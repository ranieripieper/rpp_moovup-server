class Report < ActiveRecord::Base
  acts_as_paranoid

  REPORT_TYPES = {
    1 => 'inappropriate_content',
    2 => 'spam',
    3 => 'inappropriate_comment'
  }

  belongs_to :user

  belongs_to :reported_user, class_name: 'User'

  belongs_to :reportable, polymorphic: true

  has_one :origin, as: :originable

  validates :user_id, :reportable_id, :reportable_type, :report_type, presence: true

  validates :report_type, inclusion: REPORT_TYPES.keys

  validates :report_type, uniqueness: { scope: [:user_id, :reportable_id, :reportable_type] }

  before_save :set_user_id

  def report_type_text
    REPORT_TYPES[report_type.to_i]
  end

  private
  def set_user_id
    self.reported_user_id ||= self.reportable.try(:user_id)
    nil
  end

end
