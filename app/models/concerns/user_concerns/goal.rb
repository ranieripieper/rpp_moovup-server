module UserConcerns
  module Goal

    extend ActiveSupport::Concern

    included do

      has_many :goals
      has_many :public_goals, -> { where(public: true) }, class_name: 'Goal'

      has_many :activities, through: :goals
      has_many :public_activities, through: :public_goals, source: :activities

      has_one :current_goal, -> { order(created_at: :desc) }, class_name: 'Goal', foreign_key: :user_id
      has_one :current_public_goal, -> {
        where(public: true)
        .joins('RIGHT JOIN activities ON activities.goal_id = goals.id')
        .order('activities.created_at DESC NULLS LAST, goals.created_at DESC')
      }, class_name: 'Goal', foreign_key: :user_id

      has_one :current_activity, through: :current_goal
      has_one :current_public_activity, through: :current_public_goal, source: :current_activity

      SELECT_ACTIVITY_STATEMENT = <<-QUERY
        (SELECT a.created_at FROM activities a
                  INNER JOIN goals g ON a.goal_id = g.id AND g.deleted_at IS NULL and g.public = 't'
                  WHERE a.user_id = users.id AND a.deleted_at IS NULL
               ORDER BY created_at DESC LIMIT 1
        ) as last_activity_created_at,
        (SELECT g.created_at FROM goals g
                  WHERE g.user_id = users.id AND g.deleted_at IS NULL AND g.public = 't'
               ORDER BY created_at DESC LIMIT 1) as last_goal_created_at
      QUERY

      SELECT_ACTIVITY_USER_LIKES_JOIN_STATEMENT = <<-QUERY
        LEFT JOIN upvotes ON (upvotes.upvotable_id IN (
            (
              SELECT a.id FROM activities a
                WHERE a.user_id = users.id AND a.deleted_at IS NULL
              ORDER BY created_at DESC LIMIT 1
            )
          )) AND upvotes.upvotable_type = 'Activity' AND upvotes.user_id = %s
      QUERY

      scope :order_by_last_activity, -> {
       select(SELECT_ACTIVITY_STATEMENT)
       .select('users.*')
       .order('last_activity_created_at DESC NULLS LAST')
       .order('last_goal_created_at DESC NULLS LAST')
      }

      scope :order_by_last_activity_for_user, -> (user_id) {
        join_statement = SELECT_ACTIVITY_USER_LIKES_JOIN_STATEMENT % user_id
        select('upvotes.id as user_activity_like_id')
        .order_by_last_activity
        .joins(join_statement)
      }

    end

  end
end
