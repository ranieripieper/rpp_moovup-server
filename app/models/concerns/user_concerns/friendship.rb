module UserConcerns
  module Friendship

    extend ActiveSupport::Concern

    included do
      has_many :friendships
      has_many :requested_friendships, class_name: 'Friendship', foreign_key: :friend_id

      ::Friendship::STATUS.each do |status, _|

        has_many :"#{status}_friendships", -> { where(status: ::Friendship::STATUS[status]) },
                 class_name: 'Friendship'

        has_many :"#{status}_requested_friendships", -> { where(status: ::Friendship::STATUS[status]) },
                 class_name: 'Friendship', foreign_key: :friend_id

        has_many :"#{status}_friends", through: :"#{status}_friendships", source: :friend

        has_many :"#{status}_requested_friends", through: :"#{status}_requested_friendships", source: :user
      end

      alias :friends :accepted_friends

      def be_friend_with(another_user)
        return true if self.has_pending_request?(another_user)
        return true if self.is_friend_of?(another_user)

        self.friendships.create(friend_id: another_user.id)
      end

      def is_friend_of?(another_user_or_id)
        _user_id = another_user_or_id.is_a?(ActiveRecord::Base) ? another_user_or_id.id : another_user_or_id
        self.friends.where(id: _user_id).exists?
      end

      def accept_friendship_of(another_user)
        friendship = self.pending_friendships.find_by(user_id: another_user.id)

        return false if friendship.blank?

        friendship.migrate_to_accepted
        friendship.exchange_friend_register(self)
      end

      def has_pending_request?(another_user)
        self.friendships.pending.where(friend_id: another_user.id).exists?
      end


      alias :friend_of? :is_friend_of?
      alias :accept_friendship :accept_friendship_of

    end

  end
end
