module UserConcerns
  # Q&A
  module QA

    extend ActiveSupport::Concern

    included do

      # created questions
      has_many :questions #, dependent: :destroy

      # created upvotes
      has_many :upvotes

      has_many :reports
      has_many :received_reports, class_name: 'Report', foreign_key: :reported_user_id

      has_many :questions_upvotes, -> { where(upvotable_type: 'Question') }, class_name: 'Upvote'
      has_many :answers_upvotes, -> { where(upvotable_type: 'Answer') }, class_name: 'Upvote'
      has_many :goals_upvotes, -> { where(upvotable_type: 'Goal') }, class_name: 'Upvote'
      has_many :activities_upvotes, -> { where(upvotable_type: 'Activity') }, class_name: 'Upvote'

      has_many :questions_upvoted, through: :upvotes, source: :upvotable, source_type: 'Question'
      has_many :answers_upvoted, through: :upvotes, source: :upvotable, source_type: 'Answer'
      has_many :goals_upvoted, through: :upvotes, source: :upvotable, source_type: 'Goal'
      has_many :activities_upvoted, through: :upvotes, source: :upvotable, source_type: 'Activity'

      has_many :answers

      has_many :favorited_questions
      has_many :favorite_questions, through: :favorited_questions, source: :question

      has_many :interests, class_name: 'UserInterest'
      has_many :categories_interested, through: :interests, source: :category

      has_many :ignored_questions
      has_many :questions_ignored, through: :ignored_questions, source: :question

      def favorited?(question)
        favorited_questions.exists?(question_id: question.id)
      end

      def upvoted?(resource)
        self.upvotes.exists?(upvotable: resource)
      end

      alias :liked? :upvoted?

      def questions_feed_for_user
        Question.not_reported.questions_feed_for_user(self.id, self.is_company?)
      end

      def questions_to_reply_feed(max_default = nil)
        Question.not_reported.questions_to_reply_feed_for_user(self.id, max_default)
      end

    end
  end
end
