module UserConcerns
  module Basic

    extend ActiveSupport::Concern

    VALID_PROFILES_TYPES = {
      :staff       => "staff",
      :common_user => "common_user",
      :company     => "company"
    }

    VALID_GENDERS = {
      :male   => "male",
      :female => "female"
    }

    EMAIL_REGEXP = /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i

    MINIMUM_AGE = 18

    included do
      has_one :origin, as: :originable

      belongs_to :carrer

      belongs_to :hobby, class_name: 'Category', foreign_key: :hobby_id

      alias :main_hobby :hobby

      belongs_to :commercial_activity

      VALID_PROFILES_TYPES.each do |key, value|
        scope key, -> { where(profile_type: value) }

        define_method("is_#{key}?") do
          self.profile_type.to_s == value.to_s
        end

        alias :"#{key}?" :"is_#{key}?"
      end

      validates :email, presence: true
      validates :email, uniqueness: true, on: [:create]
      validates :email, format: { with: EMAIL_REGEXP }, if: -> {
        self.email.present?
      }

      validates :username, uniqueness: true, on: :create

      validates :first_name, presence: true
      validates :first_name, uniqueness: { scope: [:last_name] }, if: -> {
        !self.from_oauth? && Moovup::Config.enabled?(:uniqueness_user_name_and_last_name_validation)
      }

      validates :last_name, presence: true, if: -> {
        self.commercial_activity_id.blank?
      }

      validates :gender, presence: true, inclusion: VALID_GENDERS.values

      validates :profile_type, presence: true, inclusion: VALID_PROFILES_TYPES.values

      validate :validates_birthday_date

      after_initialize  :set_defaults

      before_validation  :set_defaults

      before_save :clear_attributes_for_user

      def company_name=(company_name)
        names = company_name.split(/\s/)
        self.first_name = names.shift
        self.last_name  = names.join(' ')
      end

      def foundation_date
        birthday_date
      end

      def foundation_date=(date)
        self.birthday_date = date
      end

      def admin?
        self.try(:is_admin?) || self.profile_type == 'admin'
      end

      private
      def set_defaults
        self.gender       ||= VALID_GENDERS[:male] if self.is_company?
        self.profile_type ||= VALID_PROFILES_TYPES[:user]

        nil
      end

      def clear_attributes_for_user
        if self.is_company?
          self.carrer_id = self.hobby_id = nil
        elsif self.is_common_user?
          self.commercial_activity_id = nil
        end

        nil
      end

      def validates_birthday_date
        return nil if self.is_company? || self.from_oauth?

        if self.birthday_date
          if (self.birthday_date > (Date.today - MINIMUM_AGE.years))
            self.errors.add(:birthday_date, "must be greater than (today - #{MINIMUM_AGE} years)")
          end
        end

        return nil
      end

    end

  end
end
