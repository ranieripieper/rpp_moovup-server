module UserConcerns
  module Search
    extend ActiveSupport::Concern

    included do
      include PgSearch

      pg_search_scope :search_by_full_name, against: [:first_name, :last_name, :username],
                                            using: {
                                              tsearch: {
                                                prefix: true,
                                                any_word: true
                                              }
                                            }

      scope :search_with_friendship_status, -> (term, user_id) {
        subquery = self.where.not(id: user_id).search_by_full_name(term).to_sql

        select('f.status as mine_friendship_status, f2.status as user_friendship_status, users.*')
          .joins("LEFT JOIN friendships f ON (f.user_id = users.id) AND f.friend_id = #{user_id}")
          .joins("LEFT JOIN friendships f2 ON (f2.friend_id = users.id) AND f2.user_id = #{user_id}")
          .from("(#{subquery}) as users")
      }
    end
  end
end
