module QuestionConcerns
  module SQL
    extend ActiveSupport::Concern

    included do
      QUESTIONS_TO_REPLY_SUBQUERY  = <<-QUERY
        -- Obtain row_number to limit result set
        SELECT ROW_NUMBER() OVER (
          PARTITION BY questions.category_id
            ORDER BY questions.category_id,
                     questions.answers_count DESC,
                     questions.upvotes_count DESC,
                     questions.created_at DESC
        ) AS row_number,

        -- Obtain max number of answers from category(if is NULL will be 10)
        COALESCE(user_interests.max_answers_count, :max_default) as max_answers_count,

        -- Query for questions matching criteria
        questions.* FROM questions
          -- Limit questions to questions which has user_interests
          INNER JOIN user_interests ON  user_interests.category_id = questions.category_id
        -- soft deleted
        WHERE questions.deleted_at IS NULL AND
        -- only questions not reported
              questions.reported_at IS NULL AND
        -- only not answered questions
              (questions.answers_count <= :max_question_answers_count OR questions.answers_count IS NULL) AND
        -- only interests of specific user
              (user_interests.user_id = :user_id) AND
        -- without the questions created by user
              (questions.user_id != :user_id) AND
        -- removing all ignored questions
              (questions.id NOT IN (SELECT DISTINCT(question_id) FROM ignored_questions WHERE user_id = :user_id))
        ORDER BY row_number DESC -- order using the criterias to obtain the row number
      QUERY

      QUESTIONS_TO_REPLY_FEED = <<-QUERY
        SELECT * FROM (%{subquery}) as question
        -- by default is 10 (see coalesce)
        WHERE question.max_answers_count IS NULL OR question.row_number <= question.max_answers_count
        -- same order rule from ROW_NUMBER()
        ORDER BY question.answers_count DESC,
                 question.upvotes_count DESC,
                 question.created_at DESC
      QUERY

      # same as above but return a Array (not a ActiveRecord::Collection)
      # dont use this if you need pagination, for real
      def self.questions_to_reply_feed_for_user_by_sql(user_id, max_default = nil)
        query_data = {
          max_default: max_default,
          user_id: user_id,
          max_question_answers_count: (Moovup::Config.max_answers_to_show_in_reply_feed || 0).to_i
        }

        formatted_query = QUESTIONS_TO_REPLY_FEED % { subquery: QUESTIONS_TO_REPLY_SUBQUERY }
        query = ActiveRecord::Base.send(:sanitize_sql, [formatted_query, query_data], '')

        ActiveRecord::Base.connection.execute(query).to_a
      end

      scope :questions_to_reply_feed_for_user, -> (user_id, max_default = nil) {
        query_data = {
          max_default: max_default,
          user_id: user_id,
          max_question_answers_count: (Moovup::Config.max_answers_to_show_in_reply_feed || 0).to_i
        }

        subquery = ActiveRecord::Base.send(:sanitize_sql, [
          QUESTIONS_TO_REPLY_SUBQUERY,
          query_data
        ], '')

        select('*')
        .from("(#{subquery}) as questions")
        .where('questions.max_answers_count IS NULL OR questions.row_number <= questions.max_answers_count')
        .recent_popular
      }

      # see lib/tasks/benchmark.rake for another querys with the same purpose
      scope :questions_feed_for_user, -> (user_id, is_company = false) {
        _scope = where('questions.user_id != ?', user_id) # only questions created by other users

        unless is_company
          _scope = _scope.where('questions.category_id IN (
            (SELECT hobby_id FROM users WHERE id = :user_id) UNION
            (SELECT DISTINCT(category_id) FROM goals WHERE goals.user_id = :user_id AND goals.deleted_at IS NULL) UNION
            (SELECT category_id FROM user_interests WHERE user_interests.user_id = :user_id) UNION
            (SELECT parent_id FROM categories WHERE categories.id = (SELECT hobby_id FROM users WHERE id = :user_id))
          )', user_id: user_id)
        end

        _scope.not_reported.recent
      }

      scope :popular, -> {
        order(upvotes_count: :desc)
      }

      scope :most_replied, -> {
        order(answers_count: :desc)
      }

      scope :with_image_answers_order, -> {
        joins(:answers).order("answers.image DESC")
      }

      scope :recent, -> {
        order(created_at: :desc)
      }

      scope :with_answers, -> {
        where('answers_count > 0')
      }

      scope :without_answers, -> {
        where('answers_count IS NULL OR answers_count = 0')
      }

      scope :recent_popular, -> {
        recent.popular
      }

    end

  end
end
