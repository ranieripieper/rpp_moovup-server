module Imageable

  extend ActiveSupport::Concern

  included do
    mount_uploader :image, PhotoUploader
    mount_base64_uploader :image, PhotoUploader, base_filename: :image

    process_in_background :image if Moovup::Config.enabled?(:process_photo_upload_in_background) ||
                                    Moovup::Config.enabled?(:process_upload_in_background)

  end

  def images_url
    image_versions = self.image.versions
    versions = image_versions.map(&:first)
    images   = image_versions.map(&:last).map(&:url)

    Hash[versions.zip(images)].presence || [self.image_url]
  end

  def has_uploaded_image?
    self.image_url.present? &&
    !self.image_url.match(/(fallback|default)\.(png|jpg|jpeg)$/)
  end

  def push_notification_image_url
    has_uploaded_image? ? self.image_url : nil
  end

  def social_share_image_url
    has_uploaded_image? ? self.image_url : Moovup::Config.default_social_share_image_url
  end
end
