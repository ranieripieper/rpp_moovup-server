# encoding: UTF-8

class User < ActiveRecord::Base

  acts_as_paranoid

  # Profile management
  include UserConcerns::Basic

  # Authentication management
  include UserConcerns::Auth

  # Friendship & Friends management
  include UserConcerns::Friendship

  # Goals/Activities
  include UserConcerns::Goal

  # Questions & Answers
  include UserConcerns::QA

  # Notifications e Push Notifications
  include UserConcerns::Notification

  # User Settings/Preferences
  include UserConcerns::Preference

  # Search
  include UserConcerns::Search

  # Username validation
  include Usernamed

  # Profile Image
  include ProfileImageable


  alias :name :fullname
  alias :company_name :fullname

  def user_can_delete?(user)
    self.id == user.id
  end

end
