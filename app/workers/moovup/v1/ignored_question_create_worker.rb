module Moovup
  module V1
    class IgnoredQuestionCreateWorker

      include Sidekiq::Worker

      sidekiq_options :retry => 5, queue: :ignored_questions

      sidekiq_retry_in { |count| count * 60 }

      def perform(user_id, ignored_data = {})
        user = User.find_by(id: user_id)

        return nil unless user

        begin
          user.ignored_questions.create(ignored_data)
        rescue => e
          Rollbar.error(e)
        end
      end
    end
  end
end
