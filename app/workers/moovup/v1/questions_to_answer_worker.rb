module Moovup
  module V1
    class QuestionsToAnswerWorker
      include Sidekiq::Worker

      sidekiq_options :retry => 5, queue: :push_notifications

      sidekiq_retry_in { |count| count * 60 }

      def perform(options = {})
        service = Moovup::V1::PushNotifications::QuestionsToAnswerDeliveryService.new(options)
        service.execute
      end
    end
  end
end
