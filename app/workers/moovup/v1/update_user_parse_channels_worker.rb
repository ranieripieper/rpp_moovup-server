module Moovup
  module V1
    class UpdateUserParseChannelsWorker

      include Sidekiq::Worker

      sidekiq_options :retry => 5, queue: :parse

      sidekiq_retry_in { |count| count * 60 }

      def perform(user_id)
        user = User.with_deleted.find(user_id)

        if user
          service = ::Moovup::V1::Parse::UpdateUserChannelsService.new(user)
          service.execute
        end

      end

    end
  end
end
