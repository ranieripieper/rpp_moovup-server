module Moovup
  module V1
    class QuestionUpdateCoverImageWorker

      include Sidekiq::Worker

      sidekiq_options :retry => 5, queue: :questions

      sidekiq_retry_in { |count| count * 60 }

      def perform(question_id)
        question = Question.find_by(id: question_id)

        question.try(:update_cover_image)
      end
    end
  end
end
