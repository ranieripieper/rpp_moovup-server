# encoding: UTF-8

class PhotoUploader < BaseImageUploader

  include Piet::CarrierWaveExtension

  VERSIONS = {
    large: {
      size: [800, 800],
      process: true,
      process_method: :resize_to_fit
    },
    medium: {
      size: [500, 500],
      process: true,
      process_method: :resize_to_fit
    },
    small: {
      size: [300, 300],
      process: true,
      process_method: :resize_to_fit
    },
    thumb: {
      size: [180, 180],
      process_method: :resize_to_fill
    }
  }

  DEFAULT_JPG_QUALITY = 85
  DEFAULT_PNG_QUALITY = 5

  if Moovup::Config.enabled?(:compress_images_on_upload)
    process :optimize => [ { quality: DEFAULT_JPG_QUALITY, level: DEFAULT_PNG_QUALITY } ]
  end

  VERSIONS.each do |image_version, options|
    version image_version, if: :create_upload_version? do
      process_options = { (options[:process_method] || :resize_to_fill) => options[:size] }

      process process_options

      if options[:process].present? && Moovup::Config.enabled?(:compress_images_on_upload)
        process optimize: [ { quality: DEFAULT_JPG_QUALITY, level: DEFAULT_PNG_QUALITY } ]
      end
    end
  end

  # TODO: Move to config
  def create_upload_version?(version)
    return true
  end

  def filename
    unique_filename
  end

  def unique_filename
    "#{secure_token}.#{file.extension}" if original_filename.present?
  end

  def secure_token
    var = :"@#{mounted_as}_secure_token"
    model.instance_variable_get(var) or model.instance_variable_set(var, SecureRandom.uuid)
  end
end
