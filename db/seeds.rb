actions = ENV['ACTIONS'].present? ? ENV['ACTIONS'].split(',').map(&:squish) : nil
Moovup::Services::CreateDefaultDataService.new(actions: actions).execute