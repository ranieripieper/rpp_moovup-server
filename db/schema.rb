# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160722181400) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "hstore"
  enable_extension "unaccent"
  enable_extension "pg_trgm"
  enable_extension "fuzzystrmatch"

  create_table "activities", force: :cascade do |t|
    t.text     "body_text"
    t.integer  "upvotes_count"
    t.integer  "total"
    t.integer  "goal_id"
    t.string   "image"
    t.datetime "created_at",    null: false
    t.datetime "updated_at",    null: false
    t.integer  "user_id"
    t.date     "date"
    t.datetime "deleted_at"
  end

  add_index "activities", ["deleted_at"], name: "index_activities_on_deleted_at", using: :btree
  add_index "activities", ["goal_id"], name: "index_activities_on_goal_id", using: :btree
  add_index "activities", ["user_id"], name: "index_activities_on_user_id", using: :btree

  create_table "answers", force: :cascade do |t|
    t.string   "body_text"
    t.integer  "comment_id"
    t.integer  "question_id"
    t.integer  "upvotes_count", default: 0
    t.datetime "reported_at"
    t.integer  "user_id"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.datetime "deleted_at"
    t.integer  "parent_id"
    t.integer  "replies_count", default: 0
    t.string   "image"
  end

  add_index "answers", ["body_text", "parent_id", "user_id"], name: "index_answers_on_body_text_and_parent_id_and_user_id", unique: true, using: :btree
  add_index "answers", ["body_text"], name: "index_answers_on_body_text", using: :btree
  add_index "answers", ["deleted_at"], name: "index_answers_on_deleted_at", using: :btree
  add_index "answers", ["question_id"], name: "index_answers_on_question_id", using: :btree
  add_index "answers", ["replies_count"], name: "index_answers_on_replies_count", using: :btree
  add_index "answers", ["reported_at"], name: "index_answers_on_reported_at", using: :btree
  add_index "answers", ["upvotes_count"], name: "index_answers_on_upvotes_count", using: :btree

  create_table "authorizations", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "token"
    t.string   "provider"
    t.datetime "expires_at"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "authorizations", ["token"], name: "index_authorizations_on_token", using: :btree
  add_index "authorizations", ["user_id"], name: "index_authorizations_on_user_id", using: :btree

  create_table "carrers", force: :cascade do |t|
    t.string   "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.datetime "deleted_at"
  end

  add_index "carrers", ["deleted_at"], name: "index_carrers_on_deleted_at", using: :btree

  create_table "categories", force: :cascade do |t|
    t.string   "title"
    t.integer  "position"
    t.datetime "created_at",                       null: false
    t.datetime "updated_at",                       null: false
    t.integer  "parent_id"
    t.integer  "measurement_type"
    t.boolean  "hobby",            default: false
    t.boolean  "closed_goal",      default: false
    t.boolean  "locked"
    t.datetime "deleted_at"
  end

  add_index "categories", ["deleted_at"], name: "index_categories_on_deleted_at", using: :btree
  add_index "categories", ["hobby"], name: "index_categories_on_hobby", using: :btree
  add_index "categories", ["measurement_type"], name: "index_categories_on_measurement_type", using: :btree
  add_index "categories", ["parent_id"], name: "index_categories_on_parent_id", using: :btree
  add_index "categories", ["title"], name: "index_categories_on_title", using: :btree

  create_table "cities", force: :cascade do |t|
    t.integer  "state_id"
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "cities", ["state_id"], name: "index_cities_on_state_id", using: :btree

  create_table "commercial_activities", force: :cascade do |t|
    t.string   "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "favorited_questions", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "question_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.datetime "deleted_at"
  end

  add_index "favorited_questions", ["deleted_at"], name: "index_favorited_questions_on_deleted_at", using: :btree
  add_index "favorited_questions", ["question_id"], name: "index_favorited_questions_on_question_id", using: :btree
  add_index "favorited_questions", ["user_id", "question_id"], name: "index_favorited_questions_on_user_id_and_question_id", unique: true, using: :btree
  add_index "favorited_questions", ["user_id"], name: "index_favorited_questions_on_user_id", using: :btree

  create_table "friendships", force: :cascade do |t|
    t.integer  "friend_id"
    t.integer  "user_id"
    t.string   "status"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.datetime "deleted_at"
    t.datetime "status_changed_at"
  end

  add_index "friendships", ["deleted_at"], name: "index_friendships_on_deleted_at", using: :btree
  add_index "friendships", ["friend_id", "user_id"], name: "index_friendships_on_friend_id_and_user_id", unique: true, using: :btree
  add_index "friendships", ["friend_id"], name: "index_friendships_on_friend_id", using: :btree
  add_index "friendships", ["status"], name: "index_friendships_on_status", using: :btree
  add_index "friendships", ["user_id"], name: "index_friendships_on_user_id", using: :btree

  create_table "goals", force: :cascade do |t|
    t.boolean  "public"
    t.string   "periodicity_type"
    t.decimal  "target"
    t.decimal  "current"
    t.integer  "category_id"
    t.datetime "created_at",                   null: false
    t.datetime "updated_at",                   null: false
    t.date     "target_date"
    t.integer  "user_id"
    t.integer  "upvotes_count",    default: 0
    t.decimal  "total"
    t.datetime "deleted_at"
  end

  add_index "goals", ["category_id"], name: "index_goals_on_category_id", using: :btree
  add_index "goals", ["deleted_at"], name: "index_goals_on_deleted_at", using: :btree
  add_index "goals", ["periodicity_type"], name: "index_goals_on_periodicity_type", using: :btree
  add_index "goals", ["total"], name: "index_goals_on_total", using: :btree
  add_index "goals", ["upvotes_count"], name: "index_goals_on_upvotes_count", using: :btree
  add_index "goals", ["user_id"], name: "index_goals_on_user_id", using: :btree

  create_table "ignored_questions", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "question_id"
    t.integer  "reason",      default: 1
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  add_index "ignored_questions", ["question_id"], name: "index_ignored_questions_on_question_id", using: :btree
  add_index "ignored_questions", ["user_id", "question_id"], name: "index_ignored_questions_on_user_id_and_question_id", unique: true, using: :btree
  add_index "ignored_questions", ["user_id"], name: "index_ignored_questions_on_user_id", using: :btree

  create_table "notifications", force: :cascade do |t|
    t.integer  "notificable_id"
    t.string   "notificable_type"
    t.integer  "receiver_user_id"
    t.integer  "sender_user_id"
    t.string   "notification_type"
    t.boolean  "read"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.datetime "deleted_at"
  end

  add_index "notifications", ["deleted_at"], name: "index_notifications_on_deleted_at", using: :btree
  add_index "notifications", ["notificable_id"], name: "index_notifications_on_notificable_id", using: :btree
  add_index "notifications", ["notificable_type"], name: "index_notifications_on_notificable_type", using: :btree
  add_index "notifications", ["read"], name: "index_notifications_on_read", using: :btree
  add_index "notifications", ["receiver_user_id"], name: "index_notifications_on_receiver_user_id", using: :btree
  add_index "notifications", ["sender_user_id"], name: "index_notifications_on_sender_user_id", using: :btree

  create_table "origins", force: :cascade do |t|
    t.integer  "originable_id"
    t.string   "originable_type"
    t.string   "ip"
    t.string   "provider"
    t.string   "user_agent"
    t.string   "locale"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
  end

  add_index "origins", ["originable_id"], name: "index_origins_on_originable_id", using: :btree
  add_index "origins", ["originable_type"], name: "index_origins_on_originable_type", using: :btree

  create_table "push_notification_histories", force: :cascade do |t|
    t.integer  "receiver_user_id"
    t.integer  "sender_user_id"
    t.integer  "notificable_id"
    t.string   "notificable_type"
    t.string   "message"
    t.string   "notification_type"
    t.json     "metadata"
    t.datetime "delivery_at"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  add_index "push_notification_histories", ["message"], name: "index_push_notification_histories_on_message", using: :btree
  add_index "push_notification_histories", ["notificable_id"], name: "index_push_notification_histories_on_notificable_id", using: :btree
  add_index "push_notification_histories", ["notificable_type"], name: "index_push_notification_histories_on_notificable_type", using: :btree
  add_index "push_notification_histories", ["notification_type"], name: "index_push_notification_histories_on_notification_type", using: :btree
  add_index "push_notification_histories", ["receiver_user_id"], name: "index_push_notification_histories_on_receiver_user_id", using: :btree
  add_index "push_notification_histories", ["sender_user_id"], name: "index_push_notification_histories_on_sender_user_id", using: :btree

  create_table "questions", force: :cascade do |t|
    t.string   "body_text"
    t.integer  "user_id"
    t.integer  "upvotes_count",   default: 0
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.datetime "reported_at"
    t.integer  "favorited_count"
    t.string   "status"
    t.integer  "category_id"
    t.datetime "deleted_at"
    t.integer  "parent_id"
    t.integer  "answers_count",   default: 0
    t.string   "cover_image_url"
  end

  add_index "questions", ["answers_count"], name: "index_questions_on_answers_count", using: :btree
  add_index "questions", ["body_text"], name: "index_questions_on_body_text", using: :btree
  add_index "questions", ["deleted_at"], name: "index_questions_on_deleted_at", using: :btree
  add_index "questions", ["parent_id"], name: "index_questions_on_parent_id", using: :btree
  add_index "questions", ["reported_at"], name: "index_questions_on_reported_at", using: :btree
  add_index "questions", ["upvotes_count"], name: "index_questions_on_upvotes_count", using: :btree

  create_table "reports", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "reportable_id"
    t.string   "reportable_type"
    t.integer  "report_type"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.integer  "reported_user_id"
    t.datetime "deleted_at"
  end

  add_index "reports", ["deleted_at"], name: "index_reports_on_deleted_at", using: :btree
  add_index "reports", ["reportable_id"], name: "index_reports_on_reportable_id", using: :btree
  add_index "reports", ["reportable_type"], name: "index_reports_on_reportable_type", using: :btree
  add_index "reports", ["reported_user_id"], name: "index_reports_on_reported_user_id", using: :btree
  add_index "reports", ["user_id"], name: "index_reports_on_user_id", using: :btree

  create_table "states", force: :cascade do |t|
    t.string   "name"
    t.string   "acronym"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "states", ["acronym"], name: "index_states_on_acronym", using: :btree

  create_table "upvotes", force: :cascade do |t|
    t.integer  "upvotable_id"
    t.string   "upvotable_type"
    t.integer  "user_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
    t.datetime "deleted_at"
  end

  add_index "upvotes", ["deleted_at"], name: "index_upvotes_on_deleted_at", using: :btree
  add_index "upvotes", ["upvotable_type", "upvotable_id", "user_id"], name: "index_upvotes_on_upvotable_type_and_upvotable_id_and_user_id", unique: true, using: :btree
  add_index "upvotes", ["upvotable_type", "upvotable_id"], name: "index_upvotes_on_upvotable_type_and_upvotable_id", using: :btree
  add_index "upvotes", ["user_id"], name: "index_upvotes_on_user_id", using: :btree

  create_table "user_devices", force: :cascade do |t|
    t.integer  "user_id"
    t.string   "identifier"
    t.string   "token"
    t.string   "platform"
    t.datetime "deleted_at"
    t.string   "parse_object_id"
    t.string   "parse_installation_id"
    t.datetime "created_at",            null: false
    t.datetime "updated_at",            null: false
  end

  add_index "user_devices", ["deleted_at"], name: "index_user_devices_on_deleted_at", using: :btree
  add_index "user_devices", ["parse_object_id", "token"], name: "index_user_devices_on_parse_object_id_and_token", unique: true, using: :btree
  add_index "user_devices", ["parse_object_id"], name: "index_user_devices_on_parse_object_id", unique: true, using: :btree
  add_index "user_devices", ["token"], name: "index_user_devices_on_token", using: :btree
  add_index "user_devices", ["user_id"], name: "index_user_devices_on_user_id", using: :btree

  create_table "user_interests", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "category_id"
    t.integer  "max_answers_count"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
    t.datetime "deleted_at"
  end

  add_index "user_interests", ["category_id"], name: "index_user_interests_on_category_id", using: :btree
  add_index "user_interests", ["deleted_at"], name: "index_user_interests_on_deleted_at", using: :btree
  add_index "user_interests", ["user_id", "category_id"], name: "index_user_interests_on_user_id_and_category_id", unique: true, using: :btree
  add_index "user_interests", ["user_id"], name: "index_user_interests_on_user_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.string   "username"
    t.string   "profile_type"
    t.string   "gender"
    t.string   "password_digest"
    t.datetime "reset_password_sent_at"
    t.datetime "password_reseted_at"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.string   "reset_password_token"
    t.string   "profile_image"
    t.date     "birthday_date"
    t.integer  "carrer_id"
    t.integer  "commercial_activity_id"
    t.string   "oauth_provider"
    t.string   "oauth_provider_uid"
    t.integer  "hobby_id"
    t.integer  "login_attempts",         default: 0
    t.datetime "blocked_until"
    t.hstore   "preferences"
    t.datetime "deleted_at"
    t.string   "activation_token"
    t.datetime "activated_at"
    t.datetime "activation_sent_at"
    t.integer  "received_upvotes_count", default: 0
    t.json     "login_status"
  end

  add_index "users", ["deleted_at"], name: "index_users_on_deleted_at", using: :btree
  add_index "users", ["email"], name: "index_users_on_email", using: :btree
  add_index "users", ["hobby_id"], name: "index_users_on_hobby_id", using: :btree
  add_index "users", ["profile_type"], name: "index_users_on_profile_type", using: :btree
  add_index "users", ["username"], name: "index_users_on_username", using: :btree

  add_foreign_key "authorizations", "users", on_delete: :cascade
  add_foreign_key "cities", "states"
  add_foreign_key "goals", "categories"
  add_foreign_key "ignored_questions", "questions"
  add_foreign_key "ignored_questions", "users"
  add_foreign_key "reports", "users"
  add_foreign_key "upvotes", "users"
  add_foreign_key "user_devices", "users"
  add_foreign_key "user_interests", "categories"
  add_foreign_key "user_interests", "users"
end
