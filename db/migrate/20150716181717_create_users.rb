class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :first_name
      t.string :last_name
      t.string :email
      t.string :username
      t.string :profile_type
      t.string :gender
      t.string :password_digest
      t.datetime :reset_password_sent_at
      t.datetime :password_reseted_at

      t.timestamps null: false
    end
    add_index :users, :email
    add_index :users, :username
    add_index :users, :profile_type
  end
end
