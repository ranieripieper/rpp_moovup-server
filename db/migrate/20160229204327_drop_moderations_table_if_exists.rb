class DropModerationsTableIfExists < ActiveRecord::Migration
  def up
    if table_exists?(:moderations)
      drop_table :moderations
    end
  end
end
