class CreateUnaccentExtension < ActiveRecord::Migration
  def up
    begin
      enable_extension "unaccent" unless extension_enabled?('unaccent')
    rescue Exception => e
      puts "[Migration Error] #{e.message}"
    end
  end
end
