class AddUpvotesCountToActivities < ActiveRecord::Migration
  def up
    unless column_exists?(:activities, :upvotes_count)
      add_column :activities, :upvotes_count, :integer
    end
  end

  def down
    remove_column :activities, :upvotes_count
  end
end
