class RemoveFundationDateFromUsers < ActiveRecord::Migration
  def change
    remove_column :users, :fundation_date, :date
  end
end
