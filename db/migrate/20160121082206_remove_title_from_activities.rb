class RemoveTitleFromActivities < ActiveRecord::Migration
  def change
    remove_column :activities, :title, index: true
  end
end
