class AddMeasurementTypeToCategories < ActiveRecord::Migration
  def change
    execute <<-SQL
      CREATE TYPE measurement_type AS ENUM ('km', 'minutes', 'unit', 'weight', 'time');
    SQL

    add_column :categories, :measurement_type, :integer
    add_index :categories, :measurement_type
  end
end
