class AddIndexesToUpvotableResources < ActiveRecord::Migration
  def change
    add_index :questions, :upvotes_count
    add_index :answers, :upvotes_count
  end
end
