class ChangeReceivedUpvotesCountDefaultValue < ActiveRecord::Migration
  def up
    change_column :users, :received_upvotes_count, :integer, :default => 0
  end

  def down
    change_column :users, :received_upvotes_count, :integer, :default => nil
  end
end
