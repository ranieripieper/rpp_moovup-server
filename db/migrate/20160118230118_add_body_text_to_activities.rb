class AddBodyTextToActivities < ActiveRecord::Migration
  def change
   unless column_exists?(:activities, :body_text)
      add_column :activities, :body_text, :text
   end
  end
end
