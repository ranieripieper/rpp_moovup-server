class ChangeGoalColumns < ActiveRecord::Migration
  def change
    rename_column :goals, :goal_total, :total
    rename_column :goals, :goal_target, :target
    rename_column :goals, :goal_current, :current
  end
end
