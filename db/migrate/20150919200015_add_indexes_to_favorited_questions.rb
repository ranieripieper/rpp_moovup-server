class AddIndexesToFavoritedQuestions < ActiveRecord::Migration
  def change
    add_index :favorited_questions, :question_id
    add_index :favorited_questions, :user_id
    add_index :favorited_questions, [:user_id, :question_id], unique: true
  end
end
