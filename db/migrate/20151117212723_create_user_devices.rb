class CreateUserDevices < ActiveRecord::Migration
  def change
    create_table :user_devices do |t|
      t.references :user, index: true, foreign_key: true
      t.string :identifier
      t.string :token
      t.string :platform
      t.datetime :deleted_at
      t.string :parse_object_id
      t.string :parse_installation_id

      t.timestamps null: false
    end
    add_index :user_devices, :token
    add_index :user_devices, :deleted_at
  end
end
