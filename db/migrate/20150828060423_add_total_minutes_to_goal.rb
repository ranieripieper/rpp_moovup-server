class AddTotalMinutesToGoal < ActiveRecord::Migration
  def change
    add_column :goals, :total_minutes, :integer
  end
end
