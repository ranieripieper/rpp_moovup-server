class AddPreferencesToUser < ActiveRecord::Migration
  def change
   ActiveRecord::Schema.define do
     enable_extension 'hstore' unless extension_enabled?('hstore')

     change_table :users do |t|
       t.hstore 'preferences'
     end
   end
  end
end
