class RemoveHobbiesTable < ActiveRecord::Migration
  def up
    drop_table :hobbies
    drop_table :user_hobbies
  end

  def down
    create_table :hobbies do |t|
      t.string :title, index: true

      t.timestamps null: false
    end

    create_table :user_hobbies do |t|
      t.integer :user_id, index: true
      t.integer :hobby_id

      t.timestamps null: false
    end
  end
end
