class AddTotalToGoals < ActiveRecord::Migration
  def change
    add_column :goals, :total, :decimal
    add_index :goals, :total
  end
end
