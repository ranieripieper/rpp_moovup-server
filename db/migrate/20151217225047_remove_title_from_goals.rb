class RemoveTitleFromGoals < ActiveRecord::Migration
  def change
    remove_column :goals, :title, :string
  end
end
