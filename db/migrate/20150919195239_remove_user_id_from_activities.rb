class RemoveUserIdFromActivities < ActiveRecord::Migration
  def change
    if column_exists?(:activities, :user_id)
      remove_column :activities, :user_id, :integer, index: true
    end
  end
end
