class CreateUpvotes < ActiveRecord::Migration
  def change
    create_table :upvotes do |t|
      t.references :upvotable, polymorphic: true, index: true
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end

    add_index :upvotes, [:upvotable_type, :upvotable_id, :user_id], unique: true
  end
end
