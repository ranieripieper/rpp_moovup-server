class AddIndexesToFriendships < ActiveRecord::Migration
  def up
    add_index :friendships, :user_id unless index_exists?(:friendships, :user_id)
    add_index :friendships, :friend_id unless index_exists?(:friendships, :friend_id)
    add_index :friendships, :status unless index_exists?(:friendships, :status)
    unless index_exists? :friendships, [:friend_id, :user_id], unique: true
      add_index :friendships, [:friend_id, :user_id], unique: true
    end
  end

  def down
    remove_index :friendships, :user_id if index_exists?(:friendships, :user_id)
    remove_index :friendships, :friend_id if index_exists?(:friendships, :friend_id)
    remove_index :friendships, :status if index_exists?(:friendships, :status)
    if index_exists? :friendships, [:friend_id, :user_id], unique: true
      remove_index :friendships, [:friend_id, :user_id]
    end
  end
end
