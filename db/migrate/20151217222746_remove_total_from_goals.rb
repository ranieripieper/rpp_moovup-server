class RemoveTotalFromGoals < ActiveRecord::Migration
  def change
    remove_column :goals, :total, :integer
  end
end
