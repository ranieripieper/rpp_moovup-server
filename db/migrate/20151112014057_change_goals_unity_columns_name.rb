class ChangeGoalsUnityColumnsName < ActiveRecord::Migration
  def change
    rename_column :goals, :total_minutes, :goal_total
    rename_column :goals, :target_weight, :goal_target
    rename_column :goals, :current_weight, :goal_current
  end
end
