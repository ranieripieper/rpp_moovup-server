class DropCommentsTable < ActiveRecord::Migration
  def up
    if table_exists?(:comments)
      drop_table :comments
    end
  end

  def down
    puts 'nothing to do :('
  end
end
