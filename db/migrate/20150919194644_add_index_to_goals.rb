class AddIndexToGoals < ActiveRecord::Migration
  def change
    add_index :goals, :periodicity_type
  end
end
