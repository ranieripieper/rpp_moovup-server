class CreateQuestions < ActiveRecord::Migration
  def change
    create_table :questions do |t|
      t.string :body_text
      t.integer :user_id
      t.integer :upvotes_count

      t.timestamps null: false
    end
  end
end
