class AddReportsTable < ActiveRecord::Migration
  def change
    create_table :reports do |t|
      t.references :user, index: true
      t.integer :reportable_id, index: true
      t.string :reportable_type, index: true
      t.integer :report_type
      t.timestamps null: false
    end
  end
end
