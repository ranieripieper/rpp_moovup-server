class AddIndexToGoal < ActiveRecord::Migration
  def change
    add_index :goals, :user_id unless index_exists?(:goals, :user_id)
  end
end
