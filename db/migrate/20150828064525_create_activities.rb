class CreateActivities < ActiveRecord::Migration
  def change
    create_table :activities do |t|
      t.text :body_text
      t.integer :upvotes_count
      t.integer :total_minutes
      t.text :title
      t.integer :goal_id
      t.string :image
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
