class AddStatusChangedAtToFriendships < ActiveRecord::Migration
  def change
    add_column :friendships, :status_changed_at, :datetime
  end
end
