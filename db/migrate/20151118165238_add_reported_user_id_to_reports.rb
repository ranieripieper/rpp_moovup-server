class AddReportedUserIdToReports < ActiveRecord::Migration
  def change
    add_column :reports, :reported_user_id, :integer, foreign_key: true, references: :users
    add_foreign_key :reports, :users
    add_index :reports, :reported_user_id
  end
end
