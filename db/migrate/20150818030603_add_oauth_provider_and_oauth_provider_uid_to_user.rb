class AddOauthProviderAndOauthProviderUidToUser < ActiveRecord::Migration
  def change
    add_column :users, :oauth_provider, :string
    add_column :users, :oauth_provider_uid, :string
  end
end
