class AddLoginAttemptsToUsers < ActiveRecord::Migration
  def change
    add_column :users, :login_attempts, :integer, default: 0
    add_column :users, :blocked_until, :datetime
  end
end
