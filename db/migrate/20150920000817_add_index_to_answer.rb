class AddIndexToAnswer < ActiveRecord::Migration
  def change
    remove_column :answers, :reported

    add_index :answers, :question_id
    add_index :answers, :body_text
    add_index :answers, :reported_at
  end
end
