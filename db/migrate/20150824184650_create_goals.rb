class CreateGoals < ActiveRecord::Migration
  def change
    create_table :goals do |t|
      t.string      :title
      t.boolean     :public
      t.string      :periodicity_type
      t.integer     :total_minutes
      t.decimal     :target_weight
      t.decimal     :current_weight
      t.references  :category, index: true

      t.timestamps null: false
    end
    add_foreign_key :goals, :categories
  end
end
