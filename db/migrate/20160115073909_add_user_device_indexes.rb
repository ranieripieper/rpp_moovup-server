class AddUserDeviceIndexes < ActiveRecord::Migration
  def change
    add_index :user_devices, [:parse_object_id], unique: true
    add_index :user_devices, [:parse_object_id, :token], unique: true
  end
end
