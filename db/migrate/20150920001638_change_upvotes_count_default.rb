class ChangeUpvotesCountDefault < ActiveRecord::Migration
  def change
    change_column :questions, :upvotes_count, :integer, default: 0
    change_column :answers, :upvotes_count, :integer, default: 0
    change_column :activities, :likes_count, :integer, default: 0
  end
end
