class AddCoverImageToQuestions < ActiveRecord::Migration
  def up
    add_column :questions, :cover_image_url, :string

    Question.with_answers.pluck(:id).each do |question_id|
      ::Moovup::V1::QuestionUpdateCoverImageWorker.perform_async(question_id)
    end
  end

  def down
    remove_column :questions, :cover_image_url
  end
end
