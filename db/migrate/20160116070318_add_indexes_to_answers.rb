class AddIndexesToAnswers < ActiveRecord::Migration
  def change
    add_index :answers, [:body_text, :parent_id, :user_id], unique: true
  end
end
