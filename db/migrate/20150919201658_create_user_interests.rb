class CreateUserInterests < ActiveRecord::Migration
  def change
    create_table :user_interests do |t|
      t.references :user, index: true, foreign_key: true
      t.references :category, index: true, foreign_key: true
      t.integer :max_answers_count

      t.timestamps null: false
    end

    add_index :user_interests, [:user_id, :category_id], unique: true
  end
end
