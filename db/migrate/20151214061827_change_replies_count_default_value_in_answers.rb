class ChangeRepliesCountDefaultValueInAnswers < ActiveRecord::Migration
  def change
    change_column :answers, :replies_count, :integer, :default => 0

    Answer.where(replies_count: nil).update_all(replies_count: 0)
  end
end
