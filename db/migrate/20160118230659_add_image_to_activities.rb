class AddImageToActivities < ActiveRecord::Migration
  def change
    unless column_exists?(:activities, :image)
       add_column :activities, :image, :string
    end
  end
end
