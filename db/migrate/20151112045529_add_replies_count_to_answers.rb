class AddRepliesCountToAnswers < ActiveRecord::Migration
  def change
    add_column :answers, :replies_count, :integer
    add_index :answers, :replies_count

    Answer.find_each do |answer|
      Answer.reset_counters(answer.id, :children)
    end
  end
end
