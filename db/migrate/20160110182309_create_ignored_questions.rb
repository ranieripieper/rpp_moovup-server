class CreateIgnoredQuestions < ActiveRecord::Migration
  def change
    create_table :ignored_questions do |t|
      t.references :user, index: true, foreign_key: true
      t.references :question, index: true, foreign_key: true
      t.integer :reason, default: 1

      t.timestamps null: false
    end

    add_index :ignored_questions, [:user_id, :question_id], unique: true
  end
end
