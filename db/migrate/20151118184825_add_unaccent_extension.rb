class AddUnaccentExtension < ActiveRecord::Migration
  def up
    execute "create extension unaccent" unless extension_enabled?(:unaccent)
  end

  def down
    execute "drop extension unaccent" if extension_enabled?(:unaccent)
  end
end
