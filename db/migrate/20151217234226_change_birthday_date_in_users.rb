class ChangeBirthdayDateInUsers < ActiveRecord::Migration
  def up
    change_column :users, :birthday_date, :date
  end

  def down
    change_column :users, :birthday_date, :datetime
  end
end
