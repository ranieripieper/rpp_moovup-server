class ChangeUserIndexes < ActiveRecord::Migration

  def change_foreign_key_20160103185155(table, column, options = {})
    options = options.reverse_merge(on_delete: :cascade)

    remove_foreign_key table, column
    add_foreign_key table, column, options
  end

  def up
    change_foreign_key_20160103185155 :authorizations, :users
  end

  def down
    change_foreign_key_20160103185155 :authorizations, :users, on_delete: nil
  end
end
