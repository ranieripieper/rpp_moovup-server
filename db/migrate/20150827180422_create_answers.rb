class CreateAnswers < ActiveRecord::Migration
  def change
    create_table :answers do |t|
      t.string :body_text
      t.text :comment_id
      t.integer :question_id
      t.integer :comment_id
      t.integer :upvotes_count
      t.boolean :reported
      t.datetime :reported_at
      t.integer :question_id
      t.integer :user_id

      t.timestamps null: false
    end
  end
end
