class RemoveReportedFromQuestions < ActiveRecord::Migration
  def up
    remove_column :questions, :reported, :boolean if column_exists? :questions, :reported

    add_index :questions, :reported_at unless index_exists? :questions, :reported_at
  end

  def down
    add_column :questions, :reported, :boolean
  end
end
