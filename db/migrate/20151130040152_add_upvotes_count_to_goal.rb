class AddUpvotesCountToGoal < ActiveRecord::Migration
  def change
    add_column :goals, :upvotes_count, :integer, default: 0
    add_index :goals, :upvotes_count
  end
end
