class AddTargetDateToGoal < ActiveRecord::Migration
  def change
    add_column :goals, :target_date, :date
  end
end
