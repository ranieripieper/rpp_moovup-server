class AddFieldsToQuestion < ActiveRecord::Migration
  def change
    add_column :questions, :reported, :boolean
    add_column :questions, :reported_at, :datetime
    add_column :questions, :favorited_count, :integer
  end
end
