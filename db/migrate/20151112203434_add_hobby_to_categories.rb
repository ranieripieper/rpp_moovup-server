class AddHobbyToCategories < ActiveRecord::Migration
  def change
    add_column :categories, :hobby, :boolean, default: false
    add_index :categories, :hobby
  end
end
