class ChangeActivityTable < ActiveRecord::Migration
  def change

    column_exists = column_exists?(:activities, :upvotes_count)

    remove_column :activities, :likes_count, :integer
    rename_column :activities, :total_minutes, :total
    add_column :activities, :user_id, :integer, references: :users
    add_column :activities, :date, :date
    add_index :activities, :user_id
    add_index :activities, :goal_id
    add_column :activities, :upvotes_count, :integer unless column_exists
    add_index :activities, :upvotes_count unless column_exists
  end
end
