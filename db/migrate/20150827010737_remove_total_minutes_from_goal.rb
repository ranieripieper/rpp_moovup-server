class RemoveTotalMinutesFromGoal < ActiveRecord::Migration
  def change
    remove_column :goals, :total_minutes, :integer
  end
end
