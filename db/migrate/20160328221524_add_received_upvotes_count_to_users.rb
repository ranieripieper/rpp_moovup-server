class AddReceivedUpvotesCountToUsers < ActiveRecord::Migration
  def change
    add_column :users, :received_upvotes_count, :integer unless column_exists?(:users, :received_upvotes_count)
  end
end
