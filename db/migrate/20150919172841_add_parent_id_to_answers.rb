class AddParentIdToAnswers < ActiveRecord::Migration
  def change
    add_column :answers, :parent_id, :integer
  end
end
