class CreateFavoritedQuestions < ActiveRecord::Migration
  def change
    create_table :favorited_questions do |t|
      t.integer :user_id
      t.integer :question_id

      t.timestamps null: false
    end
  end
end
