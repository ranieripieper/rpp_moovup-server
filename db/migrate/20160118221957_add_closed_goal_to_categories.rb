class AddClosedGoalToCategories < ActiveRecord::Migration
  def change
    add_column :categories, :closed_goal, :boolean, default: false
  end
end
