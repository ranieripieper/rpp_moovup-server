class AddHobbyIdToUsers < ActiveRecord::Migration
  def change
    add_column :users, :hobby_id, :integer
    add_index :users, :hobby_id
  end
end
