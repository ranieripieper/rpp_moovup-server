class AddAnswersCountToQuestions < ActiveRecord::Migration
  def up
    add_column :questions, :answers_count, :integer, default: 0
    add_index :questions, :answers_count

    Question.find_each do |question|
      Question.reset_counters(question.id, :answers)
    end
  end
  def down
    remove_column :questions, :answers_count, :integer
  end
end
