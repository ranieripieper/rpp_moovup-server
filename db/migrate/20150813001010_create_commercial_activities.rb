class CreateCommercialActivities < ActiveRecord::Migration
  def change
    create_table :commercial_activities do |t|
      t.string :title

      t.timestamps null: false
    end
  end
end
