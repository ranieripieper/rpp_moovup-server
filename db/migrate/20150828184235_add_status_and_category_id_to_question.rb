class AddStatusAndCategoryIdToQuestion < ActiveRecord::Migration
  def change
    add_column :questions, :status, :string
    add_column :questions, :category_id, :integer
  end
end
