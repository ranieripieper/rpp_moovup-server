class AddIndexesToQuestions < ActiveRecord::Migration
  def change
    add_index :questions, :body_text
    add_index :questions, :reported_at
  end
end
