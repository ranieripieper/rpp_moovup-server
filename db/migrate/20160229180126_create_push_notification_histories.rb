class CreatePushNotificationHistories < ActiveRecord::Migration
  def change
    create_table :push_notification_histories do |t|
      t.integer :receiver_user_id
      t.integer :sender_user_id
      t.integer :notificable_id
      t.string :notificable_type
      t.string :message
      t.string :notification_type
      t.json :metadata

      t.datetime :delivery_at

      t.timestamps null: false
    end

    add_index :push_notification_histories, :notificable_id
    add_index :push_notification_histories, :notificable_type
    add_index :push_notification_histories, :receiver_user_id
    add_index :push_notification_histories, :sender_user_id
    add_index :push_notification_histories, :message
    add_index :push_notification_histories, :notification_type
  end
end
