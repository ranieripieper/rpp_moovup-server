[![Build Status](https://semaphoreci.com/api/v1/projects/b36e9d3a-fc64-43e4-a71c-346da21b95d8/508004/badge.svg)](https://semaphoreci.com/fidelisrafael/moovup-api)

# API
Camada de serviço do projeto Moovup.

## Desenvolvimento
O desenvolvimento deverá ser feito através do `branch develop`, e todas as ramificações deverão ser originadas dele.

```
~$ git checkout develop
~$ git checkout -b feature_new_feature_name
```

Para garantir qualidade, o projeto é desenvolvido baseado em testes.

Todo push feito no `branch master` é validado na ferramenta de _Integração Contínua_ [Semaphore CI](https://semaphoreci.com/fidelisrafael/moovup-api/ "Semaphore - fidelisrafael/moovup-api"), e caso os testes passem, será publicado em produção. Muito cuidado com isso.

```
~$ git add .
~$ git commit -am "New feature commit. Will publish in staging."
~$ git checkout develop
~$ git pull --rebase origin develop
~$ git merge feature_new_feature_name --no-ff
~$ git push origin develop
~$ git checkout master
~$ git pull --rebase origin master
~$ git merge develop --no-ff
~$ git push origin master
```

### Git remotes

Para facilitar sua vida, adicione todos os remotes da aplicação com os seguintes comandos:

```
git remote add heroku git@heroku.com:moovup-api-staging.git

git remote add heroku-prod git@heroku.com:moovup-api-production.git

git remote add origin git@bitbucket.org/fidelisrafael/moovup-api.git
```

## Dependências
 - [ruby](https://rvm.io/ "RVM") __2.2.1__
 - [grape](https://github.com/intridea/grape, "Grape") >= _0.12_
 - [postgresql](http://www.postgresql.org/ "PostgreSQL") >= __9.2.0__
   - Extensions:
       - [hstore](http://www.postgresql.org/docs/9.1/static/hstore.html)
       - [json](http://www.postgresql.org/docs/9.4/static/datatype-json.html)
 - [redis](http://redis.io/, "Redis") >= _3.0.2_
 - [memcached](http://memcached.org/, "MemCached") >= _1.4.24_
 - [imagemagick](http://www.imagemagick.org/script/index.php, "ImageMagick") >= _6.9.1-5_
 - [optipng](http://optipng.sourceforge.net/) >= _0.7.5_
 - [jpegoptim](http://freecode.com/projects/jpegoptim) >= _1.4.2_
 - [bundle](http://bundler.io/ "Bundler") >= __1.10.0__
 - [node](https://nodejs.org/ "Node JS") >= __0.10.32__

## Setup inicial

No postgres, crie um banco para desenvolvimento e outro para testes:

```
~$ psql -U postgres
# CREATE DATABASE moovup_pi_development;
# CREATE DATABASE moovup_api_test;
# \q
```

Instale as dependências do projeto, listadas no `Gemfile`:

```
~$ bundle install
```

Copie e edite o arquivo database.yml com os dados de acesso aos bancos de dados locais:

```
~$ cp config/database.yml.sample config/database.yml
~$ subl config/database.yml
```

Após a configuração com as credenciais do banco de dados, execute:

`rake db:seed` para popular o banco com os dados iniciais.

Configure e inicialize o banco de dados, com seus dados iniciais:

```
~$ rake db:setup
```

Por fim, rode os testes. Isso garantirá que as dependências estejam funcionando na sua máquina:

```
~$ rake spec
```

## API

**Staging**: http://moovup-api-staging.herokuapp.com/api
**Production**: http://moovup-api-production.herokuapp.com/api

```
[GET,POST,PUT,DELETE] /api/{version}/{endpoint} HTTP/1.1
```

## Deploy

Conforme já dito, `push's` enviados para o branch `master` são automaticamente processados pela ferramenta de CI, e se **TODOS** os testes passarem, é feito deploy da aplicação para **produção**.

Em casos aonde não seja necessário (ou mesmo não seja desejado) rodar o build de testes da aplicação para deploy(_production somente_), é possivel utilizar os seguintes comandos:

```
~$ rake deploy:staging:migrations
```
> Este comando coloca a aplicação em `manutenção` e roda o comando de migrations no servidor remoto, após a execução do comando a aplicação volta a ficar disponível.

```
~$ rake deploy:staging [RELEASE_NAME='my-awesome-new-feature'] [RUN_MIGRATIONS=true] [FORCE=true]
```
> Este comando envia o código do branch `develop` para a aplicação em staging e em seguida roda `deploy:staging:migrations`.
**OBS**: O comando de migrations somente é executado se a variavel de ambiente `RUN_MIGRATIONS` estiver presente.
As variáveis de ambiente são opcionais no momento da execução do comando.
`RELEASE_NAME` = Quando não é fornecido a o nome da tag de release é gerado a partir do timestamp atual.
`FORCE` =  Quando estiver presente, o comando de push irá forçar a atualização no remote.

```
~$ rake deploy:staging:rollback
```
> Rode este comando quando for necessário fazer rollback da aplicação para seu ultimo `release` publicado.

Para o ambiente de produção, os comandos são:

```
~$ rake deploy:production:migrations
~$ rake deploy:production RUN_MIGRATIONS=true|false RELEASE_NAME='my-awesome-new-feature' FORCE=true|false
~$ rake deploy:production:rollback
```

Para efetuar o deploy para os dois ambientes, execute:
```
~$ rake db:deploy:all
```

## Rake Tasks

Além das `rake tasks` para deploy, o projeto inclui utilidades para otimizar e automatizar algumas tarefas, entre elas:

`~$ rake api:routes` - Lista com todos os endpoints expostos da API
`~$ rake db:truncate` - Limpa todas as tabelas do banco de dados utilizando `truncate`
`~$ rake db:truncate TABLES=users,authorizations` - Efetuar truncate nas tabelas especificadas
`~$ rake stats` - Exibe estatisticas do código do projeto


## Desenvolvimento

### Configurações da aplicação

As variaveis de configurações da aplicação são gerenciadas utilizando o [Figaro](https://github.com/laserlemon/figaro).
O arquivo `config/application.yml` é o arquivo principal com as configurações do projeto, nele é possivel definir configuraçoes especificas por ambiente de aplicação - `development`,  `staging` e `production`. Todas as configurações setadas neste arquivo, serão adicionadas como variáveis de ambiente na Heroku logo após o deploy da aplicação. (quando feito via rake task)

Para atualizar as variaveis - na Heroku - a qualquer momento, execute:

```
figaro heroku:set -e APP_ENVIRONMENT(staging,production) --app HEROKU_APP_NAME(moovup-api-staging, moovup-api-production) --remote=REMOTE_NAME(heroku, heroku-prod)
```

Ex:

`figaro heroku:set -e staging --app moovup-api-staging --remote=heroku`

**OBS**: Variáveis de configuração devem ser do tipo `CHAVE=VALOR` não sendo possível armazenar estruturas aninhados.

## Setup sidekiq

Muitos processos são executados em background, para isso utilizamos o **excelente** projeto [Sidekiq](https://github.com/mperham/sidekiq). Para executar o sidekiq como daemon, certifique-se de estar rodando o `redis-server`, e então execute:

`bundle exec sidekiq -C config/sidekiq.yml -d`

Para reiniciar o processo do sidekiq, primeiro procure pelo PID: `~$ ps aux | grep sidekiq` e mate o
processo: `~$ kill -9 SIDEKIQ_PID`, e então execute o comando acima para iniciar o processo novamente.

Para acessar o painel web, inicie o servidor(ex: `~$ rails s -p 9090`) da aplicação e acesse `/sidekiq/`, ex:

`http://localhost:9090/sidekiq`

As credenciais de acesso estão no arquivo `config/application.yml` (`sidekiq_username` e `sidekiq_password`)


### Foreman

Este projeto utiliza o [Foreman](https://github.com/ddollar/foreman) para gerenciar os processos necessários para executar a aplicação, entre eles:

* Servidor web
* Sidekiq (Job Background Processing)
* Redis

Primeiramente, instale a gem: `gem install foreman`.

Em seguida, para facilitar a execução de todos os processos, você pode executar o comando:

`PORT=9090 foreman start -f Procfile.dev`

Isso vai subir o servidor de aplicação na porta 9090, e iniciar o sidekiq e redis. Para mais detalhes, verifique o arquivo `Procfile.dev`
_(obs: O arquivo Procfile é utilizado pela Heroku)_

## Versionamento da API

Esta API utiliza versionamento via `URL path`, ex:

`http://api.moovupapp.com/v1/{...}`

Para adicionar uma nova versão da API, é necessário seguir esse fluxo:

1 - Criar pastas e arquivos para manter convenção de
```
app/grape/api/v{VERSAO}
 -> api/v{VERSAO}/base.rb

app/grape/api/helpers/v{VERSAO}
 -> helpers/v{VERSAO}/applications_helpers.rb

app/serializers/moovup/v{VERSAO}

app/workers/moovup/v{VERSAO}

app/services/moovup/v{VERSAO}
 -> moovup/v{VERSAO}/services_concerns
 -> moovup/v{VERSAO}/base_service.rb

app/workers/moovup/v{VERSAO}
```

Ex: **Versão 2**

```
app/grape/api/v2
 -> api/v2/base.rb

app/grape/api/helpers/v2
 -> helpers/v2/applications_helpers.rb

app/serializers/moovup/v2

app/workers/moovup/v2

app/services/moovup/v2
 -> moovup/v2/services_concerns
 -> moovup/v2/base_service.rb

app/workers/moovup/v2
```

O arquivo `app/grape/api/v2/base.rb` é o arquivo principal, nele devem ser montadas todos os endpoints da nova versão, após esse processo, você deve montar a classe `API::V2::Base` no arquivo `app/grape/api/base.rb`, ex: `mount API::V2::Base`

Para facilitar todo esse trabalho(isso tudo foi explicado para deixar todo o processo mais didatico, você pode executar a rake task:

`rake api:add_new_version VERSION=2`

----------


### Criação de Endpoints

Diferente do padrão do Rails, as rotas do Grape não são gerenciadas via arquivo `config/routes.rb`,  cada endpoint deve ser explicitamente declarado no escopo da aplicação.
Nesse projeto, os endpoints estão em arquivos separados, dentro de suas respectivas classes, e posteriormente são montandos no arquivo base da aplicação.

Para mais detalhes sobre a DSL do Grape, visite a documentação. (https://github.com/ruby-grape/grape)

Todos os endpoints - versionados - devem estar na pasta `app/grape/api`, um arquivo `base.rb` deve estar presente em cada versão, para ser montado posteriormente no arquivo `app/grape/api/base.rb` (veja a seção "Versionamento da API" para mais detalhes).

O fluxo para adicionar um novo endpoint, deve ser:

1 - Verificar necessidade de criar arquivo separado para endpoint.
  -> Caso seja necessário, criar também um arquivo de helper.
2 - Descrever

#### Cache de Endpoints

Desenvolvi para este projeto, uma arquitetura de cache para facilitar a criação e gerenciamento de endpoints que necessitam de cache para otimizar o tempo de resposta.

Para adicionar um endpoint com cache, siga os seguintes passos:

**1 -** Edite o arquivo `config/caches.yml` para adicionar as configurações de cache e tempo de expiração dos dados no servidor de cache. (Nesse caso, MemCached)
Ex:

```
 hello:
   world:
     key: 'hello_world'
     expires: <%= 2.days %>
```

**2 -** Descreva o endpoint usando o seguinte padrão:

```
with_cacheable_enpoints :hello do
 get :world do
  respond_with_cacheable('hello.world') do
    { hello: 'world' }
  end
 end
end
```

Com isso, duas URLs serão disponibilizadas para acessar este endpoint:

`/hello/world`
`/cached/hello/world`

Ao acessar a URL com cache, a aplicação vai tentar salvar a resposta no servidor de cache, e todas as demais requisições nessa URL irão primeiramente buscar os dados no cache.
A URL sem cache nunca tentará guardar a resposta em cache, sendo possível dessa forma disponibilizar endpoints que ainda não tenham invalidação de cache.

----

#### Endpoints paginados

Muitos endpoints que retornam dados diretamente do banco de dados, devem ser paginados, com isso é evitada a sobrecarga no servidor e também na aplicação cliente.

Para adicionar um endpoint paginado, você deve seguir o seguinte padrão:

```
paginated_endpoint do
 desc 'Returns all musics from artist'
 route_param :id do
   get :musics do
     artist = Artist.find_by(id: params[:id])
     musics = paginate(artist.musics)
     {
       musics: musics,
       meta: pagination_meta # helper method
     }
   end
 end
end

# [GET] /1/musics # default per_page=30&page=1
# [GET]/1/musics?per_page=20&page=1
```

É possivel definir os valores padrões de para um endpoint paginado utilizando:

```
paginated_endpoint per_page: 15, max_per_page: 30 do
end
```

O exemplo acima não utiliza todas as convenções de resposta do projeto, para ficar mais fiel a arquitetura do sistema, os objetos da resposta devem devem ser serializadas utilizando um Serializer que herda a class `ActiveModel::Serializer`.

**OBS:** - Por padrão, o ideal é criar um simple_serializer e um serializer full para as entidades, ex:

```
class SimpleMusicSerializer < ActiveModel::Serializer

class MusicSerializer < SimpleMusicSerializer
```

O serializer `simple` **NUNCA** deve carregar associações, somente dados da própria entidade, o serializer completo deve ser utilizado para isso.

Baseado no exemplo acima, o correto seria:

```
paginated_endpoint do
 desc 'Returns all musics from artist'
 route_param :id do
   get :musics do
     artist = Artist.find_by(id: params[:id])
     musics = paginate(artist.musics)

     options = {
      # SimpleMusicSerializer must be declared in
      # app/serializers/moovup/v1/music_serializer.rb
      serializer: :simple_music,
      root: :musics
     }

    # This is will include `meta: pagination` in response
    paginated_serialized_array(musics, options)

   end
 end
end

### app/services/moovup/v1/simple_music_serializer.rb

module Moovup
  module V1
    class SimpleMusicSerializer < ActiveModel::Serializer
      root false
      attributes :id, :title, :year, :duration
    end
  end
end

### app/services/moovup/v1/music_serializer.rb
module Moovup
  module V1
    class MusicSerializer < SimpleMusicSerializer
       has_one :artist
    end
  end
end
```

#### Cache de Endpoints paginados

```
paginated_endpoint do
 desc 'Returns all musics from artist'
 route_param :id do
   get :musics do
    response_for_paginated_endpoint 'artists.musics'
	 paginated_serialized_array(musics, options)
    end
   end
 end
end

## config/caches.yml
artists:
  musics:
    key: 'artist_id_%s_musics_page_%s_per_page_%s'
    expires: <%= 1.hour %>

# [GET]/1/musics?per_page=20&page=1

# [GET]/cached/1/musics?per_page=20&page=1
  ### artist_id_1_musics_page_1_per_page_20

# [GET]/cached/2/musics?per_page=20&page=2
  ### artist_id_2_musics_page_2_per_page_20

```

----

### Arquitetura de Services


[TODO]
* Base Service
* Base Crud Service



### Arquitetura de Uploaders
[TODO]

* Processamento de imagens
* Upload & Setup


### Arquitetura de Serializers

* Convenções
* Association namespace


[TODO]

### Arquitetura de Workers
[TODO]

### Estruturação de cache
 [TODO]
  * Configuração

### I18n
[TODO]
  * Setup
