# encoding: UTF-8

require 'carrierwave'

CarrierWave.configure do |config|

  config.fog_credentials = {
    :provider               => 'AWS',
    :aws_access_key_id      => Moovup::Config.aws_access_key_id,
    :aws_secret_access_key  => Moovup::Config.aws_secret_access_key,
    :region                 => Moovup::Config.aws_region
  }

  config.fog_use_ssl_for_aws = Moovup::Config.enabled?(:aws_use_ssl)
  config.fog_directory  = Moovup::Config.aws_bucket_name
  config.fog_public     = true
  config.fog_attributes = {'Cache-Control'=>"max-age=#{Moovup::Config.aws_cache_max_age}"}

  config.remove_previously_stored_files_after_update = false
end
