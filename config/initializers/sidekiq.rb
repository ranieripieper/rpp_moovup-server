redis_url = ENV['REDISTOGO_URL'] || Moovup::Config.redis_url.presence

Sidekiq.configure_server do |config|
  config.redis = { url: redis_url }
end

Sidekiq.configure_client do |config|
  config.redis = { url: redis_url }
end
