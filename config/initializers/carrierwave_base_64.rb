module Carrierwave
  module Base64
    class Base64StringIO < StringIO

      alias :_initialize :initialize
      def initialize(encoded_file, base_filename=nil)
        @base_filename =  base_filename || 'file'
        _initialize(encoded_file)
      end

      def original_filename
        base_filename = (@base_filename || 'file').to_s.underscore
        File.basename("#{base_filename}.#{@file_format}")
      end
    end
  end
end


module Carrierwave
  module Base64
    module Adapter
      def mount_base64_uploader(attribute, uploader_class, options = {})
        mount_uploader attribute, uploader_class, options

        define_method "#{attribute}=" do |data|
          if data.present? && data.is_a?(String) && data.strip.start_with?("data")
            super(Carrierwave::Base64::Base64StringIO.new(data.strip, options[:base_filename] || attribute))
          else
            super(data)
          end
        end
      end
    end
  end
end
