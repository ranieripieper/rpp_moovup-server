# encoding: UTF-8
require 'sidekiq/web'

Rails.application.routes.draw do

  mount API::Base => '/'

  Sidekiq::Web.use Rack::Auth::Basic do |username, password|
    username == Moovup::Config.sidekiq_username && password == Moovup::Config.sidekiq_password
  end

  mount Sidekiq::Web, at: '/sidekiq'
end

