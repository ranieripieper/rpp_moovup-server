every :hour do
  rake "cron_jobs:async:send_questions_to_answer_push_notifications"
end

every :day, at: '10:00AM' do
  rake 'cron_jobs:async:send_invite_push_notifications'
end
