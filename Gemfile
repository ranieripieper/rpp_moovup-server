source 'https://rubygems.org'
ruby '2.3.0'

gem 'rails', '4.2.3'
gem 'pg'

group :development do
  gem 'brakeman', require: false
  gem 'letter_opener'
  gem 'rubocop', require: false
  gem "rails-erd"
end

group :development, :staging, :test do
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'
  gem 'factory_girl_rails', '~> 4.0'
  gem 'shoulda-matchers', require: false
  gem 'nyan-cat-formatter'
  gem 'rspec-rails', '~> 3.0'
  gem 'pry'
end

# deploy specific
group :development do
  gem 'capistrano',         require: false
  gem 'capistrano-rvm',     require: false
  gem 'capistrano-rails',   require: false
  gem 'capistrano-bundler', require: false
  gem 'capistrano3-puma',   require: false
  gem 'capistrano-sidekiq', require: false

  # mão na roda!
  gem 'capistrano-rails-tail-log', require: false
  gem 'capistrano-rails-console', require: false

  # ensure folders creation and ownership
  gem 'capistrano-safe-deploy-to', '~> 1.1.1', require: false

  # good
  gem 'capistrano-ssh-doctor', '~> 1.0'

  # pretty print for capistrano tasks
  gem 'airbrussh', :require => false

  # srever selection on deploy (in case theres more than 1 server)
  gem 'capistrano-hostmenu', require: false

  # useful rails tasks
  gem 'capistrano-rails-collection'
end

group :staging, :production do
  gem 'rack-timeout'
end

gem 'acts_as_tree'
gem 'acts_as_list'
gem 'bcrypt', '~> 3.1.7'
gem 'carrierwave', require: ['carrierwave', 'carrierwave/orm/activerecord']
gem 'carrierwave_backgrounder'
gem 'carrierwave-base64'
gem 'colorize'
gem 'dalli'
gem 'dalli-elasticache'
gem 'database_cleaner'
gem 'faker'
gem 'figaro'
gem 'fog'
gem 'foreman'
gem 'lazy_columns'
gem 'mini_magick'
gem 'newrelic_rpm'
gem 'paranoia', '~> 2.0'
gem 'parse-ruby-client', git: 'https://github.com/adelevie/parse-ruby-client.git'
gem 'piet'
gem 'pg_search'
gem 'piet-binary'
gem 'rack-cors'
gem 'rollbar', '~> 1.4.4'
gem 'sinatra', require: false
gem 'sidekiq'
gem 'puma'
gem 'whenever', :require => false

# grape specific
gem 'grape', '~> 0.12'
gem 'grape-kaminari'
gem 'grape-active_model_serializers', require: 'grape-active_model_serializers'
gem 'rack-contrib'
gem 'grape-swagger'
